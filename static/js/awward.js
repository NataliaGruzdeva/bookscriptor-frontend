$(function () {
    uploadFile();
    
    function uploadFile() {
        $('body').on('change', '.js-upload-file', function (e) {
            var $input = $(this),
            $txt = $input.siblings('.js-upload-name'),
            fileName = $input.val().replace(/^.*[\\\/]/, '');

            if(fileName) {
                $txt.text('Вы загрузили файл: ' + fileName);
            }
        });
    }
})