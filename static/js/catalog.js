var data;

$(function () {
    hideHoverPanel();
    initialRangeSlider();
    initialCatalogSlider();
    showFilters();
    validateFilterForm();
    searchBooks();
    sendToCard();
    changePrice();
    sortBooks();
});
function initialCatalogSlider() {
    $('[data-content=\'catalog-slider\']').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 5
    });
}
function initialRangeSlider() {
    $( "#slider-range-max" ).slider({
        value:1200,
        min: 0,
        max: 1200,
        step: 10,
        slide: function( event, ui ) {
            $( "#price-max" ).val( ui.value).trigger("change");
        }
    });
    $( "#price-max" ).val( $( "#slider-range-max" ).slider( "value" ));

    $( "#slider-range-min" ).slider({
        value:0,
        min: 0,
        max: 1200,
        step: 10,
        slide: function( event, ui ) {
            $( "#price-min" ).val( ui.value).trigger("change");
        }
    });
    $( "#price-min" ).val( $( "#slider-range-min" ).slider( "value" ));
}

function showFilters() {
    $('body').on('click', '[data-action="show-more-filters"]', function (e) {
        e.preventDefault();
        $(this).removeClass('active');
        $('[data-action="hide-more-filters"]').addClass('active');
        $('[data-filter="add"]').slideDown();
    })
    $('body').on('click', '[data-action="hide-more-filters"]', function (e) {
        e.preventDefault();
        $(this).removeClass('active');
        $('[data-action="show-more-filters"]').addClass('active');
        $('[data-filter="add"]').slideUp();
    })
}

function hideHoverPanel() {
    $('body').on('mouseover', '[data-menu]', function (e) {
        e.preventDefault();
        $('.jq-selectbox__dropdown').each(function () {
           $(this).hide();
        });
    });
}

function changePrice() {
    $('body').on('change', '[data-filter-price]', function (e) {
        e.preventDefault();
        $('form[data-form="filter"]').submit();
    })
}

function sortBooks() {
    $('body').on('change', '[data-sort="filter"]', function (e) {
        e.preventDefault();
        sendFilter();
    })
}

function sendFilter() {
    var data = $('[data-form="filter"]').serialize();
    console.log(data);
    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        error: function (data) {
            console.log(data);
            $('[data-content="searchResult"]').html(data);
        }
    })
}

function searchBooks() {
    $('body').on('click', '[data-action="search"]', function (e) {
        e.preventDefault();
        var data = $('[data-form="search"]').serialize();
        console.log(data);
        $.ajax({
            url: '/ajax/print_book/send_anket.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {
                console.log(data);
                $('[data-content="searchResult"]').html(data);
            }
        })
    })
}

/*валидация формы*/
function validateFilterForm() {
    var form = $('form[data-form="filter"]');
    $(form).validate({
        rules: {
            minPrice: {
                required: true,
                range: [0, parseInt($('[name="maxPrice"]').val())]
            },
            maxPrice: {
                required: true,
                range: [parseInt($('[name="minPrice"]').val()), 1200]
            },
        },
        messages: {

        },
        submitHandler: function submitHandler(form) {
            sendFilter();

        }
    });

}

function sendToCard() {
    $('body').on('click', '[data-action="cart"]', function (e) {
        var btnThis = $(this);
        data += "&typeBook=print" + $(this).data('name');
        console.log(data);
        $.ajax({
            url: '/ajax/print_book/send_anket.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {
                if($(btnThis).data('free')) {
                    $(btnThis).html('В библиотеке →');
                    $(btnThis).closest('[data-book-item]').find('[data-price]').html('В библиотеке');

                } else if($(btnThis).data('pay')) {
                    $(btnThis).html('В корзине →');
                    $(btnThis).closest('[data-book-item]').find('[data-price]').html('В корзине');
                }
            }
        })
    });
}