var download_file,
    data;

$(function () {
    proofReaderTabs();
    changeSteps();
    showExampleImg();
    customInputNumber();
    hardingTabs();
    validateForm();
    onlyTextValidate();

});
function proofReaderTabs() {
    $('body').on('click', '.c-tab-design', function (e) {
        var $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');

        $('.c-tab-content').hide().removeClass('active');
        $(showBlock).fadeIn('100').addClass('active');


    });
    $('body').on('click', '.c-next-tab', function (e) {
        e.preventDefault();
        var thisTabId = parseInt($(this).parents('.c-tab-content').attr('id'));
        var nextId = thisTabId + 1;
        $('.c-tab-design').removeClass('active') ;
        $('.c-tab-design[data-link="#' + nextId + '"]').addClass('active') ;

        $('.c-tab-content').hide().removeClass('active');
        $('.c-tab-content#' + nextId).fadeIn('100').addClass('active');
        sendForm();

    })
    $('body').on('click', '.c-prev-tab', function (e) {
        e.preventDefault();
        var thisTabId = parseInt($(this).parents('.c-tab-content').attr('id'));
        var nextId = thisTabId - 1;
        $('.c-tab-design').removeClass('active') ;
        $('.c-tab-design[data-link="#' + nextId + '"]').addClass('active') ;

        $('.c-tab-content').hide().removeClass('active');
        $('.c-tab-content#' + nextId).fadeIn('100').addClass('active');
        sendForm()

    })
}

function hardingTabs() {
    $('body').on('click', '.c-tab', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $('.c-tab').removeClass('active');
        $this.addClass('active');

        $('.c-tab-block').hide().removeClass('active');
        $(showBlock).fadeIn('100').addClass('active');
    });
}
/*валидация формы*/
function validateForm() {
    $("form[name='design-calc']").validate({
        rules: {
            phone: "required",
            name: "required",
            email: {
                required: true,
                // Отдельное правило для проверки email
                email: true
            }
        },
        messages: {
            name: "Напишите свое имя",
            email: "Оставьте свой email",
            phone: "Оставьте телефон для связи",
        },
        submitHandler: function submitHandler(form) {
            sendForm()
        }
    });

}

function sendForm() {
    var data = new FormData();
    data.append('name', $('input[name="name"]').val());
    data.append('email', $('input[name="email"]').val());
    data.append('phone', $('input[name="phone"]').val());
    data.append('book', download_file);
    data.append('color', $('input[name="color"]:checked').data('check'));
    data.append('type', $('input[name="type"]:checked').data('check'));
    data.append('count', $('input[name="count"]').val());
    data.append('harding', $('a.c-tab.active').data('harding'));
    console.log(data)
    // getFormData(data);
    // getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        error: function (data) {

        }
    })
}
function changeSteps() {
    if($('.dropzone-steps').length) {
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone(".dropzone-steps", {
            maxFiles: 1,
            url: "/ajax/print_book/dropzone_upload.php",
            paramName: "image",
            acceptedFiles: '.doc, .docx',
            init: function() {
                var $this = this,
                    isChanged = false;

                $this.on("addedfile", function(file, done) {
                    $('.dropzone-steps').removeClass('preload');
                    $('.c-preloader').hide();

                    if (isChanged) {
                        var newFile = file;
                        isChanged = false;
                        $this.removeAllFiles(true);
                        $this.emit("addedfile", newFile);
                        //$this.emit("thumbnail", newFile, newFile.url);
                        $this.files.push(newFile);
                    }

                });

                $this.on("removedfile", function(file, done) {
                    $('.dropzone-steps').removeClass('preload');
                    $('.c-preloader').hide();
                });

                $this.on("totaluploadprogress", function(file, progress, bytesSent) {
                    $('.dropzone-steps').addClass('preload');
                    $('.c-preloader').show();
                });

                $('body').on('click', '.c-change-dropzone', function () {
                    isChanged = true;
                    $this.removeAllFiles(true);
                    console.log('change');
                    $('#upload').trigger('click');
                });
            },
            accept: function(file, done) {
                $('.c-steps').removeClass('active');
                $('.c-step-2').addClass('active');
                if(!$('.c-append-form').length) {
                    $('.dz-details').append('<div class="append-form c-append-form"><a href="javascript:void(0);" class="change-dropzone c-change-dropzone">Изменить файл</a><button class="custom-btn custom-btn--green c-next-tab">Рассчитать</button></div>');
                }
                download_file = file;
            }

        });
    }
}

function onlyTextValidate() {
    $('body').on('click', '.c-check-type', function (e) {
        if($(this).data('check') == "WITH_IMG") {
            console.log(1223);
            $('.c-quantity').addClass('not-disabled')
        } else {
            $('.c-quantity').removeClass('not-disabled')
        }
    });
}
function showExampleImg() {
    $('.c-check-color').each(function () {
        if (($(this).prop('checked')) && (($(this).data('check') == 'B_W_PRINT'))) {
            $('.c-check-type').each(function () {
                if (($(this).prop('checked')) && ($(this).data('check') == 'ONLY_TEXT')) {
                    $('.example-image').hide();
                    $('.one').show();
                } else if (($(this).prop('checked')) && ($(this).data('check') == 'WITH_IMG')) {
                    $('.example-image').hide();
                    $('.two').show();
                }
            })
        } else  if (($(this).prop('checked')) && (($(this).data('check') == 'COLOR_PRINT'))) {
            $('.c-check-type').each(function () {
                if (($(this).prop('checked')) && ($(this).data('check') == 'ONLY_TEXT')) {
                    $('.example-image').hide();
                    $('.three').show();
                } else if (($(this).prop('checked')) && ($(this).data('check') == 'WITH_IMG')) {
                    $('.example-image').hide();
                    $('.four').show();
                }
            })
        }
    })
}
function customInputNumber() {
    $('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    $('.quantity').each(function() {
        var spinner = $(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
}