var download_file,
    price_one_example,
    price_full,
    data;
$(function () {
    validateForm();
    showExampleImg();
//    checkFilterImg();
    dataSerialize();
    $('.c-size-select').select2('destroy');
    $('input[type="checkbox"], .c-size-select').styler();
    // $('.owl-carousel').owlCarousel({
    //     loop: true,
    //     margin: 10,
    //     nav: true,
    //     items: 1
    // });
    $(".input-phone, .input-phone-call-me").mask("+7 (999) 999-9999");

    $('.price .tooltip_printbook').click(function () {
        $(this).find(".tooltip-content").fadeIn();
    });
    $('.c-list-book').mCustomScrollbar({
        theme: "dark",
        scrollButtons:{
            enable: true
        }
    });


    if($('.dropzone')) {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone", {
            maxFiles: 1,
            url: "/ajax/print_book/dropzone_upload.php",
            addRemoveLinks: true,
            dictRemoveFile: "Удалить",
            dictCancelUpload: "Отменить загрузку",
            paramName: "file",
            acceptedFiles: '.docx, .doc, .pdf',
            accept: function(file) {
                download_file = file;
                console.log(download_file);
            }

        });
    }

    $('body').on('click', '.send_more', function (e) {
        e.preventDefault();
        $('.short-form').show();
        $("form[name='printbook_order']").show().nextAll('.short-form-success').hide();
        $("form[name='printbook_order']").show().nextAll('.short-form-error').hide();
        $('.input-phone-call-me').val('');
        $('.c-count').val('');
        $('.c-page').val('');
        $('.c-calc').hide();
    })

});



$(document).click(function (e) {
    if ($(e.target).is('.price .tooltip_printbook, .tooltip-content, .tooltip-content *')) return false;else $('.tooltip-content').fadeOut();
});
/*показ примера с картинкой в зависимости от выбранных опций*/
//function checkFilterImg() {
//    $('body').on('click', '.c-check', function () {
//        showExampleImg();
//    })
//}

function showExampleImg() {
    $('.c-check-color').each(function () {
        if (($(this).prop('checked')) && (($(this).data('check') == 'B_W_PRINT'))) {
            $('.c-check-type').each(function () {
                if (($(this).prop('checked')) && ($(this).data('check') == 'Скрепка')) {
                    $('.example-image').hide();
                    $('.c-chb-clip').show();
                } else if (($(this).prop('checked')) && ($(this).data('check') == 'Твердый переплет')) {
                    $('.example-image').hide();
                    $('.c-chb-hard-cover').show();
                } else if (($(this).prop('checked')) && ($(this).data('check') == 'Мягкий переплет')) {
                    $('.example-image').hide();
                    $('.c-chb-soft-cover').show();
                }
            })
        } else  if (($(this).prop('checked')) && (($(this).data('check') == 'COLOR_PRINT'))) {
            $('.c-check-type').each(function () {
                if (($(this).prop('checked')) && ($(this).data('check') == 'Скрепка')) {
                    $('.example-image').hide();
                    $('.c-color-clip').show();
                } else if (($(this).prop('checked')) && ($(this).data('check') == 'Твердый переплет')) {
                    $('.example-image').hide();
                    $('.c-color-hard-cover').show();
                } else if (($(this).prop('checked')) && ($(this).data('check') == 'Мягкий переплет')) {
                    $('.example-image').hide();
                    $('.c-color-soft-cover').show();
                }
            })
        }
    })
}

/*валидация формы*/
function validateForm() {
    $("form[name='printbook_order']").validate({
        // Правила проверки полей
        rules: {
            // Ключ с левой стороны это название полей формы.
            // Правила валидации находятся с правой стороны
            name: "required",
            email: {
                required: true,
                // Отдельное правило для проверки email
                email: true
            },
            phone: "required",
            kol: {
                required: true,
                number: true
            },
            pages: {
                number: true,
                required: true,
                max: $('.c-page').data('max'),
                min: $('.c-page').data('min'),
            },
            file1: {
                required: true,
                uploadFile: true

            }
        },
        // Установка сообщений об ошибке
        messages: {
            name: "Напишите свое имя",
            email: "Оставьте свой email",
            phone: "Оставьте телефон для связи",
            file: "Прикрепите файл"
        },
        submitHandler: function submitHandler(form) {
            clickCalc();
        }
    });

    $("form[name='printbook_call']").validate({
        rules: {
            phone_callback: "required"
        },
        messages: {
            phone: "Оставьте телефон для связи"
        },
        submitHandler: function submitHandler(form) {
            // form.submit();
            clickCallbackPrintBook();
        }
    });

}
/*сбор данных и отправка аякса*/
function dataSerialize() {
    var data = '';

    /*Цветность печати*/
    $('body').on('click', '.c-check-color', function () {
        data = new FormData();
        data.append('color', $(this).data('check'));
        sendAjax(data);
        //showExampleImg();
    })

    $('body').on('click', '.c-check-type', function () {
        data = new FormData();
        data.append('type', $(this).data('check'));
        $('.c-check-color').each(function () {
            if($(this).prop('checked')) {
                data.append('color', $(this).data('check'));
            }
        })
        sendAjax(data);
    })

    /*выбор размера*/
    $('body').on('change', '.c-check-size', function () {
        data = new FormData();
        data.append('size', $('.c-check-size:selected').val());
        $('.c-check-type').each(function () {
            if($(this).prop('checked')) {
                data.append('type', $(this).data('check'));
            }
        })
        $('.c-check-color').each(function () {
            if($(this).prop('checked')) {
                data.append('color', $(this).data('check'));
            }
        })
        sendAjax(data);
        //showCalcBlock();

    })

    /*Ввод количества страниц*/
    var timeout_id_page;
    $('body').on('keyup', '.c-page', function () {
        data = new FormData();
        data.append('page', $('.c-page').val());
        if($('.c-count').val())
        {
            data.append('count', $('.c-count').val());
            getDataFormWithoutPageCount(data);
            if(timeout_id_page !== undefined) {
                clearTimeout(timeout_id_page);
            }
            timeout_id_page = setTimeout(function() {
                sendAjax(data);
                //showCalcBlock();
            }, 2000);
        }
    })

    /*Ввод количества экземпляров*/
    var timeout_id_count;
    $('body').on('keyup', '.c-count', function () {
        data = new FormData();
        data.append('count', $('.c-count').val());
        if($('.c-page').val())
        {
            data.append('page', $('.c-page').val());
            getDataFormWithoutPageCount(data);
            if(timeout_id_count !== undefined) {
                clearTimeout(timeout_id_count);
            }
            timeout_id_count = setTimeout(function() {
                sendAjax(data);
                //showCalcBlock();
            }, 2000);

        }
    })
}

/**
 * Отправка формы "Отправить заявку"
 */
function clickCalc() {
    data = new FormData();
    data.append('book', download_file);
    data.append('count', $('.c-count').val());
    data.append('page', $('.c-page').val());
    getFormData(data);
    getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_order.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            // $("form[name='printbook_call']").html('');
            // if(data == true) {
            //     $("form[name='printbook_order']").next('.c-success').addClass('show');
            // } else {
            //     $("form[name='printbook_order']").next('.c-error').addClass('show');
            // }
        },
        error: function (data) {

        }
    })
}

/**
 * Отправка сообщения "Перезвоните мне"
 */
function clickCallbackPrintBook() {
    showExampleImg();
    data = new FormData();
    data.append('count', $('.c-count').val());
    data.append('page', $('.c-page').val());
    data.append('call_me_phone', $('.input-phone-call-me').val());
    getFormData(data);
    getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_call_me.php',
        type: 'POST',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            if (data == true) {
                $('.short-form').hide();
                $("form[name='printbook_order']").hide().nextAll('.short-form-success').show();

            } else {
                $('.short-form').hide();
                $("form[name='printbook_order']").hide().nextAll('.short-form-error').show();
            }
        },
        error: function () {

        }
    })


}

function getFormData() {
    data.append('form_name', $('input[name="name"]').val());
    data.append('form_email', $('input[name="email"]').val());
    data.append('form_phone', $('input[name="phone"]').val());
}

function getDataFormWithoutPageCount(data) {
    $('.c-check-size').each(function () {
        if($(this).prop('selected')) {
            data.append('size', $(this).val());
        }
    });
    $('.c-check-type').each(function () {
        if($(this).prop('checked')) {
            data.append('type', $(this).data('check'));
        }
    });
    $('.c-check-color').each(function () {
        if($(this).prop('checked')) {
            data.append('color', $(this).data('check'));
        }
    });
    $('.c-check-cover').each(function () {
        if($(this).prop('checked')) {
            data.append('cover', $(this).data('check'));
        }
    });
    $('.c-check-book').each(function () {
        if($(this).prop('checked')) {
            data.append('book', $(this).data('check'));
        }
    });
}

/*подпихивание данных в нижний серый блок*/
function showCalcBlock(data) {
    if(data.all && data.one) {
        var obj = {
            price: data.one,
            cost: data.all
        };
        price_full = data.all;
        price_one_example = data.one;
        $('.c-calc').find('.c-price').html(obj.price);
        $('.c-calc').find('.c-cost').html(obj.cost);

        $('.item-one-price').show();
        $('.item-full-price').show();
        $('.item-manager').hide();
        $('.c-calc').show();
    }else if(data.call_manager == true) {

        $('.item-one-price').hide();
        $('.item-full-price').hide();
        $('.item-manager').show();
        $('.c-calc').show();

    }
}



function sendAjax(data) {
    showExampleImg();
       $.ajax({
        url: '/ajax/print_book/calc_price.php',
        type: 'POST',
        data: data,
        dataType: 'json',
           processData: false,
           contentType: false,
        success: function (data) {
            $('.c-filter').html(data.html);
            $('input[type="checkbox"], .c-size-select').styler();
            showCalcBlock(data);
        },
       complete: function (data) {
           showExampleImg();
       }
    })
}





