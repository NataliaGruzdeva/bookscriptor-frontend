$(function () {
    initSumRequest();   
    chooseBanks();
    initPopupBanks();
    
    function initPopupBanks() { 
        $('.c-banks').fancybox({
            autoSize: 'false',
            scrolling   : 'auto',
            closeClick  : false,
            minHeight	: 1097,
            openEffect  : 'none',
            closeEffect : 'none',
            closeBtn : false,
            beforeShow: function() {
                $(".fancybox-wrap").focus();

            },
            afterShow: function(){            
                $( ".c-datapicker" ).datepicker({
                    dateFormat: 'd MM yy',               
                    beforeShow: function(input, inst) {
                       $('#ui-datepicker-div').removeClass('custom-datapicker');
                       $('#ui-datepicker-div').addClass('custom-datapicker');
                    },
                    onSelect: function(date) {
                        $(this).val(setRussianDecline(date));
                        $(this).focus();
                    }
                });

                $(".fancybox-inner").addClass('fancybox-height__inn');
                $(".fancybox-wrap").focus();
                $(".fancybox-inner").click();

                if(fancyChoosenBank == 'pasport') {
                    $(".fancybox-inner").addClass('fancybox-height__passport');   
                    $(".fancybox-inner form").addClass('c-passport');             
                } else {  
                    $(".fancybox-inner form").addClass('c-inn');   
                }
            }
        });
    }


    function initSumRequest() {
        $('body').on('click', '.c-send-request', function (e) {
            var $form = $(this).closest(".c-form-request"),
                $input = $form.find('.c-sum-request'),
                $error = $form.find('.c-error-request'),
                $errorBooks = $form.find('.c-error-request-books'),
                payidBooks = $(this).data('payid-books'),
                sum = $input.val();

            e.preventDefault();

            $input.removeClass('error');
            $error.text('');
            $errorBooks.removeClass('active');

            if (parseFloat(payidBooks) < 1 || isNaN(parseFloat(payidBooks))) {
                $errorBooks.addClass('active');
                $input.val(0);
                $input.addClass('error');
                $('.c-send-request').removeClass('allowfancybox');
                return;
            }

            if(parseFloat(sum) < 1500 || isNaN(parseFloat(sum))) {
                $error.text('Введите сумму более 1500 ₽');            
                $input.val(0);
                $input.addClass('error');
                $('.c-send-request').removeClass('allowfancybox');

            } else {  
                $('.c-send-request').addClass('allowfancybox');
                $('.c-send-request.allowfancybox').fancybox({                
                    closeBtn : false,
                    afterShow: function(){
                        $(".c-popup-sum").text(sum);
                    }
                });
            }
        });

        $('body').on('keypress', '.c-only-digit-dot', function (key) {        
            if(key.charCode == 44 || key.charCode == 46) return true;
            if(key.charCode < 48 || key.charCode > 57) return false;
        });    
    }
    
    var fancyChoosenBank = 'inn';
    function chooseBanks() {
        $('body').on('click', '.c-choose-bank', function (e) {
            let $this = $(this),
                showBlock = $this.data('link'),
                $form = $this.closest('form');

            $('.c-bank-block').fadeOut().removeClass('active');
            $(showBlock).fadeIn().addClass('active');
            $(".fancybox-inner").removeClass('fancybox-height__passport');
            $form.removeClass('c-inn').removeClass('c-passport');

            fancyChoosenBank = 'inn';

            if (showBlock == "#pasport") {
                fancyChoosenBank = 'pasport';
                $(".fancybox-inner").addClass('fancybox-height__passport');
                $form.addClass('c-passport');
            } else {
                $form.addClass('c-inn');
            }
        });
    }
});