var validatorPlaneta,
    download_file,
    data;
$(function () {
    $('body').on('click', '.c-load-more', function (e) {
        e.preventDefault();
        sendAjax($(this));
    });
    validateForm();
    validateProfileEditForm();
    validatePopups();
    initPopups();
    fixedHeader();
    animateBlock();
    inputFocus();
    inputLoad();
    contextMenu();
    formatSubmenu();
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");
    var edge = ua.indexOf("Edge ");

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer, return version number
    {
        hackIE();
    }

    if (edge > 0)  // If Internet Explorer, return version number
    {
        hackIE();
    }
    var datepicker = $.fn.datepicker.noConflict();
    $.fn.bootstrapDP = datepicker;
    $.datepicker.regional['ru'];
    
    $('.c-select').select2('destroy');
    $('.c-select').styler();
    $('input[name="phone"]').mask("+7 (999) 999-9999");
    

    if($('.dropzone').length) {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone", {
            maxFiles: 1,
            url: "/ajax/print_book/dropzone_upload.php",
            addRemoveLinks: true,
            dictRemoveFile: "Удалить",
            dictCancelUpload: "Отменить загрузку",
            paramName: "file",
            acceptedFiles: '.docx, .doc, .pdf',
            accept: function(file) {
                download_file = file;
            }
        });
    }

    /*scroll to anchor*/
    goToAnchors($('.c-link-anchor'))

    $(window).scroll(function () {
        $('.c-anchor').each(function() {
            var anchor = $(this)
            scrollWindow(200, anchor)
        })
    })
    
    if(detectIE()) {
        $('.c-ie-img').each(function(){
            var $container = $(this),
                imgUrl = $container.find('img').prop('src');
            if (imgUrl) {
              $container
                .css('backgroundImage', 'url(' + imgUrl + ')')
                .addClass('compat-object-fit');
            }  
        });
    }
});
function formatSubmenu() {
    $('body').on('mouseenter', '[data-role="submenu"] li', function () {

        var countItem = 0;
        countItem = parseInt(countItem);
        var linkSubmenu =  $(this).find('[data-type="submenu"]');
           $(linkSubmenu).each(function () {
                countItem += 1;
            })
        var thisSubmenu = $(this).find('[data-type="submenuParent"]');
        if (countItem > 3) {
            $(thisSubmenu).css('column-count', 2);
        } else if (countItem > 6) {
            $(thisSubmenu).css('column-count', 3);
        }
    });
}

/*переход по якорям*/
function goToAnchors(anchorsLink) {
    $.each(anchorsLink, function () {
        var $links = $('.c-link-anchor')

        var $link = $(this)
        $link.on('click', function () {
            var $elem = $(this)
            $links.removeClass('active-ancor')
            $elem.addClass('active-ancor')
            var $anchor = $('[name="' + $elem.attr('href').replace('#', '') + '"]')
            var destination = ($anchor.offset().top) - 62
            $('html:not(:animated),body:not(:animated)').animate({scrollTop: destination}, 800)

            return false
        })

    })

}

function scrollWindow(count, anchor) {
    if($(window).scrollTop() + count > anchor.offset().top) {
        var name = anchor.attr('name')
        var anchorLink = $('a[href="#' + name + '"]')
        anchorLink.addClass('active-ancor')
        $('.c-link-anchor').not(anchorLink).removeClass('active-ancor')
    }
}
/*интерактивные инпуты*/
function inputFocus() {
    $('body').on('focusout', '.c-input-custom', function () {
        var $input = $(this),
            value = $(this).val(),
            isPhoneValid = true;
        
        if($input.hasClass('c-input-phone')) {
            value = value.replace(/[+(_) -]/g, '');
            (value.length == 11) ? isPhoneValid = true : isPhoneValid = false;
        }
        
        if(value.length == 0 || !isPhoneValid) {
            $input.siblings('.c-label-custom').removeClass('small-label');
        } else {
            $input.siblings('.c-label-custom').addClass('small-label');
        }
    })
}
/*интерактивные инпуты*/
function inputLoad() {
    $('.c-input-custom').each (function () {
        if($(this).val().length == 0) {
            $(this).siblings('.c-label-custom').removeClass('small-label');
        } else {
            $(this).siblings('.c-label-custom').addClass('small-label');
        }
    })
}
/*валидация формы*/
function validateForm() {
    var form = $("form[name='printbook_form']");
    validatorPlaneta = $("form[name='printbook_form']").validate({
        rules: {
            name: "required",
            phone: "required",
            confirm: {
                required: true
            },
            confirmRules: {
                required: true
            },
            file: {
                required: true,
                uploadFile: true
            },
            nameBook: "required",
        },

        messages: {
            name: "Напишите свое имя",
            phone: "Оставьте телефон для связи",
            file: "Прикрепите файл",
            nameBook: "Напишите название своей книги",
            confirm: "Необходимо подтвердить согласие с правилами проекта",
            confirmRules: "Подтвердите согласие с правилами проекта"
                // "Подтвердите согласие с правилами проекта"
        },
        submitHandler: function submitHandler(form) {
            sendFormPlaneta();
            $.fancybox.open({
                content: $('#popup-success-kraud'),
                autoSize: 'false',
                closeClick  : true,
                openEffect  : 'none',
                closeEffect : 'none',
                closeBtn : false
            });
        }
    });

}

function sendFormPlaneta() {
    data = new FormData();
    data.append('name', $('input[name="name"]').val());
    data.append('email', $('input[name="email"]').val());
    data.append('phone', $('input[name="phone"]').val());
    data.append('nameBook', $('input[name="nameBook"]').val());
    data.append('idea', $('input[name="idea"]').val());
    data.append('book', download_file);
    data.append('stepsDone', $('input[name="stepsDone"]:checked').val());
    data.append('ready', $('select[name="ready"] option:selected').val());
    data.append('format', $('input[name="format"]:checked').val());
    data.append('author', $('select[name="author"] option:selected').val());
    console.log(data)
    // getFormData(data);
    // getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            $.fancybox.open({
                content: $('#popup-success-kraud'),
                autoSize: 'false',
                closeClick  : true,
                openEffect  : 'none',
                closeEffect : 'none',
                closeBtn : false
            });
        },
        error: function (data) {

        }
    })
}

function validatePopups() {
    $("form[name='banksEdit']").validate({
        rules: {
            surname: "required",
            name: "required",
            family: "required",            
            bank: "required",
            bik: "required",
            check: "required",             
            inn_num: {
                required: {
                    depends: function (element) {
                        return $("form[name='banksEdit']").hasClass('c-inn');
                    }
                }
            },            
            part: {
                required: {
                    depends: function (element) {
                        return $("form[name='banksEdit']").hasClass('c-passport');
                    }
                }
            },            
            num: {
                required: {
                    depends: function (element) {
                        return $("form[name='banksEdit']").hasClass('c-passport');
                    }
                }
            },            
            date: {
                required: {
                    depends: function (element) {
                        return $("form[name='banksEdit']").hasClass('c-passport');
                    }
                },
                datapickerDate: true
            },            
            who: {
                required: {
                    depends: function (element) {
                        return $("form[name='banksEdit']").hasClass('c-passport');
                    }
                }
            }
            
        },
        messages: {
            surname: "Напишите свою фамилию",
            name: "Напишите свое имя",
            family: "Напишите свое отчество",
            who: 'Необходимо заполнить паспортные данные',
            part: '',
            num: '',
            date: ''
        },
        submitHandler: function submitHandler(form) {
            var $form = $($(this)[0].currentForm),
                $btn = $form.find('input[type="submit"]'),
                date = $('.fancybox-height__passport #date').val();

            $.ajax({
                url: $btn.data('ajax-url'),
                type: 'POST',
                data: $form.serialize()
            }).done(function(data) {
                //дата переведенная с 12 Августа 2017 в 12.08.2017
                //if(date.lenght) {
                //  var newDate = changeDate(date);
                //}
                
                $.fancybox.close();
            }).error(function(data) {
                $.fancybox.close();
            });  
        }
    });
    $("form[name='reportSend']").validate({
        rules: {
            from: "required",
            to: "required",
            email: "required"

        },
        messages: {
            email: "Укажите свой email",
            from: 'Укажите дату',
            to: 'Укажите дату'
        },
        submitHandler: function submitHandler(form) {
            var $form = $($(this)[0].currentForm),
                $btn = $form.find('input[type="submit"]'),
                dateFrom = changeDate($form.find('#from').val()),
                dateTo = changeDate($form.find('#to').val());

            $.ajax({
                url: $btn.data('ajax-url'),
                type: 'POST',
                data: $form.serialize()
            }).done(function(data) {
                $.fancybox.open({
                    content: $('#report-success'),
                    autoSize: 'false',
                    closeClick  : false,
                    openEffect  : 'none',
                    closeEffect : 'none',
                    closeBtn : false

                });

            }).error(function(data) {
                $.fancybox.open({
                    content: $('#report-error'),
                    autoSize: 'false',
                    closeClick  : false,
                    openEffect  : 'none',
                    closeEffect : 'none',
                    closeBtn : false

                });
            });
        }
    });
    
    $.validator.addMethod(
        "datapickerDate",
        function(value, element) {
            var arr = value.split(' '),
                pattern = /^(\d{1,2}) ([а-яА-Я]*) (\d{4})$/g,
                isValid = true;
            
            if (!value.match(pattern)) 
                isValid = false;
            if (arr.length > 1 && !isRusMonth(arr[1]))
                isValid = false;
            
            return isValid;
        },
        "Неверный формат даты"
    );

}

function sendFormPlaneta() {

    data = new FormData();
    data.append('name', $('input[name="name"]').val());
    data.append('email', $('input[name="email"]').val());
    data.append('phone', $('input[name="phone"]').val());
    data.append('nameBook', $('input[name="nameBook"]').val());
    data.append('idea', $('input[name="idea"]').val());
    data.append('book', download_file);
    data.append('stepsDone', $('input[name="stepsDone"]:checked').val());
    data.append('ready', $('select[name="ready"] option:selected').val());
    data.append('format', $('input[name="format"]:checked').val());
    data.append('author', $('select[name="author"] option:selected').val());
    console.log(data)
    // getFormData(data);
    // getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            $.fancybox.open({
                content: $('#popup-success-kraud'),
                autoSize: 'false',
                closeClick  : true,
                openEffect  : 'none',
                closeEffect : 'none',
                closeBtn : false

            });
        }
    });

}

function validateProfileEditForm () {
    $('body').on('submit', "form[name='profileEdit']", function(e) {
        var $this = $(this),
            name = $this.find("#name"),
            email = $this.find("#email"),
            phone = $this.find("#phone"),
            isValidate = true;
               
        clearErrorForm($this);
        
        if(!(email.val()) && !(phone.val())) {
            e.preventDefault(); 
            setErrorToField(phone, "Оставьте телефон для связи");
            setErrorToField(email, "Или оставьте свой email");
        }
        
        if(!(name.val())) {
            e.preventDefault(); 
            setErrorToField(name, "Напишите свое имя");
        }
        
        if (isValidate) { 
            window.onbeforeunload = null;           
            $.ajax({
                url: $this.find('#saveProfile').data('ajax-url'),
                type: 'POST',
                dataType: 'html',
                success: function (data) {
                }
            })

        }
    
    })
    .on('keyup propertychange',"form[name='profileEdit'] .c-input-custom", function(e) {
        var $this = $(this);
        
        if($this.val()) {
            clearErrorField($this);
        }
        
    });
}

function setErrorToField(field, text) {
    field.addClass('error');
    field.siblings('.c-label-error').addClass('error').text(text);
    window.scrollTo(0, field.offset().top - 150);
}

function clearErrorField(field) {
    field.removeClass('error');
    field.siblings('.c-label-error').removeClass('error').text('');
}

function clearErrorForm(form) {
    form.find('.error').not('.c-label-error').each(function() {
        clearErrorField($(this));
    });
}

function sendAjax(elem) {
    $('.c-preloader').show();
    $(elem).addClass('preload');
    $.ajax({
        url: elem.data('ajax-url'),
        type: 'GET',
        dataType: 'html',
        success: function (data) {
            var count = 0;
            $('.c-review__item').each(function () {
                count += 1;
            });
            if(count < 10) {
                $('.c-review').append(data);
                $('.c-preloader').hide();
                $(elem).removeClass('preload');
            } else {
                $('.c-load-more').hide();
                $('.c-preloader').hide();
            }
        },

    })
}

function contextMenu() {
    $('body').on('click', '.c-link-contextmenu', function (e) {
        e.preventDefault();
        $(this).find('.c-content-contextmenu').toggleClass('visible');

    })
    $('body').on('mouseleave', '.c-link-contextmenu', function () {
        $(this).find('.c-content-contextmenu').removeClass('visible');
    })

}

function fixedHeader() {
    $(window).scroll(function () {
        if ($(window).scrollTop() >= 50) {
            $('.c-header').addClass('fixed-header')
            $('.c-header-alternate').show()
        } else if ($(window).scrollTop() < 50) {
            $('.c-header').removeClass('fixed-header')
            $('.c-header-alternate').hide()
        }
    })
}

function animateBlock() {
    if($('.c-start-animate').length) {
        var scrollLenght = $('.c-start-animate').offset().top - $(window).height();
        $(window).scroll(function () {
            if ($(window).scrollTop() >= scrollLenght) {
                $('.c-animate').each(function () {
                    $(this).addClass('animate');
                })
            }
        })
    }
}

function initPopups() {
    $("a.popupbox-video").fancybox({
        padding: 0,
        width: '980px',
        autoSize: 'false'
    });

    $(".c-popup-confirm").fancybox({
        width: '560px',
        autoSize: 'false',
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        closeBtn : false
    });

    $(".c-popup-kraud").fancybox({
        width: '736px',
        autoSize: 'false',
        closeClick  : true,
        openEffect  : 'none',
        closeEffect : 'none',
        closeBtn : false
    });

    $(".c-load-avatar").fancybox({
        width: '560px',
        autoSize: 'false',
        content: $("#popup-avatar"),
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
    });

    $(".c-send-report").fancybox({
        content: $('#report'),
        autoSize: 'false',
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        closeBtn : false,        
        afterShow   : function() {        
            var $datefrom = $('#report').find('.c-report-from'),
                dateFrom = $datefrom.data('registration'),
                $dateTo = $('#report').find('.c-report-to'),
                today = getRusDateToday();
            
            $datefrom.val(dateFrom);
            $dateTo.val(today);
            
            $( ".c-datapicker" ).datepicker({
                dateFormat: 'd MM yy',                
                beforeShow: function(input, inst) {
                   $('#ui-datepicker-div').removeClass('custom-datapicker');
                   $('#ui-datepicker-div').addClass('custom-datapicker');
                },
                onSelect: function(date) {
                    $(this).val(setRussianDecline(date));
                }
            });
        }
    });
    
    $(".c-report-success").fancybox({
        content: $('#report-success'),
        autoSize: 'false',
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        closeBtn : false
    });
    
    $(".c-report-error").fancybox({
        content: $('#report-error'),
        autoSize: 'false',
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        closeBtn : false
    });
    
    $('.c-send-request.allowfancybox').fancybox({
        autoSize: 'false',
        closeClick  : false,
        openEffect  : 'none',
        closeEffect : 'none',
        closeBtn : false
    });
}


function getRusDateToday() {
    var today = new Date().toLocaleString('ru', {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
              });
            
    today = today.replace(' г.','');
    var arr = today.split(' ');
    arr[1] = arr[1].replace(arr[1][0], arr[1][0].toUpperCase());
    
    return arr.join(' ');
}

function setRussianDecline(date) {
    var arr = date.split(' '),
        declines = {
            'Янв': 'Января',
            'Фев': 'Февраля',
            'Мар': 'Марта',
            'Апр': 'Апреля',
            'Май': 'Мая',
            'Июн': 'Июня',
            'Июл': 'Июля',
            'Авг': 'Августа',
            'Сен': 'Сентября',
            'Окт': 'Октября',
            'Ноя': 'Ноября',
            'Дек': 'Декабря'
        },
        month = arr[1].slice(0, 3);
    
    arr[1] = declines[month];
    
    return arr.join(' ');
}

function isRusMonth(month) {
    var months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
                  'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        declinemonths = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля',
                  'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'];
    return months.indexOf(month) > -1 || declinemonths.indexOf(month) > -1; 
}

function changeDate(date) {
    var declinemonths = ['Января', 'Февраля', 'Марта', 'Апреля', 'Мая', 'Июня', 'Июля',
                  'Августа', 'Сентября', 'Октября', 'Ноября', 'Декабря'],
        arr = date.split(' ');
        if (arr[0].length == 1) 
            arr[0] = '0' + arr[0];
        arr[1] = declinemonths.indexOf(arr[1]) + 1;
        if (arr[1] < 10) 
            arr[1] = '0' + arr[1];
    
    return arr.join('.');    
}

function detectIE() {
    //All Standards and Compatibility Mode
    var uA = window.navigator.userAgent,
        onlyIEorEdge = /msie\s|trident\/|edge\//i.test(uA) && !!( document.uniqueID || window.MSInputMethodContext),
        checkVersion = (onlyIEorEdge && +(/(edge\/|rv:|msie\s)([\d.]+)/i.exec(uA)[2])) || NaN;
    
    return !isNaN(checkVersion);
}

function hackIE() {
    var head  = document.getElementsByTagName("head")[0];
    var link  = document.createElement("link");
    link.rel  = "stylesheet";
    link.href = "/css/ie.css";
    head.appendChild(link);
}