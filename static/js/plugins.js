/*!
	Autosize 4.0.0
	license: MIT
	http://www.jacklmoore.com/autosize
*/
!function(e,t){if("function"==typeof define&&define.amd)define(["exports","module"],t);else if("undefined"!=typeof exports&&"undefined"!=typeof module)t(exports,module);else{var n={exports:{}};t(n.exports,n),e.autosize=n.exports}}(this,function(e,t){"use strict";function n(e){function t(){var t=window.getComputedStyle(e,null);"vertical"===t.resize?e.style.resize="none":"both"===t.resize&&(e.style.resize="horizontal"),s="content-box"===t.boxSizing?-(parseFloat(t.paddingTop)+parseFloat(t.paddingBottom)):parseFloat(t.borderTopWidth)+parseFloat(t.borderBottomWidth),isNaN(s)&&(s=0),l()}function n(t){var n=e.style.width;e.style.width="0px",e.offsetWidth,e.style.width=n,e.style.overflowY=t}function o(e){for(var t=[];e&&e.parentNode&&e.parentNode instanceof Element;)e.parentNode.scrollTop&&t.push({node:e.parentNode,scrollTop:e.parentNode.scrollTop}),e=e.parentNode;return t}function r(){var t=e.style.height,n=o(e),r=document.documentElement&&document.documentElement.scrollTop;e.style.height="";var i=e.scrollHeight+s;return 0===e.scrollHeight?void(e.style.height=t):(e.style.height=i+"px",u=e.clientWidth,n.forEach(function(e){e.node.scrollTop=e.scrollTop}),void(r&&(document.documentElement.scrollTop=r)))}function l(){r();var t=Math.round(parseFloat(e.style.height)),o=window.getComputedStyle(e,null),i="content-box"===o.boxSizing?Math.round(parseFloat(o.height)):e.offsetHeight;if(i!==t?"hidden"===o.overflowY&&(n("scroll"),r(),i="content-box"===o.boxSizing?Math.round(parseFloat(window.getComputedStyle(e,null).height)):e.offsetHeight):"hidden"!==o.overflowY&&(n("hidden"),r(),i="content-box"===o.boxSizing?Math.round(parseFloat(window.getComputedStyle(e,null).height)):e.offsetHeight),a!==i){a=i;var l=d("autosize:resized");try{e.dispatchEvent(l)}catch(e){}}}if(e&&e.nodeName&&"TEXTAREA"===e.nodeName&&!i.has(e)){var s=null,u=e.clientWidth,a=null,c=function(){e.clientWidth!==u&&l()},p=function(t){window.removeEventListener("resize",c,!1),e.removeEventListener("input",l,!1),e.removeEventListener("keyup",l,!1),e.removeEventListener("autosize:destroy",p,!1),e.removeEventListener("autosize:update",l,!1),Object.keys(t).forEach(function(n){e.style[n]=t[n]}),i.delete(e)}.bind(e,{height:e.style.height,resize:e.style.resize,overflowY:e.style.overflowY,overflowX:e.style.overflowX,wordWrap:e.style.wordWrap});e.addEventListener("autosize:destroy",p,!1),"onpropertychange"in e&&"oninput"in e&&e.addEventListener("keyup",l,!1),window.addEventListener("resize",c,!1),e.addEventListener("input",l,!1),e.addEventListener("autosize:update",l,!1),e.style.overflowX="hidden",e.style.wordWrap="break-word",i.set(e,{destroy:p,update:l}),t()}}function o(e){var t=i.get(e);t&&t.destroy()}function r(e){var t=i.get(e);t&&t.update()}var i="function"==typeof Map?new Map:function(){var e=[],t=[];return{has:function(t){return e.indexOf(t)>-1},get:function(n){return t[e.indexOf(n)]},set:function(n,o){e.indexOf(n)===-1&&(e.push(n),t.push(o))},delete:function(n){var o=e.indexOf(n);o>-1&&(e.splice(o,1),t.splice(o,1))}}}(),d=function(e){return new Event(e,{bubbles:!0})};try{new Event("test")}catch(e){d=function(e){var t=document.createEvent("Event");return t.initEvent(e,!0,!1),t}}var l=null;"undefined"==typeof window||"function"!=typeof window.getComputedStyle?(l=function(e){return e},l.destroy=function(e){return e},l.update=function(e){return e}):(l=function(e,t){return e&&Array.prototype.forEach.call(e.length?e:[e],function(e){return n(e,t)}),e},l.destroy=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],o),e},l.update=function(e){return e&&Array.prototype.forEach.call(e.length?e:[e],r),e}),t.exports=l});


/*
 *
 * More info at [www.dropzonejs.com](http://www.dropzonejs.com)
 *
 * Copyright (c) 2012, Matias Meno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

(function() {
    var Dropzone, Emitter, ExifRestore, camelize, contentLoaded, detectVerticalSquash, drawImageIOSFix, noop, without,
        slice = [].slice,
        extend1 = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
        hasProp = {}.hasOwnProperty;

    noop = function() {};

    Emitter = (function() {
        function Emitter() {}

        Emitter.prototype.addEventListener = Emitter.prototype.on;

        Emitter.prototype.on = function(event, fn) {
            this._callbacks = this._callbacks || {};
            if (!this._callbacks[event]) {
                this._callbacks[event] = [];
            }
            this._callbacks[event].push(fn);
            return this;
        };

        Emitter.prototype.emit = function() {
            var args, callback, callbacks, event, j, len;
            event = arguments[0], args = 2 <= arguments.length ? slice.call(arguments, 1) : [];
            this._callbacks = this._callbacks || {};
            callbacks = this._callbacks[event];
            if (callbacks) {
                for (j = 0, len = callbacks.length; j < len; j++) {
                    callback = callbacks[j];
                    callback.apply(this, args);
                }
            }
            return this;
        };

        Emitter.prototype.removeListener = Emitter.prototype.off;

        Emitter.prototype.removeAllListeners = Emitter.prototype.off;

        Emitter.prototype.removeEventListener = Emitter.prototype.off;

        Emitter.prototype.off = function(event, fn) {
            var callback, callbacks, i, j, len;
            if (!this._callbacks || arguments.length === 0) {
                this._callbacks = {};
                return this;
            }
            callbacks = this._callbacks[event];
            if (!callbacks) {
                return this;
            }
            if (arguments.length === 1) {
                delete this._callbacks[event];
                return this;
            }
            for (i = j = 0, len = callbacks.length; j < len; i = ++j) {
                callback = callbacks[i];
                if (callback === fn) {
                    callbacks.splice(i, 1);
                    break;
                }
            }
            return this;
        };

        return Emitter;

    })();

    Dropzone = (function(superClass) {
        var extend, resolveOption;

        extend1(Dropzone, superClass);

        Dropzone.prototype.Emitter = Emitter;


        /*
         This is a list of all available events you can register on a dropzone object.

         You can register an event handler like this:

         dropzone.on("dragEnter", function() { });
         */

        Dropzone.prototype.events = ["drop", "dragstart", "dragend", "dragenter", "dragover", "dragleave", "addedfile", "addedfiles", "removedfile", "thumbnail", "error", "errormultiple", "processing", "processingmultiple", "uploadprogress", "totaluploadprogress", "sending", "sendingmultiple", "success", "successmultiple", "canceled", "canceledmultiple", "complete", "completemultiple", "reset", "maxfilesexceeded", "maxfilesreached", "queuecomplete"];

        Dropzone.prototype.defaultOptions = {
            url: null,
            method: "post",
            withCredentials: false,
            timeout: 30000,
            parallelUploads: 2,
            uploadMultiple: false,
            maxFilesize: 256,
            paramName: "file",
            createImageThumbnails: true,
            maxThumbnailFilesize: 10,
            thumbnailWidth: 120,
            thumbnailHeight: 120,
            thumbnailMethod: 'crop',
            resizeWidth: null,
            resizeHeight: null,
            resizeMimeType: null,
            resizeQuality: 0.8,
            resizeMethod: 'contain',
            filesizeBase: 1000,
            maxFiles: null,
            params: {},
            headers: null,
            clickable: true,
            ignoreHiddenFiles: true,
            acceptedFiles: null,
            acceptedMimeTypes: null,
            autoProcessQueue: true,
            autoQueue: true,
            addRemoveLinks: false,
            previewsContainer: null,
            hiddenInputContainer: "body",
            capture: null,
            renameFilename: null,
            renameFile: null,
            forceFallback: false,
            dictDefaultMessage: "Drop files here to upload",
            dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
            dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
            dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",
            dictInvalidFileType: "You can't upload files of this type.",
            dictResponseError: "Server responded with {{statusCode}} code.",
            dictCancelUpload: "Cancel upload",
            dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
            dictRemoveFile: "Remove file",
            dictRemoveFileConfirmation: null,
            dictMaxFilesExceeded: "You can not upload any more files.",
            dictFileSizeUnits: {
                tb: "TB",
                gb: "GB",
                mb: "MB",
                kb: "KB",
                b: "b"
            },
            init: function() {
                return noop;
            },
            accept: function(file, done) {
                return done();
            },
            fallback: function() {
                var child, j, len, messageElement, ref, span;
                this.element.className = this.element.className + " dz-browser-not-supported";
                ref = this.element.getElementsByTagName("div");
                for (j = 0, len = ref.length; j < len; j++) {
                    child = ref[j];
                    if (/(^| )dz-message($| )/.test(child.className)) {
                        messageElement = child;
                        child.className = "dz-message";
                        continue;
                    }
                }
                if (!messageElement) {
                    messageElement = Dropzone.createElement("<div class=\"dz-message\"><span></span></div>");
                    this.element.appendChild(messageElement);
                }
                span = messageElement.getElementsByTagName("span")[0];
                if (span) {
                    if (span.textContent != null) {
                        span.textContent = this.options.dictFallbackMessage;
                    } else if (span.innerText != null) {
                        span.innerText = this.options.dictFallbackMessage;
                    }
                }
                return this.element.appendChild(this.getFallbackForm());
            },
            resize: function(file, width, height, resizeMethod) {
                var info, srcRatio, trgRatio;
                info = {
                    srcX: 0,
                    srcY: 0,
                    srcWidth: file.width,
                    srcHeight: file.height
                };
                srcRatio = file.width / file.height;
                if ((width == null) && (height == null)) {
                    width = info.srcWidth;
                    height = info.srcHeight;
                } else if (width == null) {
                    width = height * srcRatio;
                } else if (height == null) {
                    height = width / srcRatio;
                }
                width = Math.min(width, info.srcWidth);
                height = Math.min(height, info.srcHeight);
                trgRatio = width / height;
                if (info.srcWidth > width || info.srcHeight > height) {
                    if (resizeMethod === 'crop') {
                        if (srcRatio > trgRatio) {
                            info.srcHeight = file.height;
                            info.srcWidth = info.srcHeight * trgRatio;
                        } else {
                            info.srcWidth = file.width;
                            info.srcHeight = info.srcWidth / trgRatio;
                        }
                    } else if (resizeMethod === 'contain') {
                        if (srcRatio > trgRatio) {
                            height = width / srcRatio;
                        } else {
                            width = height * srcRatio;
                        }
                    } else {
                        throw new Error("Unknown resizeMethod '" + resizeMethod + "'");
                    }
                }
                info.srcX = (file.width - info.srcWidth) / 2;
                info.srcY = (file.height - info.srcHeight) / 2;
                info.trgWidth = width;
                info.trgHeight = height;
                return info;
            },
            transformFile: function(file, done) {
                if ((this.options.resizeWidth || this.options.resizeHeight) && file.type.match(/image.*/)) {
                    return this.resizeImage(file, this.options.resizeWidth, this.options.resizeHeight, this.options.resizeMethod, done);
                } else {
                    return done(file);
                }
            },
            previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-image\"><img data-dz-thumbnail /></div>\n  <div class=\"dz-details\">\n    <div class=\"dz-size\"><span data-dz-size></span></div>\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n  </div>\n  <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n  <div class=\"dz-success-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Check</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <path d=\"M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" stroke-opacity=\"0.198794158\" stroke=\"#747474\" fill-opacity=\"0.816519475\" fill=\"#FFFFFF\" sketch:type=\"MSShapeGroup\"></path>\n      </g>\n    </svg>\n  </div>\n  <div class=\"dz-error-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Error</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <g id=\"Check-+-Oval-2\" sketch:type=\"MSLayerGroup\" stroke=\"#747474\" stroke-opacity=\"0.198794158\" fill=\"#FFFFFF\" fill-opacity=\"0.816519475\">\n          <path d=\"M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" sketch:type=\"MSShapeGroup\"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>",

            /*
             Those functions register themselves to the events on init and handle all
             the user interface specific stuff. Overwriting them won't break the upload
             but can break the way it's displayed.
             You can overwrite them if you don't like the default behavior. If you just
             want to add an additional event handler, register it on the dropzone object
             and don't overwrite those options.
             */
            drop: function(e) {
                return this.element.classList.remove("dz-drag-hover");
            },
            dragstart: noop,
            dragend: function(e) {
                return this.element.classList.remove("dz-drag-hover");
            },
            dragenter: function(e) {
                return this.element.classList.add("dz-drag-hover");
            },
            dragover: function(e) {
                return this.element.classList.add("dz-drag-hover");
            },
            dragleave: function(e) {
                return this.element.classList.remove("dz-drag-hover");
            },
            paste: noop,
            reset: function() {
                return this.element.classList.remove("dz-started");
            },
            addedfile: function(file) {
                var j, k, l, len, len1, len2, node, ref, ref1, ref2, removeFileEvent, removeLink, results;
                if (this.element === this.previewsContainer) {
                    this.element.classList.add("dz-started");
                }
                if (this.previewsContainer) {
                    file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
                    file.previewTemplate = file.previewElement;
                    this.previewsContainer.appendChild(file.previewElement);
                    ref = file.previewElement.querySelectorAll("[data-dz-name]");
                    for (j = 0, len = ref.length; j < len; j++) {
                        node = ref[j];
                        node.textContent = file.name;
                    }
                    ref1 = file.previewElement.querySelectorAll("[data-dz-size]");
                    for (k = 0, len1 = ref1.length; k < len1; k++) {
                        node = ref1[k];
                        node.innerHTML = this.filesize(file.size);
                    }
                    if (this.options.addRemoveLinks) {
                        file._removeLink = Dropzone.createElement("<a class=\"dz-remove\" href=\"javascript:undefined;\" data-dz-remove>" + this.options.dictRemoveFile + "</a>");
                        file.previewElement.appendChild(file._removeLink);
                    }
                    removeFileEvent = (function(_this) {
                        return function(e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (file.status === Dropzone.UPLOADING) {
                                return Dropzone.confirm(_this.options.dictCancelUploadConfirmation, function() {
                                    return _this.removeFile(file);
                                });
                            } else {
                                if (_this.options.dictRemoveFileConfirmation) {
                                    return Dropzone.confirm(_this.options.dictRemoveFileConfirmation, function() {
                                        return _this.removeFile(file);
                                    });
                                } else {
                                    return _this.removeFile(file);
                                }
                            }
                        };
                    })(this);
                    ref2 = file.previewElement.querySelectorAll("[data-dz-remove]");
                    results = [];
                    for (l = 0, len2 = ref2.length; l < len2; l++) {
                        removeLink = ref2[l];
                        results.push(removeLink.addEventListener("click", removeFileEvent));
                    }
                    return results;
                }
            },
            removedfile: function(file) {
                var ref;
                if (file.previewElement) {
                    if ((ref = file.previewElement) != null) {
                        ref.parentNode.removeChild(file.previewElement);
                    }
                }
                return this._updateMaxFilesReachedClass();
            },
            thumbnail: function(file, dataUrl) {
                var j, len, ref, thumbnailElement;
                if (file.previewElement) {
                    file.previewElement.classList.remove("dz-file-preview");
                    ref = file.previewElement.querySelectorAll("[data-dz-thumbnail]");
                    for (j = 0, len = ref.length; j < len; j++) {
                        thumbnailElement = ref[j];
                        thumbnailElement.alt = file.name;
                        thumbnailElement.src = dataUrl;
                    }
                    return setTimeout(((function(_this) {
                        return function() {
                            return file.previewElement.classList.add("dz-image-preview");
                        };
                    })(this)), 1);
                }
            },
            error: function(file, message) {
                var j, len, node, ref, results;
                if (file.previewElement) {
                    file.previewElement.classList.add("dz-error");
                    if (typeof message !== "String" && message.error) {
                        message = message.error;
                    }
                    ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
                    results = [];
                    for (j = 0, len = ref.length; j < len; j++) {
                        node = ref[j];
                        results.push(node.textContent = message);
                    }
                    return results;
                }
            },
            errormultiple: noop,
            processing: function(file) {
                if (file.previewElement) {
                    file.previewElement.classList.add("dz-processing");
                    if (file._removeLink) {
                        return file._removeLink.textContent = this.options.dictCancelUpload;
                    }
                }
            },
            processingmultiple: noop,
            uploadprogress: function(file, progress, bytesSent) {
                var j, len, node, ref, results;
                if (file.previewElement) {
                    ref = file.previewElement.querySelectorAll("[data-dz-uploadprogress]");
                    results = [];
                    for (j = 0, len = ref.length; j < len; j++) {
                        node = ref[j];
                        if (node.nodeName === 'PROGRESS') {
                            results.push(node.value = progress);
                        } else {
                            results.push(node.style.width = progress + "%");
                        }
                    }
                    return results;
                }
            },
            totaluploadprogress: noop,
            sending: noop,
            sendingmultiple: noop,
            success: function(file) {
                if (file.previewElement) {
                    return file.previewElement.classList.add("dz-success");
                }
            },
            successmultiple: noop,
            canceled: function(file) {
                return this.emit("error", file, "Upload canceled.");
            },
            canceledmultiple: noop,
            complete: function(file) {
                if (file._removeLink) {
                    file._removeLink.textContent = this.options.dictRemoveFile;
                }
                if (file.previewElement) {
                    return file.previewElement.classList.add("dz-complete");
                }
            },
            completemultiple: noop,
            maxfilesexceeded: noop,
            maxfilesreached: noop,
            queuecomplete: noop,
            addedfiles: noop
        };

        extend = function() {
            var j, key, len, object, objects, target, val;
            target = arguments[0], objects = 2 <= arguments.length ? slice.call(arguments, 1) : [];
            for (j = 0, len = objects.length; j < len; j++) {
                object = objects[j];
                for (key in object) {
                    val = object[key];
                    target[key] = val;
                }
            }
            return target;
        };

        function Dropzone(element1, options) {
            var elementOptions, fallback, ref;
            this.element = element1;
            this.version = Dropzone.version;
            this.defaultOptions.previewTemplate = this.defaultOptions.previewTemplate.replace(/\n*/g, "");
            this.clickableElements = [];
            this.listeners = [];
            this.files = [];
            if (typeof this.element === "string") {
                this.element = document.querySelector(this.element);
            }
            if (!(this.element && (this.element.nodeType != null))) {
                throw new Error("Invalid dropzone element.");
            }
            if (this.element.dropzone) {
                throw new Error("Dropzone already attached.");
            }
            Dropzone.instances.push(this);
            this.element.dropzone = this;
            elementOptions = (ref = Dropzone.optionsForElement(this.element)) != null ? ref : {};
            this.options = extend({}, this.defaultOptions, elementOptions, options != null ? options : {});
            if (this.options.forceFallback || !Dropzone.isBrowserSupported()) {
                return this.options.fallback.call(this);
            }
            if (this.options.url == null) {
                this.options.url = this.element.getAttribute("action");
            }
            if (!this.options.url) {
                throw new Error("No URL provided.");
            }
            if (this.options.acceptedFiles && this.options.acceptedMimeTypes) {
                throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");
            }
            if (this.options.acceptedMimeTypes) {
                this.options.acceptedFiles = this.options.acceptedMimeTypes;
                delete this.options.acceptedMimeTypes;
            }
            if (this.options.renameFilename != null) {
                this.options.renameFile = (function(_this) {
                    return function(file) {
                        return _this.options.renameFilename.call(_this, file.name, file);
                    };
                })(this);
            }
            this.options.method = this.options.method.toUpperCase();
            if ((fallback = this.getExistingFallback()) && fallback.parentNode) {
                fallback.parentNode.removeChild(fallback);
            }
            if (this.options.previewsContainer !== false) {
                if (this.options.previewsContainer) {
                    this.previewsContainer = Dropzone.getElement(this.options.previewsContainer, "previewsContainer");
                } else {
                    this.previewsContainer = this.element;
                }
            }
            if (this.options.clickable) {
                if (this.options.clickable === true) {
                    this.clickableElements = [this.element];
                } else {
                    this.clickableElements = Dropzone.getElements(this.options.clickable, "clickable");
                }
            }
            this.init();
        }

        Dropzone.prototype.getAcceptedFiles = function() {
            var file, j, len, ref, results;
            ref = this.files;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                file = ref[j];
                if (file.accepted) {
                    results.push(file);
                }
            }
            return results;
        };

        Dropzone.prototype.getRejectedFiles = function() {
            var file, j, len, ref, results;
            ref = this.files;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                file = ref[j];
                if (!file.accepted) {
                    results.push(file);
                }
            }
            return results;
        };

        Dropzone.prototype.getFilesWithStatus = function(status) {
            var file, j, len, ref, results;
            ref = this.files;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                file = ref[j];
                if (file.status === status) {
                    results.push(file);
                }
            }
            return results;
        };

        Dropzone.prototype.getQueuedFiles = function() {
            return this.getFilesWithStatus(Dropzone.QUEUED);
        };

        Dropzone.prototype.getUploadingFiles = function() {
            return this.getFilesWithStatus(Dropzone.UPLOADING);
        };

        Dropzone.prototype.getAddedFiles = function() {
            return this.getFilesWithStatus(Dropzone.ADDED);
        };

        Dropzone.prototype.getActiveFiles = function() {
            var file, j, len, ref, results;
            ref = this.files;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                file = ref[j];
                if (file.status === Dropzone.UPLOADING || file.status === Dropzone.QUEUED) {
                    results.push(file);
                }
            }
            return results;
        };

        Dropzone.prototype.init = function() {
            var eventName, j, len, noPropagation, ref, ref1, setupHiddenFileInput;
            if (this.element.tagName === "form") {
                this.element.setAttribute("enctype", "multipart/form-data");
            }
            if (this.element.classList.contains("dropzone") && !this.element.querySelector(".dz-message")) {
                this.element.appendChild(Dropzone.createElement("<div class=\"dz-default dz-message\"><span>" + this.options.dictDefaultMessage + "</span></div>"));
            }
            if (this.clickableElements.length) {
                setupHiddenFileInput = (function(_this) {
                    return function() {
                        if (_this.hiddenFileInput) {
                            _this.hiddenFileInput.parentNode.removeChild(_this.hiddenFileInput);
                        }
                        _this.hiddenFileInput = document.createElement("input");
                        _this.hiddenFileInput.setAttribute("type", "file");
                        _this.hiddenFileInput.setAttribute("name", "file");
                        if ((_this.options.maxFiles == null) || _this.options.maxFiles > 1) {
                            _this.hiddenFileInput.setAttribute("multiple", "multiple");
                        }
                        _this.hiddenFileInput.className = "dz-hidden-input";
                        if (_this.options.acceptedFiles != null) {
                            _this.hiddenFileInput.setAttribute("accept", _this.options.acceptedFiles);
                        }
                        if (_this.options.capture != null) {
                            _this.hiddenFileInput.setAttribute("capture", _this.options.capture);
                        }
                        _this.hiddenFileInput.style.visibility = "hidden";
                        _this.hiddenFileInput.style.position = "absolute";
                        _this.hiddenFileInput.style.top = "0";
                        _this.hiddenFileInput.style.left = "0";
                        _this.hiddenFileInput.style.height = "0";
                        _this.hiddenFileInput.style.width = "0";
                        document.querySelector(_this.options.hiddenInputContainer).appendChild(_this.hiddenFileInput);
                        return _this.hiddenFileInput.addEventListener("change", function() {
                            var file, files, j, len;
                            files = _this.hiddenFileInput.files;
                            if (files.length) {
                                for (j = 0, len = files.length; j < len; j++) {
                                    file = files[j];
                                    _this.addFile(file);
                                }
                            }
                            _this.emit("addedfiles", files);
                            return setupHiddenFileInput();
                        });
                    };
                })(this);
                setupHiddenFileInput();
            }
            this.URL = (ref = window.URL) != null ? ref : window.webkitURL;
            ref1 = this.events;
            for (j = 0, len = ref1.length; j < len; j++) {
                eventName = ref1[j];
                this.on(eventName, this.options[eventName]);
            }
            this.on("uploadprogress", (function(_this) {
                return function() {
                    return _this.updateTotalUploadProgress();
                };
            })(this));
            this.on("removedfile", (function(_this) {
                return function() {
                    return _this.updateTotalUploadProgress();
                };
            })(this));
            this.on("canceled", (function(_this) {
                return function(file) {
                    return _this.emit("complete", file);
                };
            })(this));
            this.on("complete", (function(_this) {
                return function(file) {
                    if (_this.getAddedFiles().length === 0 && _this.getUploadingFiles().length === 0 && _this.getQueuedFiles().length === 0) {
                        return setTimeout((function() {
                            return _this.emit("queuecomplete");
                        }), 0);
                    }
                };
            })(this));
            noPropagation = function(e) {
                e.stopPropagation();
                if (e.preventDefault) {
                    return e.preventDefault();
                } else {
                    return e.returnValue = false;
                }
            };
            this.listeners = [
                {
                    element: this.element,
                    events: {
                        "dragstart": (function(_this) {
                            return function(e) {
                                return _this.emit("dragstart", e);
                            };
                        })(this),
                        "dragenter": (function(_this) {
                            return function(e) {
                                noPropagation(e);
                                return _this.emit("dragenter", e);
                            };
                        })(this),
                        "dragover": (function(_this) {
                            return function(e) {
                                var efct;
                                try {
                                    efct = e.dataTransfer.effectAllowed;
                                } catch (undefined) {}
                                e.dataTransfer.dropEffect = 'move' === efct || 'linkMove' === efct ? 'move' : 'copy';
                                noPropagation(e);
                                return _this.emit("dragover", e);
                            };
                        })(this),
                        "dragleave": (function(_this) {
                            return function(e) {
                                return _this.emit("dragleave", e);
                            };
                        })(this),
                        "drop": (function(_this) {
                            return function(e) {
                                noPropagation(e);
                                return _this.drop(e);
                            };
                        })(this),
                        "dragend": (function(_this) {
                            return function(e) {
                                return _this.emit("dragend", e);
                            };
                        })(this)
                    }
                }
            ];
            this.clickableElements.forEach((function(_this) {
                return function(clickableElement) {
                    return _this.listeners.push({
                        element: clickableElement,
                        events: {
                            "click": function(evt) {
                                if ((clickableElement !== _this.element) || (evt.target === _this.element || Dropzone.elementInside(evt.target, _this.element.querySelector(".dz-message")))) {
                                    _this.hiddenFileInput.click();
                                }
                                return true;
                            }
                        }
                    });
                };
            })(this));
            this.enable();
            return this.options.init.call(this);
        };

        Dropzone.prototype.destroy = function() {
            var ref;
            this.disable();
            this.removeAllFiles(true);
            if ((ref = this.hiddenFileInput) != null ? ref.parentNode : void 0) {
                this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput);
                this.hiddenFileInput = null;
            }
            delete this.element.dropzone;
            return Dropzone.instances.splice(Dropzone.instances.indexOf(this), 1);
        };

        Dropzone.prototype.updateTotalUploadProgress = function() {
            var activeFiles, file, j, len, ref, totalBytes, totalBytesSent, totalUploadProgress;
            totalBytesSent = 0;
            totalBytes = 0;
            activeFiles = this.getActiveFiles();
            if (activeFiles.length) {
                ref = this.getActiveFiles();
                for (j = 0, len = ref.length; j < len; j++) {
                    file = ref[j];
                    totalBytesSent += file.upload.bytesSent;
                    totalBytes += file.upload.total;
                }
                totalUploadProgress = 100 * totalBytesSent / totalBytes;
            } else {
                totalUploadProgress = 100;
            }
            return this.emit("totaluploadprogress", totalUploadProgress, totalBytes, totalBytesSent);
        };

        Dropzone.prototype._getParamName = function(n) {
            if (typeof this.options.paramName === "function") {
                return this.options.paramName(n);
            } else {
                return "" + this.options.paramName + (this.options.uploadMultiple ? "[" + n + "]" : "");
            }
        };

        Dropzone.prototype._renameFile = function(file) {
            if (typeof this.options.renameFile !== "function") {
                return file.name;
            }
            return this.options.renameFile(file);
        };

        Dropzone.prototype.getFallbackForm = function() {
            var existingFallback, fields, fieldsString, form;
            if (existingFallback = this.getExistingFallback()) {
                return existingFallback;
            }
            fieldsString = "<div class=\"dz-fallback\">";
            if (this.options.dictFallbackText) {
                fieldsString += "<p>" + this.options.dictFallbackText + "</p>";
            }
            fieldsString += "<input type=\"file\" name=\"" + (this._getParamName(0)) + "\" " + (this.options.uploadMultiple ? 'multiple="multiple"' : void 0) + " /><input type=\"submit\" value=\"Upload!\"></div>";
            fields = Dropzone.createElement(fieldsString);
            if (this.element.tagName !== "FORM") {
                form = Dropzone.createElement("<form action=\"" + this.options.url + "\" enctype=\"multipart/form-data\" method=\"" + this.options.method + "\"></form>");
                form.appendChild(fields);
            } else {
                this.element.setAttribute("enctype", "multipart/form-data");
                this.element.setAttribute("method", this.options.method);
            }
            return form != null ? form : fields;
        };

        Dropzone.prototype.getExistingFallback = function() {
            var fallback, getFallback, j, len, ref, tagName;
            getFallback = function(elements) {
                var el, j, len;
                for (j = 0, len = elements.length; j < len; j++) {
                    el = elements[j];
                    if (/(^| )fallback($| )/.test(el.className)) {
                        return el;
                    }
                }
            };
            ref = ["div", "form"];
            for (j = 0, len = ref.length; j < len; j++) {
                tagName = ref[j];
                if (fallback = getFallback(this.element.getElementsByTagName(tagName))) {
                    return fallback;
                }
            }
        };

        Dropzone.prototype.setupEventListeners = function() {
            var elementListeners, event, j, len, listener, ref, results;
            ref = this.listeners;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                elementListeners = ref[j];
                results.push((function() {
                    var ref1, results1;
                    ref1 = elementListeners.events;
                    results1 = [];
                    for (event in ref1) {
                        listener = ref1[event];
                        results1.push(elementListeners.element.addEventListener(event, listener, false));
                    }
                    return results1;
                })());
            }
            return results;
        };

        Dropzone.prototype.removeEventListeners = function() {
            var elementListeners, event, j, len, listener, ref, results;
            ref = this.listeners;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                elementListeners = ref[j];
                results.push((function() {
                    var ref1, results1;
                    ref1 = elementListeners.events;
                    results1 = [];
                    for (event in ref1) {
                        listener = ref1[event];
                        results1.push(elementListeners.element.removeEventListener(event, listener, false));
                    }
                    return results1;
                })());
            }
            return results;
        };

        Dropzone.prototype.disable = function() {
            var file, j, len, ref, results;
            this.clickableElements.forEach(function(element) {
                return element.classList.remove("dz-clickable");
            });
            this.removeEventListeners();
            ref = this.files;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                file = ref[j];
                results.push(this.cancelUpload(file));
            }
            return results;
        };

        Dropzone.prototype.enable = function() {
            this.clickableElements.forEach(function(element) {
                return element.classList.add("dz-clickable");
            });
            return this.setupEventListeners();
        };

        Dropzone.prototype.filesize = function(size) {
            var cutoff, i, j, len, selectedSize, selectedUnit, unit, units;
            selectedSize = 0;
            selectedUnit = "b";
            if (size > 0) {
                units = ['tb', 'gb', 'mb', 'kb', 'b'];
                for (i = j = 0, len = units.length; j < len; i = ++j) {
                    unit = units[i];
                    cutoff = Math.pow(this.options.filesizeBase, 4 - i) / 10;
                    if (size >= cutoff) {
                        selectedSize = size / Math.pow(this.options.filesizeBase, 4 - i);
                        selectedUnit = unit;
                        break;
                    }
                }
                selectedSize = Math.round(10 * selectedSize) / 10;
            }
            return "<strong>" + selectedSize + "</strong> " + this.options.dictFileSizeUnits[selectedUnit];
        };

        Dropzone.prototype._updateMaxFilesReachedClass = function() {
            if ((this.options.maxFiles != null) && this.getAcceptedFiles().length >= this.options.maxFiles) {
                if (this.getAcceptedFiles().length === this.options.maxFiles) {
                    this.emit('maxfilesreached', this.files);
                }
                return this.element.classList.add("dz-max-files-reached");
            } else {
                return this.element.classList.remove("dz-max-files-reached");
            }
        };

        Dropzone.prototype.drop = function(e) {
            var files, items;
            if (!e.dataTransfer) {
                return;
            }
            this.emit("drop", e);
            files = e.dataTransfer.files;
            this.emit("addedfiles", files);
            if (files.length) {
                items = e.dataTransfer.items;
                if (items && items.length && (items[0].webkitGetAsEntry != null)) {
                    this._addFilesFromItems(items);
                } else {
                    this.handleFiles(files);
                }
            }
        };

        Dropzone.prototype.paste = function(e) {
            var items, ref;
            if ((e != null ? (ref = e.clipboardData) != null ? ref.items : void 0 : void 0) == null) {
                return;
            }
            this.emit("paste", e);
            items = e.clipboardData.items;
            if (items.length) {
                return this._addFilesFromItems(items);
            }
        };

        Dropzone.prototype.handleFiles = function(files) {
            var file, j, len, results;
            results = [];
            for (j = 0, len = files.length; j < len; j++) {
                file = files[j];
                results.push(this.addFile(file));
            }
            return results;
        };

        Dropzone.prototype._addFilesFromItems = function(items) {
            var entry, item, j, len, results;
            results = [];
            for (j = 0, len = items.length; j < len; j++) {
                item = items[j];
                if ((item.webkitGetAsEntry != null) && (entry = item.webkitGetAsEntry())) {
                    if (entry.isFile) {
                        results.push(this.addFile(item.getAsFile()));
                    } else if (entry.isDirectory) {
                        results.push(this._addFilesFromDirectory(entry, entry.name));
                    } else {
                        results.push(void 0);
                    }
                } else if (item.getAsFile != null) {
                    if ((item.kind == null) || item.kind === "file") {
                        results.push(this.addFile(item.getAsFile()));
                    } else {
                        results.push(void 0);
                    }
                } else {
                    results.push(void 0);
                }
            }
            return results;
        };

        Dropzone.prototype._addFilesFromDirectory = function(directory, path) {
            var dirReader, errorHandler, readEntries;
            dirReader = directory.createReader();
            errorHandler = function(error) {
                return typeof console !== "undefined" && console !== null ? typeof console.log === "function" ? console.log(error) : void 0 : void 0;
            };
            readEntries = (function(_this) {
                return function() {
                    return dirReader.readEntries(function(entries) {
                        var entry, j, len;
                        if (entries.length > 0) {
                            for (j = 0, len = entries.length; j < len; j++) {
                                entry = entries[j];
                                if (entry.isFile) {
                                    entry.file(function(file) {
                                        if (_this.options.ignoreHiddenFiles && file.name.substring(0, 1) === '.') {
                                            return;
                                        }
                                        file.fullPath = path + "/" + file.name;
                                        return _this.addFile(file);
                                    });
                                } else if (entry.isDirectory) {
                                    _this._addFilesFromDirectory(entry, path + "/" + entry.name);
                                }
                            }
                            readEntries();
                        }
                        return null;
                    }, errorHandler);
                };
            })(this);
            return readEntries();
        };

        Dropzone.prototype.accept = function(file, done) {
            if (file.size > this.options.maxFilesize * 1024 * 1024) {
                return done(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(file.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize));
            } else if (!Dropzone.isValidFile(file, this.options.acceptedFiles)) {
                return done(this.options.dictInvalidFileType);
            } else if ((this.options.maxFiles != null) && this.getAcceptedFiles().length >= this.options.maxFiles) {
                done(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}", this.options.maxFiles));
                return this.emit("maxfilesexceeded", file);
            } else {
                return this.options.accept.call(this, file, done);
            }
        };

        Dropzone.prototype.addFile = function(file) {
            file.upload = {
                progress: 0,
                total: file.size,
                bytesSent: 0,
                filename: this._renameFile(file)
            };
            this.files.push(file);
            file.status = Dropzone.ADDED;
            this.emit("addedfile", file);
            this._enqueueThumbnail(file);
            return this.accept(file, (function(_this) {
                return function(error) {
                    if (error) {
                        file.accepted = false;
                        _this._errorProcessing([file], error);
                    } else {
                        file.accepted = true;
                        if (_this.options.autoQueue) {
                            _this.enqueueFile(file);
                        }
                    }
                    return _this._updateMaxFilesReachedClass();
                };
            })(this));
        };

        Dropzone.prototype.enqueueFiles = function(files) {
            var file, j, len;
            for (j = 0, len = files.length; j < len; j++) {
                file = files[j];
                this.enqueueFile(file);
            }
            return null;
        };

        Dropzone.prototype.enqueueFile = function(file) {
            if (file.status === Dropzone.ADDED && file.accepted === true) {
                file.status = Dropzone.QUEUED;
                if (this.options.autoProcessQueue) {
                    return setTimeout(((function(_this) {
                        return function() {
                            return _this.processQueue();
                        };
                    })(this)), 0);
                }
            } else {
                throw new Error("This file can't be queued because it has already been processed or was rejected.");
            }
        };

        Dropzone.prototype._thumbnailQueue = [];

        Dropzone.prototype._processingThumbnail = false;

        Dropzone.prototype._enqueueThumbnail = function(file) {
            if (this.options.createImageThumbnails && file.type.match(/image.*/) && file.size <= this.options.maxThumbnailFilesize * 1024 * 1024) {
                this._thumbnailQueue.push(file);
                return setTimeout(((function(_this) {
                    return function() {
                        return _this._processThumbnailQueue();
                    };
                })(this)), 0);
            }
        };

        Dropzone.prototype._processThumbnailQueue = function() {
            var file;
            if (this._processingThumbnail || this._thumbnailQueue.length === 0) {
                return;
            }
            this._processingThumbnail = true;
            file = this._thumbnailQueue.shift();
            return this.createThumbnail(file, this.options.thumbnailWidth, this.options.thumbnailHeight, this.options.thumbnailMethod, true, (function(_this) {
                return function(dataUrl) {
                    _this.emit("thumbnail", file, dataUrl);
                    _this._processingThumbnail = false;
                    return _this._processThumbnailQueue();
                };
            })(this));
        };

        Dropzone.prototype.removeFile = function(file) {
            if (file.status === Dropzone.UPLOADING) {
                this.cancelUpload(file);
            }
            this.files = without(this.files, file);
            this.emit("removedfile", file);
            if (this.files.length === 0) {
                return this.emit("reset");
            }
        };

        Dropzone.prototype.removeAllFiles = function(cancelIfNecessary) {
            var file, j, len, ref;
            if (cancelIfNecessary == null) {
                cancelIfNecessary = false;
            }
            ref = this.files.slice();
            for (j = 0, len = ref.length; j < len; j++) {
                file = ref[j];
                if (file.status !== Dropzone.UPLOADING || cancelIfNecessary) {
                    this.removeFile(file);
                }
            }
            return null;
        };

        Dropzone.prototype.resizeImage = function(file, width, height, resizeMethod, callback) {
            return this.createThumbnail(file, width, height, resizeMethod, false, (function(_this) {
                return function(dataUrl, canvas) {
                    var resizeMimeType, resizedDataURL;
                    if (canvas === null) {
                        return callback(file);
                    } else {
                        resizeMimeType = _this.options.resizeMimeType;
                        if (resizeMimeType == null) {
                            resizeMimeType = file.type;
                        }
                        resizedDataURL = canvas.toDataURL(resizeMimeType, _this.options.resizeQuality);
                        if (resizeMimeType === 'image/jpeg' || resizeMimeType === 'image/jpg') {
                            resizedDataURL = ExifRestore.restore(file.dataURL, resizedDataURL);
                        }
                        return callback(Dropzone.dataURItoBlob(resizedDataURL));
                    }
                };
            })(this));
        };

        Dropzone.prototype.createThumbnail = function(file, width, height, resizeMethod, fixOrientation, callback) {
            var fileReader;
            fileReader = new FileReader;
            fileReader.onload = (function(_this) {
                return function() {
                    file.dataURL = fileReader.result;
                    if (file.type === "image/svg+xml") {
                        if (callback != null) {
                            callback(fileReader.result);
                        }
                        return;
                    }
                    return _this.createThumbnailFromUrl(file, width, height, resizeMethod, fixOrientation, callback);
                };
            })(this);
            return fileReader.readAsDataURL(file);
        };

        Dropzone.prototype.createThumbnailFromUrl = function(file, width, height, resizeMethod, fixOrientation, callback, crossOrigin) {
            var img;
            img = document.createElement("img");
            if (crossOrigin) {
                img.crossOrigin = crossOrigin;
            }
            img.onload = (function(_this) {
                return function() {
                    var loadExif;
                    loadExif = function(callback) {
                        return callback(1);
                    };
                    if ((typeof EXIF !== "undefined" && EXIF !== null) && fixOrientation) {
                        loadExif = function(callback) {
                            return EXIF.getData(img, function() {
                                return callback(EXIF.getTag(this, 'Orientation'));
                            });
                        };
                    }
                    return loadExif(function(orientation) {
                        var canvas, ctx, ref, ref1, ref2, ref3, resizeInfo, thumbnail;
                        file.width = img.width;
                        file.height = img.height;
                        resizeInfo = _this.options.resize.call(_this, file, width, height, resizeMethod);
                        canvas = document.createElement("canvas");
                        ctx = canvas.getContext("2d");
                        canvas.width = resizeInfo.trgWidth;
                        canvas.height = resizeInfo.trgHeight;
                        if (orientation > 4) {
                            canvas.width = resizeInfo.trgHeight;
                            canvas.height = resizeInfo.trgWidth;
                        }
                        switch (orientation) {
                            case 2:
                                ctx.translate(canvas.width, 0);
                                ctx.scale(-1, 1);
                                break;
                            case 3:
                                ctx.translate(canvas.width, canvas.height);
                                ctx.rotate(Math.PI);
                                break;
                            case 4:
                                ctx.translate(0, canvas.height);
                                ctx.scale(1, -1);
                                break;
                            case 5:
                                ctx.rotate(0.5 * Math.PI);
                                ctx.scale(1, -1);
                                break;
                            case 6:
                                ctx.rotate(0.5 * Math.PI);
                                ctx.translate(0, -canvas.height);
                                break;
                            case 7:
                                ctx.rotate(0.5 * Math.PI);
                                ctx.translate(canvas.width, -canvas.height);
                                ctx.scale(-1, 1);
                                break;
                            case 8:
                                ctx.rotate(-0.5 * Math.PI);
                                ctx.translate(-canvas.width, 0);
                        }
                        drawImageIOSFix(ctx, img, (ref = resizeInfo.srcX) != null ? ref : 0, (ref1 = resizeInfo.srcY) != null ? ref1 : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, (ref2 = resizeInfo.trgX) != null ? ref2 : 0, (ref3 = resizeInfo.trgY) != null ? ref3 : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);
                        thumbnail = canvas.toDataURL("image/png");
                        if (callback != null) {
                            return callback(thumbnail, canvas);
                        }
                    });
                };
            })(this);
            if (callback != null) {
                img.onerror = callback;
            }
            return img.src = file.dataURL;
        };

        Dropzone.prototype.processQueue = function() {
            var i, parallelUploads, processingLength, queuedFiles;
            parallelUploads = this.options.parallelUploads;
            processingLength = this.getUploadingFiles().length;
            i = processingLength;
            if (processingLength >= parallelUploads) {
                return;
            }
            queuedFiles = this.getQueuedFiles();
            if (!(queuedFiles.length > 0)) {
                return;
            }
            if (this.options.uploadMultiple) {
                return this.processFiles(queuedFiles.slice(0, parallelUploads - processingLength));
            } else {
                while (i < parallelUploads) {
                    if (!queuedFiles.length) {
                        return;
                    }
                    this.processFile(queuedFiles.shift());
                    i++;
                }
            }
        };

        Dropzone.prototype.processFile = function(file) {
            return this.processFiles([file]);
        };

        Dropzone.prototype.processFiles = function(files) {
            var file, j, len;
            for (j = 0, len = files.length; j < len; j++) {
                file = files[j];
                file.processing = true;
                file.status = Dropzone.UPLOADING;
                this.emit("processing", file);
            }
            if (this.options.uploadMultiple) {
                this.emit("processingmultiple", files);
            }
            return this.uploadFiles(files);
        };

        Dropzone.prototype._getFilesWithXhr = function(xhr) {
            var file, files;
            return files = (function() {
                var j, len, ref, results;
                ref = this.files;
                results = [];
                for (j = 0, len = ref.length; j < len; j++) {
                    file = ref[j];
                    if (file.xhr === xhr) {
                        results.push(file);
                    }
                }
                return results;
            }).call(this);
        };

        Dropzone.prototype.cancelUpload = function(file) {
            var groupedFile, groupedFiles, j, k, len, len1, ref;
            if (file.status === Dropzone.UPLOADING) {
                groupedFiles = this._getFilesWithXhr(file.xhr);
                for (j = 0, len = groupedFiles.length; j < len; j++) {
                    groupedFile = groupedFiles[j];
                    groupedFile.status = Dropzone.CANCELED;
                }
                file.xhr.abort();
                for (k = 0, len1 = groupedFiles.length; k < len1; k++) {
                    groupedFile = groupedFiles[k];
                    this.emit("canceled", groupedFile);
                }
                if (this.options.uploadMultiple) {
                    this.emit("canceledmultiple", groupedFiles);
                }
            } else if ((ref = file.status) === Dropzone.ADDED || ref === Dropzone.QUEUED) {
                file.status = Dropzone.CANCELED;
                this.emit("canceled", file);
                if (this.options.uploadMultiple) {
                    this.emit("canceledmultiple", [file]);
                }
            }
            if (this.options.autoProcessQueue) {
                return this.processQueue();
            }
        };

        resolveOption = function() {
            var args, option;
            option = arguments[0], args = 2 <= arguments.length ? slice.call(arguments, 1) : [];
            if (typeof option === 'function') {
                return option.apply(this, args);
            }
            return option;
        };

        Dropzone.prototype.uploadFile = function(file) {
            return this.uploadFiles([file]);
        };

        Dropzone.prototype.uploadFiles = function(files) {
            var doneCounter, doneFunction, file, formData, handleError, headerName, headerValue, headers, i, input, inputName, inputType, j, k, key, l, len, len1, len2, len3, m, method, o, option, progressObj, ref, ref1, ref2, ref3, ref4, ref5, response, results, updateProgress, url, value, xhr;
            xhr = new XMLHttpRequest();
            for (j = 0, len = files.length; j < len; j++) {
                file = files[j];
                file.xhr = xhr;
            }
            method = resolveOption(this.options.method, files);
            url = resolveOption(this.options.url, files);
            xhr.open(method, url, true);
            xhr.timeout = resolveOption(this.options.timeout, files);
            xhr.withCredentials = !!this.options.withCredentials;
            response = null;
            handleError = (function(_this) {
                return function() {
                    var k, len1, results;
                    results = [];
                    for (k = 0, len1 = files.length; k < len1; k++) {
                        file = files[k];
                        results.push(_this._errorProcessing(files, response || _this.options.dictResponseError.replace("{{statusCode}}", xhr.status), xhr));
                    }
                    return results;
                };
            })(this);
            updateProgress = (function(_this) {
                return function(e) {
                    var allFilesFinished, k, l, len1, len2, len3, m, progress, results;
                    if (e != null) {
                        progress = 100 * e.loaded / e.total;
                        for (k = 0, len1 = files.length; k < len1; k++) {
                            file = files[k];
                            file.upload.progress = progress;
                            file.upload.total = e.total;
                            file.upload.bytesSent = e.loaded;
                        }
                    } else {
                        allFilesFinished = true;
                        progress = 100;
                        for (l = 0, len2 = files.length; l < len2; l++) {
                            file = files[l];
                            if (!(file.upload.progress === 100 && file.upload.bytesSent === file.upload.total)) {
                                allFilesFinished = false;
                            }
                            file.upload.progress = progress;
                            file.upload.bytesSent = file.upload.total;
                        }
                        if (allFilesFinished) {
                            return;
                        }
                    }
                    results = [];
                    for (m = 0, len3 = files.length; m < len3; m++) {
                        file = files[m];
                        results.push(_this.emit("uploadprogress", file, progress, file.upload.bytesSent));
                    }
                    return results;
                };
            })(this);
            xhr.onload = (function(_this) {
                return function(e) {
                    var error1, ref;
                    if (files[0].status === Dropzone.CANCELED) {
                        return;
                    }
                    if (xhr.readyState !== 4) {
                        return;
                    }
                    if (xhr.responseType !== 'arraybuffer' && xhr.responseType !== 'blob') {
                        response = xhr.responseText;
                        if (xhr.getResponseHeader("content-type") && ~xhr.getResponseHeader("content-type").indexOf("application/json")) {
                            try {
                                response = JSON.parse(response);
                            } catch (error1) {
                                e = error1;
                                response = "Invalid JSON response from server.";
                            }
                        }
                    }
                    updateProgress();
                    if (!((200 <= (ref = xhr.status) && ref < 300))) {
                        return handleError();
                    } else {
                        return _this._finished(files, response, e);
                    }
                };
            })(this);
            xhr.onerror = (function(_this) {
                return function() {
                    if (files[0].status === Dropzone.CANCELED) {
                        return;
                    }
                    return handleError();
                };
            })(this);
            progressObj = (ref = xhr.upload) != null ? ref : xhr;
            progressObj.onprogress = updateProgress;
            headers = {
                "Accept": "application/json",
                "Cache-Control": "no-cache",
                "X-Requested-With": "XMLHttpRequest"
            };
            if (this.options.headers) {
                extend(headers, this.options.headers);
            }
            for (headerName in headers) {
                headerValue = headers[headerName];
                if (headerValue) {
                    xhr.setRequestHeader(headerName, headerValue);
                }
            }
            formData = new FormData();
            if (this.options.params) {
                ref1 = this.options.params;
                for (key in ref1) {
                    value = ref1[key];
                    formData.append(key, value);
                }
            }
            for (k = 0, len1 = files.length; k < len1; k++) {
                file = files[k];
                this.emit("sending", file, xhr, formData);
            }
            if (this.options.uploadMultiple) {
                this.emit("sendingmultiple", files, xhr, formData);
            }
            if (this.element.tagName === "FORM") {
                ref2 = this.element.querySelectorAll("input, textarea, select, button");
                for (l = 0, len2 = ref2.length; l < len2; l++) {
                    input = ref2[l];
                    inputName = input.getAttribute("name");
                    inputType = input.getAttribute("type");
                    if (input.tagName === "SELECT" && input.hasAttribute("multiple")) {
                        ref3 = input.options;
                        for (m = 0, len3 = ref3.length; m < len3; m++) {
                            option = ref3[m];
                            if (option.selected) {
                                formData.append(inputName, option.value);
                            }
                        }
                    } else if (!inputType || ((ref4 = inputType.toLowerCase()) !== "checkbox" && ref4 !== "radio") || input.checked) {
                        formData.append(inputName, input.value);
                    }
                }
            }
            doneCounter = 0;
            results = [];
            for (i = o = 0, ref5 = files.length - 1; 0 <= ref5 ? o <= ref5 : o >= ref5; i = 0 <= ref5 ? ++o : --o) {
                doneFunction = (function(_this) {
                    return function(file, paramName, fileName) {
                        return function(transformedFile) {
                            formData.append(paramName, transformedFile, fileName);
                            if (++doneCounter === files.length) {
                                return _this.submitRequest(xhr, formData, files);
                            }
                        };
                    };
                })(this);
                results.push(this.options.transformFile.call(this, files[i], doneFunction(files[i], this._getParamName(i), files[i].upload.filename)));
            }
            return results;
        };

        Dropzone.prototype.submitRequest = function(xhr, formData, files) {
            return xhr.send(formData);
        };

        Dropzone.prototype._finished = function(files, responseText, e) {
            var file, j, len;
            for (j = 0, len = files.length; j < len; j++) {
                file = files[j];
                file.status = Dropzone.SUCCESS;
                this.emit("success", file, responseText, e);
                this.emit("complete", file);
            }
            if (this.options.uploadMultiple) {
                this.emit("successmultiple", files, responseText, e);
                this.emit("completemultiple", files);
            }
            if (this.options.autoProcessQueue) {
                return this.processQueue();
            }
        };

        Dropzone.prototype._errorProcessing = function(files, message, xhr) {
            var file, j, len;
            for (j = 0, len = files.length; j < len; j++) {
                file = files[j];
                file.status = Dropzone.ERROR;
                this.emit("error", file, message, xhr);
                this.emit("complete", file);
            }
            if (this.options.uploadMultiple) {
                this.emit("errormultiple", files, message, xhr);
                this.emit("completemultiple", files);
            }
            if (this.options.autoProcessQueue) {
                return this.processQueue();
            }
        };

        return Dropzone;

    })(Emitter);

    Dropzone.version = "5.1.1";

    Dropzone.options = {};

    Dropzone.optionsForElement = function(element) {
        if (element.getAttribute("id")) {
            return Dropzone.options[camelize(element.getAttribute("id"))];
        } else {
            return void 0;
        }
    };

    Dropzone.instances = [];

    Dropzone.forElement = function(element) {
        if (typeof element === "string") {
            element = document.querySelector(element);
        }
        if ((element != null ? element.dropzone : void 0) == null) {
            throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");
        }
        return element.dropzone;
    };

    Dropzone.autoDiscover = true;

    Dropzone.discover = function() {
        var checkElements, dropzone, dropzones, j, len, results;
        if (document.querySelectorAll) {
            dropzones = document.querySelectorAll(".dropzone");
        } else {
            dropzones = [];
            checkElements = function(elements) {
                var el, j, len, results;
                results = [];
                for (j = 0, len = elements.length; j < len; j++) {
                    el = elements[j];
                    if (/(^| )dropzone($| )/.test(el.className)) {
                        results.push(dropzones.push(el));
                    } else {
                        results.push(void 0);
                    }
                }
                return results;
            };
            checkElements(document.getElementsByTagName("div"));
            checkElements(document.getElementsByTagName("form"));
        }
        results = [];
        for (j = 0, len = dropzones.length; j < len; j++) {
            dropzone = dropzones[j];
            if (Dropzone.optionsForElement(dropzone) !== false) {
                results.push(new Dropzone(dropzone));
            } else {
                results.push(void 0);
            }
        }
        return results;
    };

    Dropzone.blacklistedBrowsers = [/opera.*Macintosh.*version\/12/i];

    Dropzone.isBrowserSupported = function() {
        var capableBrowser, j, len, ref, regex;
        capableBrowser = true;
        if (window.File && window.FileReader && window.FileList && window.Blob && window.FormData && document.querySelector) {
            if (!("classList" in document.createElement("a"))) {
                capableBrowser = false;
            } else {
                ref = Dropzone.blacklistedBrowsers;
                for (j = 0, len = ref.length; j < len; j++) {
                    regex = ref[j];
                    if (regex.test(navigator.userAgent)) {
                        capableBrowser = false;
                        continue;
                    }
                }
            }
        } else {
            capableBrowser = false;
        }
        return capableBrowser;
    };

    Dropzone.dataURItoBlob = function(dataURI) {
        var ab, byteString, i, ia, j, mimeString, ref;
        byteString = atob(dataURI.split(',')[1]);
        mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        ab = new ArrayBuffer(byteString.length);
        ia = new Uint8Array(ab);
        for (i = j = 0, ref = byteString.length; 0 <= ref ? j <= ref : j >= ref; i = 0 <= ref ? ++j : --j) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab], {
            type: mimeString
        });
    };

    without = function(list, rejectedItem) {
        var item, j, len, results;
        results = [];
        for (j = 0, len = list.length; j < len; j++) {
            item = list[j];
            if (item !== rejectedItem) {
                results.push(item);
            }
        }
        return results;
    };

    camelize = function(str) {
        return str.replace(/[\-_](\w)/g, function(match) {
            return match.charAt(1).toUpperCase();
        });
    };

    Dropzone.createElement = function(string) {
        var div;
        div = document.createElement("div");
        div.innerHTML = string;
        return div.childNodes[0];
    };

    Dropzone.elementInside = function(element, container) {
        if (element === container) {
            return true;
        }
        while (element = element.parentNode) {
            if (element === container) {
                return true;
            }
        }
        return false;
    };

    Dropzone.getElement = function(el, name) {
        var element;
        if (typeof el === "string") {
            element = document.querySelector(el);
        } else if (el.nodeType != null) {
            element = el;
        }
        if (element == null) {
            throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector or a plain HTML element.");
        }
        return element;
    };

    Dropzone.getElements = function(els, name) {
        var e, el, elements, error1, j, k, len, len1, ref;
        if (els instanceof Array) {
            elements = [];
            try {
                for (j = 0, len = els.length; j < len; j++) {
                    el = els[j];
                    elements.push(this.getElement(el, name));
                }
            } catch (error1) {
                e = error1;
                elements = null;
            }
        } else if (typeof els === "string") {
            elements = [];
            ref = document.querySelectorAll(els);
            for (k = 0, len1 = ref.length; k < len1; k++) {
                el = ref[k];
                elements.push(el);
            }
        } else if (els.nodeType != null) {
            elements = [els];
        }
        if (!((elements != null) && elements.length)) {
            throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");
        }
        return elements;
    };

    Dropzone.confirm = function(question, accepted, rejected) {
        if (window.confirm(question)) {
            return accepted();
        } else if (rejected != null) {
            return rejected();
        }
    };

    Dropzone.isValidFile = function(file, acceptedFiles) {
        var baseMimeType, j, len, mimeType, validType;
        if (!acceptedFiles) {
            return true;
        }
        acceptedFiles = acceptedFiles.split(",");
        mimeType = file.type;
        baseMimeType = mimeType.replace(/\/.*$/, "");
        for (j = 0, len = acceptedFiles.length; j < len; j++) {
            validType = acceptedFiles[j];
            validType = validType.trim();
            if (validType.charAt(0) === ".") {
                if (file.name.toLowerCase().indexOf(validType.toLowerCase(), file.name.length - validType.length) !== -1) {
                    return true;
                }
            } else if (/\/\*$/.test(validType)) {
                if (baseMimeType === validType.replace(/\/.*$/, "")) {
                    return true;
                }
            } else {
                if (mimeType === validType) {
                    return true;
                }
            }
        }
        return false;
    };

    if (typeof jQuery !== "undefined" && jQuery !== null) {
        jQuery.fn.dropzone = function(options) {
            return this.each(function() {
                return new Dropzone(this, options);
            });
        };
    }

    if (typeof module !== "undefined" && module !== null) {
        module.exports = Dropzone;
    } else {
        window.Dropzone = Dropzone;
    }

    Dropzone.ADDED = "added";

    Dropzone.QUEUED = "queued";

    Dropzone.ACCEPTED = Dropzone.QUEUED;

    Dropzone.UPLOADING = "uploading";

    Dropzone.PROCESSING = Dropzone.UPLOADING;

    Dropzone.CANCELED = "canceled";

    Dropzone.ERROR = "error";

    Dropzone.SUCCESS = "success";


    /*

     Bugfix for iOS 6 and 7
     Source: http://stackoverflow.com/questions/11929099/html5-canvas-drawimage-ratio-bug-ios
     based on the work of https://github.com/stomita/ios-imagefile-megapixel
     */

    detectVerticalSquash = function(img) {
        var alpha, canvas, ctx, data, ey, ih, iw, py, ratio, sy;
        iw = img.naturalWidth;
        ih = img.naturalHeight;
        canvas = document.createElement("canvas");
        canvas.width = 1;
        canvas.height = ih;
        ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0, 0);
        data = ctx.getImageData(1, 0, 1, ih).data;
        sy = 0;
        ey = ih;
        py = ih;
        while (py > sy) {
            alpha = data[(py - 1) * 4 + 3];
            if (alpha === 0) {
                ey = py;
            } else {
                sy = py;
            }
            py = (ey + sy) >> 1;
        }
        ratio = py / ih;
        if (ratio === 0) {
            return 1;
        } else {
            return ratio;
        }
    };

    drawImageIOSFix = function(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
        var vertSquashRatio;
        vertSquashRatio = detectVerticalSquash(img);
        return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
    };

    ExifRestore = (function() {
        function ExifRestore() {}

        ExifRestore.KEY_STR = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

        ExifRestore.encode64 = function(input) {
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4, i, output;
            output = '';
            chr1 = void 0;
            chr2 = void 0;
            chr3 = '';
            enc1 = void 0;
            enc2 = void 0;
            enc3 = void 0;
            enc4 = '';
            i = 0;
            while (true) {
                chr1 = input[i++];
                chr2 = input[i++];
                chr3 = input[i++];
                enc1 = chr1 >> 2;
                enc2 = (chr1 & 3) << 4 | chr2 >> 4;
                enc3 = (chr2 & 15) << 2 | chr3 >> 6;
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output + this.KEY_STR.charAt(enc1) + this.KEY_STR.charAt(enc2) + this.KEY_STR.charAt(enc3) + this.KEY_STR.charAt(enc4);
                chr1 = chr2 = chr3 = '';
                enc1 = enc2 = enc3 = enc4 = '';
                if (!(i < input.length)) {
                    break;
                }
            }
            return output;
        };

        ExifRestore.restore = function(origFileBase64, resizedFileBase64) {
            var image, rawImage, segments;
            if (!origFileBase64.match('data:image/jpeg;base64,')) {
                return resizedFileBase64;
            }
            rawImage = this.decode64(origFileBase64.replace('data:image/jpeg;base64,', ''));
            segments = this.slice2Segments(rawImage);
            image = this.exifManipulation(resizedFileBase64, segments);
            return 'data:image/jpeg;base64,' + this.encode64(image);
        };

        ExifRestore.exifManipulation = function(resizedFileBase64, segments) {
            var aBuffer, exifArray, newImageArray;
            exifArray = this.getExifArray(segments);
            newImageArray = this.insertExif(resizedFileBase64, exifArray);
            aBuffer = new Uint8Array(newImageArray);
            return aBuffer;
        };

        ExifRestore.getExifArray = function(segments) {
            var seg, x;
            seg = void 0;
            x = 0;
            while (x < segments.length) {
                seg = segments[x];
                if (seg[0] === 255 & seg[1] === 225) {
                    return seg;
                }
                x++;
            }
            return [];
        };

        ExifRestore.insertExif = function(resizedFileBase64, exifArray) {
            var array, ato, buf, imageData, mae, separatePoint;
            imageData = resizedFileBase64.replace('data:image/jpeg;base64,', '');
            buf = this.decode64(imageData);
            separatePoint = buf.indexOf(255, 3);
            mae = buf.slice(0, separatePoint);
            ato = buf.slice(separatePoint);
            array = mae;
            array = array.concat(exifArray);
            array = array.concat(ato);
            return array;
        };

        ExifRestore.slice2Segments = function(rawImageArray) {
            var endPoint, head, length, seg, segments;
            head = 0;
            segments = [];
            while (true) {
                if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 218) {
                    break;
                }
                if (rawImageArray[head] === 255 & rawImageArray[head + 1] === 216) {
                    head += 2;
                } else {
                    length = rawImageArray[head + 2] * 256 + rawImageArray[head + 3];
                    endPoint = head + length + 2;
                    seg = rawImageArray.slice(head, endPoint);
                    segments.push(seg);
                    head = endPoint;
                }
                if (head > rawImageArray.length) {
                    break;
                }
            }
            return segments;
        };

        ExifRestore.decode64 = function(input) {
            var base64test, buf, chr1, chr2, chr3, enc1, enc2, enc3, enc4, i, output;
            output = '';
            chr1 = void 0;
            chr2 = void 0;
            chr3 = '';
            enc1 = void 0;
            enc2 = void 0;
            enc3 = void 0;
            enc4 = '';
            i = 0;
            buf = [];
            base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                console.warning('There were invalid base64 characters in the input text.\n' + 'Valid base64 characters are A-Z, a-z, 0-9, \'+\', \'/\',and \'=\'\n' + 'Expect errors in decoding.');
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, '');
            while (true) {
                enc1 = this.KEY_STR.indexOf(input.charAt(i++));
                enc2 = this.KEY_STR.indexOf(input.charAt(i++));
                enc3 = this.KEY_STR.indexOf(input.charAt(i++));
                enc4 = this.KEY_STR.indexOf(input.charAt(i++));
                chr1 = enc1 << 2 | enc2 >> 4;
                chr2 = (enc2 & 15) << 4 | enc3 >> 2;
                chr3 = (enc3 & 3) << 6 | enc4;
                buf.push(chr1);
                if (enc3 !== 64) {
                    buf.push(chr2);
                }
                if (enc4 !== 64) {
                    buf.push(chr3);
                }
                chr1 = chr2 = chr3 = '';
                enc1 = enc2 = enc3 = enc4 = '';
                if (!(i < input.length)) {
                    break;
                }
            }
            return buf;
        };

        return ExifRestore;

    })();


    /*
     * contentloaded.js
     *
     * Author: Diego Perini (diego.perini at gmail.com)
     * Summary: cross-browser wrapper for DOMContentLoaded
     * Updated: 20101020
     * License: MIT
     * Version: 1.2
     *
     * URL:
     * http://javascript.nwbox.com/ContentLoaded/
     * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
     */

    contentLoaded = function(win, fn) {
        var add, doc, done, init, poll, pre, rem, root, top;
        done = false;
        top = true;
        doc = win.document;
        root = doc.documentElement;
        add = (doc.addEventListener ? "addEventListener" : "attachEvent");
        rem = (doc.addEventListener ? "removeEventListener" : "detachEvent");
        pre = (doc.addEventListener ? "" : "on");
        init = function(e) {
            if (e.type === "readystatechange" && doc.readyState !== "complete") {
                return;
            }
            (e.type === "load" ? win : doc)[rem](pre + e.type, init, false);
            if (!done && (done = true)) {
                return fn.call(win, e.type || e);
            }
        };
        poll = function() {
            var e, error1;
            try {
                root.doScroll("left");
            } catch (error1) {
                e = error1;
                setTimeout(poll, 50);
                return;
            }
            return init("poll");
        };
        if (doc.readyState !== "complete") {
            if (doc.createEventObject && root.doScroll) {
                try {
                    top = !win.frameElement;
                } catch (undefined) {}
                if (top) {
                    poll();
                }
            }
            doc[add](pre + "DOMContentLoaded", init, false);
            doc[add](pre + "readystatechange", init, false);
            return win[add](pre + "load", init, false);
        }
    };

    Dropzone._autoDiscoverFunction = function() {
        if (Dropzone.autoDiscover) {
            return Dropzone.discover();
        }
    };

    contentLoaded(window, Dropzone._autoDiscoverFunction);

}).call(this);









"use strict";
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/* jQuery Form Styler v2.0.0 | (c) Dimox | https://github.com/Dimox/jQueryFormStyler */
!function (e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? module.exports = e($ || require("jquery")) : e(jQuery);
}(function (e) {
    "use strict";
    function t(t, s) {
        this.element = t, this.options = e.extend({}, l, s);var i = this.options.locale;void 0 !== this.options.locales[i] && e.extend(this.options, this.options.locales[i]), this.init();
    }function s(t) {
        if (!e(t.target).parents().hasClass("jq-selectbox") && "OPTION" != t.target.nodeName && e("div.jq-selectbox.opened").length) {
            var s = e("div.jq-selectbox.opened"),
                l = e("div.jq-selectbox__search input", s),
                o = e("div.jq-selectbox__dropdown", s);s.find("select").data("_" + i).options.onSelectClosed.call(s), l.length && l.val("").keyup(), o.hide().find("li.sel").addClass("selected"), s.removeClass("focused opened dropup dropdown");
        }
    }var i = "styler",
        l = { idSuffix: "-styler", filePlaceholder: "Файл не выбран", fileBrowse: "Обзор...", fileNumber: "Выбрано файлов: %s", selectPlaceholder: "Выберите...", selectSearch: !1, selectSearchLimit: 10, selectSearchNotFound: "Совпадений не найдено", selectSearchPlaceholder: "Поиск...", selectVisibleOptions: 0, selectSmartPositioning: !0, locale: "ru", locales: { en: { filePlaceholder: "No file selected", fileBrowse: "Browse...", fileNumber: "Selected files: %s", selectPlaceholder: "Select...", selectSearchNotFound: "No matches found", selectSearchPlaceholder: "Search..." } }, onSelectOpened: function onSelectOpened() {}, onSelectClosed: function onSelectClosed() {}, onFormStyled: function onFormStyled() {} };t.prototype = { init: function init() {
        function t() {
            void 0 !== i.attr("id") && "" !== i.attr("id") && (this.id = i.attr("id") + l.idSuffix), this.title = i.attr("title"), this.classes = i.attr("class"), this.data = i.data();
        }var i = e(this.element),
            l = this.options,
            o = !(!navigator.userAgent.match(/(iPad|iPhone|iPod)/i) || navigator.userAgent.match(/(Windows\sPhone)/i)),
            a = !(!navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/(Windows\sPhone)/i));if (i.is(":checkbox")) {
            var d = function d() {
                var s = new t(),
                    l = e('<div class="jq-checkbox"><div class="jq-checkbox__div"></div></div>').attr({ id: s.id, title: s.title }).addClass(s.classes).data(s.data);i.after(l).prependTo(l), i.is(":checked") && l.addClass("checked"), i.is(":disabled") && l.addClass("disabled"), l.click(function (e) {
                    e.preventDefault(), i.triggerHandler("click"), l.is(".disabled") || (i.is(":checked") ? (i.prop("checked", !1), l.removeClass("checked")) : (i.prop("checked", !0), l.addClass("checked")), i.focus().change());
                }), i.closest("label").add('label[for="' + i.attr("id") + '"]').on("click.styler", function (t) {
                    e(t.target).is("a") || e(t.target).closest(l).length || (l.triggerHandler("click"), t.preventDefault());
                }), i.on("change.styler", function () {
                    i.is(":checked") ? l.addClass("checked") : l.removeClass("checked");
                }).on("keydown.styler", function (e) {
                    32 == e.which && l.click();
                }).on("focus.styler", function () {
                    l.is(".disabled") || l.addClass("focused");
                }).on("blur.styler", function () {
                    l.removeClass("focused");
                });
            };d(), i.on("refresh", function () {
                i.closest("label").add('label[for="' + i.attr("id") + '"]').off(".styler"), i.off(".styler").parent().before(i).remove(), d();
            });
        } else if (i.is(":radio")) {
            var r = function r() {
                var s = new t(),
                    l = e('<div class="jq-radio"><div class="jq-radio__div"></div></div>').attr({ id: s.id, title: s.title }).addClass(s.classes).data(s.data);i.after(l).prependTo(l), i.is(":checked") && l.addClass("checked"), i.is(":disabled") && l.addClass("disabled"), e.fn.commonParents = function () {
                    var t = this;return t.first().parents().filter(function () {
                        return e(this).find(t).length === t.length;
                    });
                }, e.fn.commonParent = function () {
                    return e(this).commonParents().first();
                }, l.click(function (t) {
                    if (t.preventDefault(), i.triggerHandler("click"), !l.is(".disabled")) {
                        var s = e('input[name="' + i.attr("name") + '"]');s.commonParent().find(s).prop("checked", !1).parent().removeClass("checked"), i.prop("checked", !0).parent().addClass("checked"), i.focus().change();
                    }
                }), i.closest("label").add('label[for="' + i.attr("id") + '"]').on("click.styler", function (t) {
                    e(t.target).is("a") || e(t.target).closest(l).length || (l.triggerHandler("click"), t.preventDefault());
                }), i.on("change.styler", function () {
                    i.parent().addClass("checked");
                }).on("focus.styler", function () {
                    l.is(".disabled") || l.addClass("focused");
                }).on("blur.styler", function () {
                    l.removeClass("focused");
                });
            };r(), i.on("refresh", function () {
                i.closest("label").add('label[for="' + i.attr("id") + '"]').off(".styler"), i.off(".styler").parent().before(i).remove(), r();
            });
        } else if (i.is(":file")) {
            var c = function c() {
                var s = new t(),
                    o = i.data("placeholder");void 0 === o && (o = l.filePlaceholder);var a = i.data("browse");void 0 !== a && "" !== a || (a = l.fileBrowse);var d = e('<div class="jq-file"><div class="jq-file__name">' + o + '</div><div class="jq-file__browse">' + a + "</div></div>").attr({ id: s.id, title: s.title }).addClass(s.classes).data(s.data);i.after(d).appendTo(d), i.is(":disabled") && d.addClass("disabled");var r = i.val(),
                    c = e("div.jq-file__name", d);r && c.text(r.replace(/.+[\\\/]/, "")), i.on("change.styler", function () {
                    var e = i.val();if (i.is("[multiple]")) {
                        e = "";var t = i[0].files.length;if (t > 0) {
                            var s = i.data("number");void 0 === s && (s = l.fileNumber), s = s.replace("%s", t), e = s;
                        }
                    }c.text(e.replace(/.+[\\\/]/, "")), "" === e ? (c.text(o), d.removeClass("changed")) : d.addClass("changed");
                }).on("focus.styler", function () {
                    d.addClass("focused");
                }).on("blur.styler", function () {
                    d.removeClass("focused");
                }).on("click.styler", function () {
                    d.removeClass("focused");
                });
            };c(), i.on("refresh", function () {
                i.off(".styler").parent().before(i).remove(), c();
            });
        } else if (i.is('input[type="number"]')) {
            var n = function n() {
                var s = new t(),
                    l = e('<div class="jq-number"><div class="jq-number__spin minus"></div><div class="jq-number__spin plus"></div></div>').attr({ id: s.id, title: s.title }).addClass(s.classes).data(s.data);i.after(l).prependTo(l).wrap('<div class="jq-number__field"></div>'), i.is(":disabled") && l.addClass("disabled");var o,
                    a,
                    d,
                    r = null,
                    c = null;void 0 !== i.attr("min") && (o = i.attr("min")), void 0 !== i.attr("max") && (a = i.attr("max")), d = void 0 !== i.attr("step") && e.isNumeric(i.attr("step")) ? Number(i.attr("step")) : Number(1);var n = function n(t) {
                    var s,
                        l = i.val();e.isNumeric(l) || (l = 0, i.val("0")), t.is(".minus") ? s = Number(l) - d : t.is(".plus") && (s = Number(l) + d);var r = (d.toString().split(".")[1] || []).length;if (r > 0) {
                        for (var c = "1"; c.length <= r;) {
                            c += "0";
                        }s = Math.round(s * c) / c;
                    }e.isNumeric(o) && e.isNumeric(a) ? s >= o && s <= a && i.val(s) : e.isNumeric(o) && !e.isNumeric(a) ? s >= o && i.val(s) : !e.isNumeric(o) && e.isNumeric(a) ? s <= a && i.val(s) : i.val(s);
                };l.is(".disabled") || (l.on("mousedown", "div.jq-number__spin", function () {
                    var t = e(this);n(t), r = setTimeout(function () {
                        c = setInterval(function () {
                            n(t);
                        }, 40);
                    }, 350);
                }).on("mouseup mouseout", "div.jq-number__spin", function () {
                    clearTimeout(r), clearInterval(c);
                }).on("mouseup", "div.jq-number__spin", function () {
                    i.change();
                }), i.on("focus.styler", function () {
                    l.addClass("focused");
                }).on("blur.styler", function () {
                    l.removeClass("focused");
                }));
            };n(), i.on("refresh", function () {
                i.off(".styler").closest(".jq-number").before(i).remove(), n();
            });
        } else if (i.is("select")) {
            var f = function f() {
                function d(e) {
                    var t = e.prop("scrollHeight") - e.outerHeight(),
                        s = null,
                        i = null;e.off("mousewheel DOMMouseScroll").on("mousewheel DOMMouseScroll", function (l) {
                        s = l.originalEvent.detail < 0 || l.originalEvent.wheelDelta > 0 ? 1 : -1, ((i = e.scrollTop()) >= t && s < 0 || i <= 0 && s > 0) && (l.stopPropagation(), l.preventDefault());
                    });
                }function r() {
                    for (var e = 0; e < c.length; e++) {
                        var t = c.eq(e),
                            s = "",
                            i = "",
                            o = "",
                            a = "",
                            d = "",
                            r = "",
                            f = "",
                            h = "",
                            u = "";t.prop("selected") && (i = "selected sel"), t.is(":disabled") && (i = "disabled"), t.is(":selected:disabled") && (i = "selected sel disabled"), void 0 !== t.attr("id") && "" !== t.attr("id") && (a = ' id="' + t.attr("id") + l.idSuffix + '"'), void 0 !== t.attr("title") && "" !== c.attr("title") && (d = ' title="' + t.attr("title") + '"'), void 0 !== t.attr("class") && (f = " " + t.attr("class"), u = ' data-jqfs-class="' + t.attr("class") + '"');var p = t.data();for (var v in p) {
                            "" !== p[v] && (r += " data-" + v + '="' + p[v] + '"');
                        }i + f !== "" && (o = ' class="' + i + f + '"'), s = "<li" + u + r + o + d + a + ">" + t.html() + "</li>", t.parent().is("optgroup") && (void 0 !== t.parent().attr("class") && (h = " " + t.parent().attr("class")), s = "<li" + u + r + ' class="' + i + f + " option" + h + '"' + d + a + ">" + t.html() + "</li>", t.is(":first-child") && (s = '<li class="optgroup' + h + '">' + t.parent().attr("label") + "</li>" + s)), n += s;
                    }
                }var c = e("option", i),
                    n = "";if (i.is("[multiple]")) {
                    if (a || o) return;!function () {
                        var s = new t(),
                            l = e('<div class="jq-select-multiple jqselect"></div>').attr({ id: s.id, title: s.title }).addClass(s.classes).data(s.data);i.after(l), r(), l.append("<ul>" + n + "</ul>");var o = e("ul", l),
                            a = e("li", l),
                            f = i.attr("size"),
                            h = o.outerHeight(),
                            u = a.outerHeight();void 0 !== f && f > 0 ? o.css({ height: u * f }) : o.css({ height: 4 * u }), h > l.height() && (o.css("overflowY", "scroll"), d(o), a.filter(".selected").length && o.scrollTop(o.scrollTop() + a.filter(".selected").position().top)), i.prependTo(l), i.is(":disabled") ? (l.addClass("disabled"), c.each(function () {
                            e(this).is(":selected") && a.eq(e(this).index()).addClass("selected");
                        })) : (a.filter(":not(.disabled):not(.optgroup)").click(function (t) {
                            i.focus();var s = e(this);if (t.ctrlKey || t.metaKey || s.addClass("selected"), t.shiftKey || s.addClass("first"), t.ctrlKey || t.metaKey || t.shiftKey || s.siblings().removeClass("selected first"), (t.ctrlKey || t.metaKey) && (s.is(".selected") ? s.removeClass("selected first") : s.addClass("selected first"), s.siblings().removeClass("first")), t.shiftKey) {
                                var l = !1,
                                    o = !1;s.siblings().removeClass("selected").siblings(".first").addClass("selected"), s.prevAll().each(function () {
                                    e(this).is(".first") && (l = !0);
                                }), s.nextAll().each(function () {
                                    e(this).is(".first") && (o = !0);
                                }), l && s.prevAll().each(function () {
                                    if (e(this).is(".selected")) return !1;e(this).not(".disabled, .optgroup").addClass("selected");
                                }), o && s.nextAll().each(function () {
                                    if (e(this).is(".selected")) return !1;e(this).not(".disabled, .optgroup").addClass("selected");
                                }), 1 == a.filter(".selected").length && s.addClass("first");
                            }c.prop("selected", !1), a.filter(".selected").each(function () {
                                var t = e(this),
                                    s = t.index();t.is(".option") && (s -= t.prevAll(".optgroup").length), c.eq(s).prop("selected", !0);
                            }), i.change();
                        }), c.each(function (t) {
                            e(this).data("optionIndex", t);
                        }), i.on("change.styler", function () {
                            a.removeClass("selected");var t = [];c.filter(":selected").each(function () {
                                t.push(e(this).data("optionIndex"));
                            }), a.not(".optgroup").filter(function (s) {
                                return e.inArray(s, t) > -1;
                            }).addClass("selected");
                        }).on("focus.styler", function () {
                            l.addClass("focused");
                        }).on("blur.styler", function () {
                            l.removeClass("focused");
                        }), h > l.height() && i.on("keydown.styler", function (e) {
                            38 != e.which && 37 != e.which && 33 != e.which || o.scrollTop(o.scrollTop() + a.filter(".selected").position().top - u), 40 != e.which && 39 != e.which && 34 != e.which || o.scrollTop(o.scrollTop() + a.filter(".selected:last").position().top - o.innerHeight() + 2 * u);
                        }));
                    }();
                } else !function () {
                    var a = new t(),
                        f = "",
                        h = i.data("placeholder"),
                        u = i.data("search"),
                        p = i.data("search-limit"),
                        v = i.data("search-not-found"),
                        m = i.data("search-placeholder"),
                        g = i.data("smart-positioning");void 0 === h && (h = l.selectPlaceholder), void 0 !== u && "" !== u || (u = l.selectSearch), void 0 !== p && "" !== p || (p = l.selectSearchLimit), void 0 !== v && "" !== v || (v = l.selectSearchNotFound), void 0 === m && (m = l.selectSearchPlaceholder), void 0 !== g && "" !== g || (g = l.selectSmartPositioning);var b = e('<div class="jq-selectbox jqselect"><div class="jq-selectbox__select"><div class="jq-selectbox__select-text"></div><div class="jq-selectbox__trigger"><div class="jq-selectbox__trigger-arrow"></div></div></div></div>').attr({ id: a.id, title: a.title }).addClass(a.classes).data(a.data);i.after(b).prependTo(b);var C = b.css("z-index");C = C > 0 ? C : 1;var x = e("div.jq-selectbox__select", b),
                        y = e("div.jq-selectbox__select-text", b),
                        w = c.filter(":selected");r(), u && (f = '<div class="jq-selectbox__search"><input type="search" autocomplete="off" placeholder="' + m + '"></div><div class="jq-selectbox__not-found">' + v + "</div>");var q = e('<div class="jq-selectbox__dropdown">' + f + "<ul>" + n + "</ul></div>");b.append(q);var _ = e("ul", q),
                        j = e("li", q),
                        k = e("input", q),
                        S = e("div.jq-selectbox__not-found", q).hide();j.length < p && k.parent().hide(), "" === c.first().text() && c.first().is(":selected") && !1 !== h ? y.text(h).addClass("placeholder") : y.text(w.text());var T = 0,
                        N = 0;if (j.css({ display: "inline-block" }), j.each(function () {
                            var t = e(this);t.innerWidth() > T && (T = t.innerWidth(), N = t.width());
                        }), j.css({ display: "" }), y.is(".placeholder") && y.width() > T) y.width(y.width());else {
                        var P = b.clone().appendTo("body").width("auto"),
                            H = P.outerWidth();P.remove(), H == b.outerWidth() && y.width(N);
                    }T > b.width() && q.width(T), "" === c.first().text() && "" !== i.data("placeholder") && j.first().hide();var A = b.outerHeight(!0),
                        D = k.parent().outerHeight(!0) || 0,
                        I = _.css("max-height"),
                        K = j.filter(".selected");if (K.length < 1 && j.first().addClass("selected sel"), void 0 === j.data("li-height")) {
                        var O = j.outerHeight();!1 !== h && (O = j.eq(1).outerHeight()), j.data("li-height", O);
                    }var M = q.css("top");if ("auto" == q.css("left") && q.css({ left: 0 }), "auto" == q.css("top") && (q.css({ top: A }), M = A), q.hide(), K.length && (c.first().text() != w.text() && b.addClass("changed"), b.data("jqfs-class", K.data("jqfs-class")), b.addClass(K.data("jqfs-class"))), i.is(":disabled")) return b.addClass("disabled"), !1;x.click(function () {
                        if (e("div.jq-selectbox").filter(".opened").length && l.onSelectClosed.call(e("div.jq-selectbox").filter(".opened")), i.focus(), !o) {
                            var t = e(window),
                                s = j.data("li-height"),
                                a = b.offset().top,
                                r = t.height() - A - (a - t.scrollTop()),
                                n = i.data("visible-options");void 0 !== n && "" !== n || (n = l.selectVisibleOptions);var f = 5 * s,
                                h = s * n;n > 0 && n < 6 && (f = h), 0 === n && (h = "auto");var u = function u() {
                                q.height("auto").css({ bottom: "auto", top: M });var e = function e() {
                                    _.css("max-height", Math.floor((r - 20 - D) / s) * s);
                                };e(), _.css("max-height", h), "none" != I && _.css("max-height", I), r < q.outerHeight() + 20 && e();
                            };!0 === g || 1 === g ? r > f + D + 20 ? (u(), b.removeClass("dropup").addClass("dropdown")) : (function () {
                                q.height("auto").css({ top: "auto", bottom: M });var e = function e() {
                                    _.css("max-height", Math.floor((a - t.scrollTop() - 20 - D) / s) * s);
                                };e(), _.css("max-height", h), "none" != I && _.css("max-height", I), a - t.scrollTop() - 20 < q.outerHeight() + 20 && e();
                            }(), b.removeClass("dropdown").addClass("dropup")) : !1 === g || 0 === g ? r > f + D + 20 && (u(), b.removeClass("dropup").addClass("dropdown")) : (q.height("auto").css({ bottom: "auto", top: M }), _.css("max-height", h), "none" != I && _.css("max-height", I)), b.offset().left + q.outerWidth() > t.width() && q.css({ left: "auto", right: 0 }), e("div.jqselect").css({ zIndex: C - 1 }).removeClass("opened"), b.css({ zIndex: C }), q.is(":hidden") ? (e("div.jq-selectbox__dropdown:visible").hide(), q.show(), b.addClass("opened focused"), l.onSelectOpened.call(b)) : (q.hide(), b.removeClass("opened dropup dropdown"), e("div.jq-selectbox").filter(".opened").length && l.onSelectClosed.call(b)), k.length && (k.val("").keyup(), S.hide(), k.keyup(function () {
                                var t = e(this).val();j.each(function () {
                                    e(this).html().match(new RegExp(".*?" + t + ".*?", "i")) ? e(this).show() : e(this).hide();
                                }), "" === c.first().text() && "" !== i.data("placeholder") && j.first().hide(), j.filter(":visible").length < 1 ? S.show() : S.hide();
                            })), j.filter(".selected").length && ("" === i.val() ? _.scrollTop(0) : (_.innerHeight() / s % 2 != 0 && (s /= 2), _.scrollTop(_.scrollTop() + j.filter(".selected").position().top - _.innerHeight() / 2 + s))), d(_);
                        }
                    }), j.hover(function () {
                        e(this).siblings().removeClass("selected");
                    });var W = j.filter(".selected").text();j.filter(":not(.disabled):not(.optgroup)").click(function () {
                        i.focus();var t = e(this),
                            s = t.text();if (!t.is(".selected")) {
                            var o = t.index();o -= t.prevAll(".optgroup").length, t.addClass("selected sel").siblings().removeClass("selected sel"), c.prop("selected", !1).eq(o).prop("selected", !0), W = s, y.text(s), b.data("jqfs-class") && b.removeClass(b.data("jqfs-class")), b.data("jqfs-class", t.data("jqfs-class")), b.addClass(t.data("jqfs-class")), i.change();
                        }q.hide(), b.removeClass("opened dropup dropdown"), l.onSelectClosed.call(b);
                    }), q.mouseout(function () {
                        e("li.sel", q).addClass("selected");
                    }), i.on("change.styler", function () {
                        y.text(c.filter(":selected").text()).removeClass("placeholder"), j.removeClass("selected sel").not(".optgroup").eq(i[0].selectedIndex).addClass("selected sel"), c.first().text() != j.filter(".selected").text() ? b.addClass("changed") : b.removeClass("changed");
                    }).on("focus.styler", function () {
                        b.addClass("focused"), e("div.jqselect").not(".focused").removeClass("opened dropup dropdown").find("div.jq-selectbox__dropdown").hide();
                    }).on("blur.styler", function () {
                        b.removeClass("focused");
                    }).on("keydown.styler keyup.styler", function (e) {
                        var t = j.data("li-height");"" === i.val() ? y.text(h).addClass("placeholder") : y.text(c.filter(":selected").text()), j.removeClass("selected sel").not(".optgroup").eq(i[0].selectedIndex).addClass("selected sel"), 38 != e.which && 37 != e.which && 33 != e.which && 36 != e.which || ("" === i.val() ? _.scrollTop(0) : _.scrollTop(_.scrollTop() + j.filter(".selected").position().top)), 40 != e.which && 39 != e.which && 34 != e.which && 35 != e.which || _.scrollTop(_.scrollTop() + j.filter(".selected").position().top - _.innerHeight() + t), 13 == e.which && (e.preventDefault(), q.hide(), b.removeClass("opened dropup dropdown"), l.onSelectClosed.call(b));
                    }).on("keydown.styler", function (e) {
                        32 == e.which && (e.preventDefault(), x.click());
                    }), s.registered || (e(document).on("click", s), s.registered = !0);
                }();
            };f(), i.on("refresh", function () {
                i.off(".styler").parent().before(i).remove(), f();
            });
        } else i.is(":reset") && i.on("click", function () {
            setTimeout(function () {
                i.closest("form").find("input, select").trigger("refresh");
            }, 1);
        });
    }, destroy: function destroy() {
        var t = e(this.element);t.is(":checkbox") || t.is(":radio") ? (t.removeData("_" + i).off(".styler refresh").removeAttr("style").parent().before(t).remove(), t.closest("label").add('label[for="' + t.attr("id") + '"]').off(".styler")) : t.is('input[type="number"]') ? t.removeData("_" + i).off(".styler refresh").closest(".jq-number").before(t).remove() : (t.is(":file") || t.is("select")) && t.removeData("_" + i).off(".styler refresh").removeAttr("style").parent().before(t).remove();
    } }, e.fn[i] = function (s) {
        var l = arguments;if (void 0 === s || "object" == (typeof s === "undefined" ? "undefined" : _typeof(s))) return this.each(function () {
            e.data(this, "_" + i) || e.data(this, "_" + i, new t(this, s));
        }).promise().done(function () {
            var t = e(this[0]).data("_" + i);t && t.options.onFormStyled.call();
        }), this;if ("string" == typeof s && "_" !== s[0] && "init" !== s) {
            var o;return this.each(function () {
                var a = e.data(this, "_" + i);a instanceof t && "function" == typeof a[s] && (o = a[s].apply(a, Array.prototype.slice.call(l, 1)));
            }), void 0 !== o ? o : this;
        }
    }, s.registered = !1;
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 jQuery Masked Input Plugin
 Copyright (c) 2007 - 2015 Josh Bush (digitalbush.com)
 Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
 Version: 1.4.1
 */
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : a("object" == (typeof exports === "undefined" ? "undefined" : _typeof(exports)) ? require("jquery") : jQuery);
}(function (a) {
    var b,
        c = navigator.userAgent,
        d = /iphone/i.test(c),
        e = /chrome/i.test(c),
        f = /android/i.test(c);a.mask = { definitions: { 9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]" }, autoclear: !0, dataName: "rawMaskFn", placeholder: "_" }, a.fn.extend({ caret: function caret(a, b) {
        var c;if (0 !== this.length && !this.is(":hidden")) return "number" == typeof a ? (b = "number" == typeof b ? b : a, this.each(function () {
            this.setSelectionRange ? this.setSelectionRange(a, b) : this.createTextRange && (c = this.createTextRange(), c.collapse(!0), c.moveEnd("character", b), c.moveStart("character", a), c.select());
        })) : (this[0].setSelectionRange ? (a = this[0].selectionStart, b = this[0].selectionEnd) : document.selection && document.selection.createRange && (c = document.selection.createRange(), a = 0 - c.duplicate().moveStart("character", -1e5), b = a + c.text.length), { begin: a, end: b });
    }, unmask: function unmask() {
        return this.trigger("unmask");
    }, mask: function mask(c, g) {
        var h, i, j, k, l, m, n, o;if (!c && this.length > 0) {
            h = a(this[0]);var p = h.data(a.mask.dataName);return p ? p() : void 0;
        }return g = a.extend({ autoclear: a.mask.autoclear, placeholder: a.mask.placeholder, completed: null }, g), i = a.mask.definitions, j = [], k = n = c.length, l = null, a.each(c.split(""), function (a, b) {
            "?" == b ? (n--, k = a) : i[b] ? (j.push(new RegExp(i[b])), null === l && (l = j.length - 1), k > a && (m = j.length - 1)) : j.push(null);
        }), this.trigger("unmask").each(function () {
            function h() {
                if (g.completed) {
                    for (var a = l; m >= a; a++) {
                        if (j[a] && C[a] === p(a)) return;
                    }g.completed.call(B);
                }
            }function p(a) {
                return g.placeholder.charAt(a < g.placeholder.length ? a : 0);
            }function q(a) {
                for (; ++a < n && !j[a];) {}return a;
            }function r(a) {
                for (; --a >= 0 && !j[a];) {}return a;
            }function s(a, b) {
                var c, d;if (!(0 > a)) {
                    for (c = a, d = q(b); n > c; c++) {
                        if (j[c]) {
                            if (!(n > d && j[c].test(C[d]))) break;C[c] = C[d], C[d] = p(d), d = q(d);
                        }
                    }z(), B.caret(Math.max(l, a));
                }
            }function t(a) {
                var b, c, d, e;for (b = a, c = p(a); n > b; b++) {
                    if (j[b]) {
                        if (d = q(b), e = C[b], C[b] = c, !(n > d && j[d].test(e))) break;c = e;
                    }
                }
            }function u() {
                var a = B.val(),
                    b = B.caret();if (o && o.length && o.length > a.length) {
                    for (A(!0); b.begin > 0 && !j[b.begin - 1];) {
                        b.begin--;
                    }if (0 === b.begin) for (; b.begin < l && !j[b.begin];) {
                        b.begin++;
                    }B.caret(b.begin, b.begin);
                } else {
                    for (A(!0); b.begin < n && !j[b.begin];) {
                        b.begin++;
                    }B.caret(b.begin, b.begin);
                }h();
            }function v() {
                A(), B.val() != E && B.change();
            }function w(a) {
                if (!B.prop("readonly")) {
                    var b,
                        c,
                        e,
                        f = a.which || a.keyCode;o = B.val(), 8 === f || 46 === f || d && 127 === f ? (b = B.caret(), c = b.begin, e = b.end, e - c === 0 && (c = 46 !== f ? r(c) : e = q(c - 1), e = 46 === f ? q(e) : e), y(c, e), s(c, e - 1), a.preventDefault()) : 13 === f ? v.call(this, a) : 27 === f && (B.val(E), B.caret(0, A()), a.preventDefault());
                }
            }function x(b) {
                if (!B.prop("readonly")) {
                    var c,
                        d,
                        e,
                        g = b.which || b.keyCode,
                        i = B.caret();if (!(b.ctrlKey || b.altKey || b.metaKey || 32 > g) && g && 13 !== g) {
                        if (i.end - i.begin !== 0 && (y(i.begin, i.end), s(i.begin, i.end - 1)), c = q(i.begin - 1), n > c && (d = String.fromCharCode(g), j[c].test(d))) {
                            if (t(c), C[c] = d, z(), e = q(c), f) {
                                var k = function k() {
                                    a.proxy(a.fn.caret, B, e)();
                                };setTimeout(k, 0);
                            } else B.caret(e);i.begin <= m && h();
                        }b.preventDefault();
                    }
                }
            }function y(a, b) {
                var c;for (c = a; b > c && n > c; c++) {
                    j[c] && (C[c] = p(c));
                }
            }function z() {
                B.val(C.join(""));
            }function A(a) {
                var b,
                    c,
                    d,
                    e = B.val(),
                    f = -1;for (b = 0, d = 0; n > b; b++) {
                    if (j[b]) {
                        for (C[b] = p(b); d++ < e.length;) {
                            if (c = e.charAt(d - 1), j[b].test(c)) {
                                C[b] = c, f = b;break;
                            }
                        }if (d > e.length) {
                            y(b + 1, n);break;
                        }
                    } else C[b] === e.charAt(d) && d++, k > b && (f = b);
                }return a ? z() : k > f + 1 ? g.autoclear || C.join("") === D ? (B.val() && B.val(""), y(0, n)) : z() : (z(), B.val(B.val().substring(0, f + 1))), k ? b : l;
            }var B = a(this),
                C = a.map(c.split(""), function (a, b) {
                    return "?" != a ? i[a] ? p(b) : a : void 0;
                }),
                D = C.join(""),
                E = B.val();B.data(a.mask.dataName, function () {
                return a.map(C, function (a, b) {
                    return j[b] && a != p(b) ? a : null;
                }).join("");
            }), B.one("unmask", function () {
                B.off(".mask").removeData(a.mask.dataName);
            }).on("focus.mask", function () {
                if (!B.prop("readonly")) {
                    clearTimeout(b);var a;E = B.val(), a = A(), b = setTimeout(function () {
                        B.get(0) === document.activeElement && (z(), a == c.replace("?", "").length ? B.caret(0, a) : B.caret(a));
                    }, 10);
                }
            }).on("blur.mask", v).on("keydown.mask", w).on("keypress.mask", x).on("input.mask paste.mask", function () {
                B.prop("readonly") || setTimeout(function () {
                    var a = A(!0);B.caret(a), h();
                }, 0);
            }), e && f && B.off("input.mask").on("input.mask", u), A();
        });
    } });
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! jQuery Validation Plugin - v1.15.0 - 2/24/2016
 * http://jqueryvalidation.org/
 * Copyright (c) 2016 Jörn Zaefferer; Licensed MIT */
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery"], a) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = a(require("jquery")) : a(jQuery);
}(function (a) {
    a.extend(a.fn, { validate: function validate(b) {
        if (!this.length) return void (b && b.debug && window.console && console.warn("Nothing selected, can't validate, returning nothing."));var c = a.data(this[0], "validator");return c ? c : (this.attr("novalidate", "novalidate"), c = new a.validator(b, this[0]), a.data(this[0], "validator", c), c.settings.onsubmit && (this.on("click.validate", ":submit", function (b) {
            c.settings.submitHandler && (c.submitButton = b.target), a(this).hasClass("cancel") && (c.cancelSubmit = !0), void 0 !== a(this).attr("formnovalidate") && (c.cancelSubmit = !0);
        }), this.on("submit.validate", function (b) {
            function d() {
                var d, e;return c.settings.submitHandler ? (c.submitButton && (d = a("<input type='hidden'/>").attr("name", c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)), e = c.settings.submitHandler.call(c, c.currentForm, b), c.submitButton && d.remove(), void 0 !== e ? e : !1) : !0;
            }return c.settings.debug && b.preventDefault(), c.cancelSubmit ? (c.cancelSubmit = !1, d()) : c.form() ? c.pendingRequest ? (c.formSubmitted = !0, !1) : d() : (c.focusInvalid(), !1);
        })), c);
    }, valid: function valid() {
        var b, c, d;return a(this[0]).is("form") ? b = this.validate().form() : (d = [], b = !0, c = a(this[0].form).validate(), this.each(function () {
            b = c.element(this) && b, b || (d = d.concat(c.errorList));
        }), c.errorList = d), b;
    }, rules: function rules(b, c) {
        if (this.length) {
            var d,
                e,
                f,
                g,
                h,
                i,
                j = this[0];if (b) switch (d = a.data(j.form, "validator").settings, e = d.rules, f = a.validator.staticRules(j), b) {case "add":
                a.extend(f, a.validator.normalizeRule(c)), delete f.messages, e[j.name] = f, c.messages && (d.messages[j.name] = a.extend(d.messages[j.name], c.messages));break;case "remove":
                return c ? (i = {}, a.each(c.split(/\s/), function (b, c) {
                    i[c] = f[c], delete f[c], "required" === c && a(j).removeAttr("aria-required");
                }), i) : (delete e[j.name], f);}return g = a.validator.normalizeRules(a.extend({}, a.validator.classRules(j), a.validator.attributeRules(j), a.validator.dataRules(j), a.validator.staticRules(j)), j), g.required && (h = g.required, delete g.required, g = a.extend({ required: h }, g), a(j).attr("aria-required", "true")), g.remote && (h = g.remote, delete g.remote, g = a.extend(g, { remote: h })), g;
        }
    } }), a.extend(a.expr[":"], { blank: function blank(b) {
        return !a.trim("" + a(b).val());
    }, filled: function filled(b) {
        var c = a(b).val();return null !== c && !!a.trim("" + c);
    }, unchecked: function unchecked(b) {
        return !a(b).prop("checked");
    } }), a.validator = function (b, c) {
        this.settings = a.extend(!0, {}, a.validator.defaults, b), this.currentForm = c, this.init();
    }, a.validator.format = function (b, c) {
        return 1 === arguments.length ? function () {
            var c = a.makeArray(arguments);return c.unshift(b), a.validator.format.apply(this, c);
        } : void 0 === c ? b : (arguments.length > 2 && c.constructor !== Array && (c = a.makeArray(arguments).slice(1)), c.constructor !== Array && (c = [c]), a.each(c, function (a, c) {
            b = b.replace(new RegExp("\\{" + a + "\\}", "g"), function () {
                return c;
            });
        }), b);
    }, a.extend(a.validator, { defaults: { messages: {}, groups: {}, rules: {}, errorClass: "error", pendingClass: "pending", validClass: "valid", errorElement: "label", focusCleanup: !1, focusInvalid: !0, errorContainer: a([]), errorLabelContainer: a([]), onsubmit: !0, ignore: ":hidden", ignoreTitle: !1, onfocusin: function onfocusin(a) {
        this.lastActive = a, this.settings.focusCleanup && (this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass), this.hideThese(this.errorsFor(a)));
    }, onfocusout: function onfocusout(a) {
        this.checkable(a) || !(a.name in this.submitted) && this.optional(a) || this.element(a);
    }, onkeyup: function onkeyup(b, c) {
        var d = [16, 17, 18, 20, 35, 36, 37, 38, 39, 40, 45, 144, 225];9 === c.which && "" === this.elementValue(b) || -1 !== a.inArray(c.keyCode, d) || (b.name in this.submitted || b.name in this.invalid) && this.element(b);
    }, onclick: function onclick(a) {
        a.name in this.submitted ? this.element(a) : a.parentNode.name in this.submitted && this.element(a.parentNode);
    }, highlight: function highlight(b, c, d) {
        "radio" === b.type ? this.findByName(b.name).addClass(c).removeClass(d) : a(b).addClass(c).removeClass(d);
    }, unhighlight: function unhighlight(b, c, d) {
        "radio" === b.type ? this.findByName(b.name).removeClass(c).addClass(d) : a(b).removeClass(c).addClass(d);
    } }, setDefaults: function setDefaults(b) {
        a.extend(a.validator.defaults, b);
    }, messages: { required: "This field is required.", remote: "Please fix this field.", email: "Please enter a valid email address.", url: "Please enter a valid URL.", date: "Please enter a valid date.", dateISO: "Please enter a valid date ( ISO ).", number: "Please enter a valid number.", digits: "Please enter only digits.", equalTo: "Please enter the same value again.", maxlength: a.validator.format("Please enter no more than {0} characters."), minlength: a.validator.format("Please enter at least {0} characters."), rangelength: a.validator.format("Please enter a value between {0} and {1} characters long."), range: a.validator.format("Please enter a value between {0} and {1}."), max: a.validator.format("Please enter a value less than or equal to {0}."), min: a.validator.format("Please enter a value greater than or equal to {0}."), step: a.validator.format("Please enter a multiple of {0}.") }, autoCreateRanges: !1, prototype: { init: function init() {
        function b(b) {
            var c = a.data(this.form, "validator"),
                d = "on" + b.type.replace(/^validate/, ""),
                e = c.settings;e[d] && !a(this).is(e.ignore) && e[d].call(c, this, b);
        }this.labelContainer = a(this.settings.errorLabelContainer), this.errorContext = this.labelContainer.length && this.labelContainer || a(this.currentForm), this.containers = a(this.settings.errorContainer).add(this.settings.errorLabelContainer), this.submitted = {}, this.valueCache = {}, this.pendingRequest = 0, this.pending = {}, this.invalid = {}, this.reset();var c,
            d = this.groups = {};a.each(this.settings.groups, function (b, c) {
            "string" == typeof c && (c = c.split(/\s/)), a.each(c, function (a, c) {
                d[c] = b;
            });
        }), c = this.settings.rules, a.each(c, function (b, d) {
            c[b] = a.validator.normalizeRule(d);
        }), a(this.currentForm).on("focusin.validate focusout.validate keyup.validate", ":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable]", b).on("click.validate", "select, option, [type='radio'], [type='checkbox']", b), this.settings.invalidHandler && a(this.currentForm).on("invalid-form.validate", this.settings.invalidHandler), a(this.currentForm).find("[required], [data-rule-required], .required").attr("aria-required", "true");
    }, form: function form() {
        return this.checkForm(), a.extend(this.submitted, this.errorMap), this.invalid = a.extend({}, this.errorMap), this.valid() || a(this.currentForm).triggerHandler("invalid-form", [this]), this.showErrors(), this.valid();
    }, checkForm: function checkForm() {
        this.prepareForm();for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) {
            this.check(b[a]);
        }return this.valid();
    }, element: function element(b) {
        var c,
            d,
            e = this.clean(b),
            f = this.validationTargetFor(e),
            g = this,
            h = !0;return void 0 === f ? delete this.invalid[e.name] : (this.prepareElement(f), this.currentElements = a(f), d = this.groups[f.name], d && a.each(this.groups, function (a, b) {
            b === d && a !== f.name && (e = g.validationTargetFor(g.clean(g.findByName(a))), e && e.name in g.invalid && (g.currentElements.push(e), h = h && g.check(e)));
        }), c = this.check(f) !== !1, h = h && c, c ? this.invalid[f.name] = !1 : this.invalid[f.name] = !0, this.numberOfInvalids() || (this.toHide = this.toHide.add(this.containers)), this.showErrors(), a(b).attr("aria-invalid", !c)), h;
    }, showErrors: function showErrors(b) {
        if (b) {
            var c = this;a.extend(this.errorMap, b), this.errorList = a.map(this.errorMap, function (a, b) {
                return { message: a, element: c.findByName(b)[0] };
            }), this.successList = a.grep(this.successList, function (a) {
                return !(a.name in b);
            });
        }this.settings.showErrors ? this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors();
    }, resetForm: function resetForm() {
        a.fn.resetForm && a(this.currentForm).resetForm(), this.invalid = {}, this.submitted = {}, this.prepareForm(), this.hideErrors();var b = this.elements().removeData("previousValue").removeAttr("aria-invalid");this.resetElements(b);
    }, resetElements: function resetElements(a) {
        var b;if (this.settings.unhighlight) for (b = 0; a[b]; b++) {
            this.settings.unhighlight.call(this, a[b], this.settings.errorClass, ""), this.findByName(a[b].name).removeClass(this.settings.validClass);
        } else a.removeClass(this.settings.errorClass).removeClass(this.settings.validClass);
    }, numberOfInvalids: function numberOfInvalids() {
        return this.objectLength(this.invalid);
    }, objectLength: function objectLength(a) {
        var b,
            c = 0;for (b in a) {
            a[b] && c++;
        }return c;
    }, hideErrors: function hideErrors() {
        this.hideThese(this.toHide);
    }, hideThese: function hideThese(a) {
        a.not(this.containers).text(""), this.addWrapper(a).hide();
    }, valid: function valid() {
        return 0 === this.size();
    }, size: function size() {
        return this.errorList.length;
    }, focusInvalid: function focusInvalid() {
        if (this.settings.focusInvalid) try {
            a(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin");
        } catch (b) {}
    }, findLastActive: function findLastActive() {
        var b = this.lastActive;return b && 1 === a.grep(this.errorList, function (a) {
                return a.element.name === b.name;
            }).length && b;
    }, elements: function elements() {
        var b = this,
            c = {};return a(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function () {
            var d = this.name || a(this).attr("name");return !d && b.settings.debug && window.console && console.error("%o has no name assigned", this), this.hasAttribute("contenteditable") && (this.form = a(this).closest("form")[0]), d in c || !b.objectLength(a(this).rules()) ? !1 : (c[d] = !0, !0);
        });
    }, clean: function clean(b) {
        return a(b)[0];
    }, errors: function errors() {
        var b = this.settings.errorClass.split(" ").join(".");return a(this.settings.errorElement + "." + b, this.errorContext);
    }, resetInternals: function resetInternals() {
        this.successList = [], this.errorList = [], this.errorMap = {}, this.toShow = a([]), this.toHide = a([]);
    }, reset: function reset() {
        this.resetInternals(), this.currentElements = a([]);
    }, prepareForm: function prepareForm() {
        this.reset(), this.toHide = this.errors().add(this.containers);
    }, prepareElement: function prepareElement(a) {
        this.reset(), this.toHide = this.errorsFor(a);
    }, elementValue: function elementValue(b) {
        var c,
            d,
            e = a(b),
            f = b.type;return "radio" === f || "checkbox" === f ? this.findByName(b.name).filter(":checked").val() : "number" === f && "undefined" != typeof b.validity ? b.validity.badInput ? "NaN" : e.val() : (c = b.hasAttribute("contenteditable") ? e.text() : e.val(), "file" === f ? "C:\\fakepath\\" === c.substr(0, 12) ? c.substr(12) : (d = c.lastIndexOf("/"), d >= 0 ? c.substr(d + 1) : (d = c.lastIndexOf("\\"), d >= 0 ? c.substr(d + 1) : c)) : "string" == typeof c ? c.replace(/\r/g, "") : c);
    }, check: function check(b) {
        b = this.validationTargetFor(this.clean(b));var c,
            d,
            e,
            f = a(b).rules(),
            g = a.map(f, function (a, b) {
                return b;
            }).length,
            h = !1,
            i = this.elementValue(b);if ("function" == typeof f.normalizer) {
            if (i = f.normalizer.call(b, i), "string" != typeof i) throw new TypeError("The normalizer should return a string value.");delete f.normalizer;
        }for (d in f) {
            e = { method: d, parameters: f[d] };try {
                if (c = a.validator.methods[d].call(this, i, b, e.parameters), "dependency-mismatch" === c && 1 === g) {
                    h = !0;continue;
                }if (h = !1, "pending" === c) return void (this.toHide = this.toHide.not(this.errorsFor(b)));if (!c) return this.formatAndAdd(b, e), !1;
            } catch (j) {
                throw this.settings.debug && window.console && console.log("Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method.", j), j instanceof TypeError && (j.message += ".  Exception occurred when checking element " + b.id + ", check the '" + e.method + "' method."), j;
            }
        }if (!h) return this.objectLength(f) && this.successList.push(b), !0;
    }, customDataMessage: function customDataMessage(b, c) {
        return a(b).data("msg" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()) || a(b).data("msg");
    }, customMessage: function customMessage(a, b) {
        var c = this.settings.messages[a];return c && (c.constructor === String ? c : c[b]);
    }, findDefined: function findDefined() {
        for (var a = 0; a < arguments.length; a++) {
            if (void 0 !== arguments[a]) return arguments[a];
        }
    }, defaultMessage: function defaultMessage(b, c) {
        var d = this.findDefined(this.customMessage(b.name, c.method), this.customDataMessage(b, c.method), !this.settings.ignoreTitle && b.title || void 0, a.validator.messages[c.method], "<strong>Warning: No message defined for " + b.name + "</strong>"),
            e = /\$?\{(\d+)\}/g;return "function" == typeof d ? d = d.call(this, c.parameters, b) : e.test(d) && (d = a.validator.format(d.replace(e, "{$1}"), c.parameters)), d;
    }, formatAndAdd: function formatAndAdd(a, b) {
        var c = this.defaultMessage(a, b);this.errorList.push({ message: c, element: a, method: b.method }), this.errorMap[a.name] = c, this.submitted[a.name] = c;
    }, addWrapper: function addWrapper(a) {
        return this.settings.wrapper && (a = a.add(a.parent(this.settings.wrapper))), a;
    }, defaultShowErrors: function defaultShowErrors() {
        var a, b, c;for (a = 0; this.errorList[a]; a++) {
            c = this.errorList[a], this.settings.highlight && this.settings.highlight.call(this, c.element, this.settings.errorClass, this.settings.validClass), this.showLabel(c.element, c.message);
        }if (this.errorList.length && (this.toShow = this.toShow.add(this.containers)), this.settings.success) for (a = 0; this.successList[a]; a++) {
            this.showLabel(this.successList[a]);
        }if (this.settings.unhighlight) for (a = 0, b = this.validElements(); b[a]; a++) {
            this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass);
        }this.toHide = this.toHide.not(this.toShow), this.hideErrors(), this.addWrapper(this.toShow).show();
    }, validElements: function validElements() {
        return this.currentElements.not(this.invalidElements());
    }, invalidElements: function invalidElements() {
        return a(this.errorList).map(function () {
            return this.element;
        });
    }, showLabel: function showLabel(b, c) {
        var d,
            e,
            f,
            g,
            h = this.errorsFor(b),
            i = this.idOrName(b),
            j = a(b).attr("aria-describedby");h.length ? (h.removeClass(this.settings.validClass).addClass(this.settings.errorClass), h.html(c)) : (h = a("<" + this.settings.errorElement + ">").attr("id", i + "-error").addClass(this.settings.errorClass).html(c || ""), d = h, this.settings.wrapper && (d = h.hide().show().wrap("<" + this.settings.wrapper + "/>").parent()), this.labelContainer.length ? this.labelContainer.append(d) : this.settings.errorPlacement ? this.settings.errorPlacement(d, a(b)) : d.insertAfter(b), h.is("label") ? h.attr("for", i) : 0 === h.parents("label[for='" + this.escapeCssMeta(i) + "']").length && (f = h.attr("id"), j ? j.match(new RegExp("\\b" + this.escapeCssMeta(f) + "\\b")) || (j += " " + f) : j = f, a(b).attr("aria-describedby", j), e = this.groups[b.name], e && (g = this, a.each(g.groups, function (b, c) {
            c === e && a("[name='" + g.escapeCssMeta(b) + "']", g.currentForm).attr("aria-describedby", h.attr("id"));
        })))), !c && this.settings.success && (h.text(""), "string" == typeof this.settings.success ? h.addClass(this.settings.success) : this.settings.success(h, b)), this.toShow = this.toShow.add(h);
    }, errorsFor: function errorsFor(b) {
        var c = this.escapeCssMeta(this.idOrName(b)),
            d = a(b).attr("aria-describedby"),
            e = "label[for='" + c + "'], label[for='" + c + "'] *";return d && (e = e + ", #" + this.escapeCssMeta(d).replace(/\s+/g, ", #")), this.errors().filter(e);
    }, escapeCssMeta: function escapeCssMeta(a) {
        return a.replace(/([\\!"#$%&'()*+,./:;<=>?@\[\]^`{|}~])/g, "\\$1");
    }, idOrName: function idOrName(a) {
        return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name);
    }, validationTargetFor: function validationTargetFor(b) {
        return this.checkable(b) && (b = this.findByName(b.name)), a(b).not(this.settings.ignore)[0];
    }, checkable: function checkable(a) {
        return (/radio|checkbox/i.test(a.type)
        );
    }, findByName: function findByName(b) {
        return a(this.currentForm).find("[name='" + this.escapeCssMeta(b) + "']");
    }, getLength: function getLength(b, c) {
        switch (c.nodeName.toLowerCase()) {case "select":
            return a("option:selected", c).length;case "input":
            if (this.checkable(c)) return this.findByName(c.name).filter(":checked").length;}return b.length;
    }, depend: function depend(a, b) {
        return this.dependTypes[typeof a === "undefined" ? "undefined" : _typeof(a)] ? this.dependTypes[typeof a === "undefined" ? "undefined" : _typeof(a)](a, b) : !0;
    }, dependTypes: { "boolean": function boolean(a) {
        return a;
    }, string: function string(b, c) {
        return !!a(b, c.form).length;
    }, "function": function _function(a, b) {
        return a(b);
    } }, optional: function optional(b) {
        var c = this.elementValue(b);return !a.validator.methods.required.call(this, c, b) && "dependency-mismatch";
    }, startRequest: function startRequest(b) {
        this.pending[b.name] || (this.pendingRequest++, a(b).addClass(this.settings.pendingClass), this.pending[b.name] = !0);
    }, stopRequest: function stopRequest(b, c) {
        this.pendingRequest--, this.pendingRequest < 0 && (this.pendingRequest = 0), delete this.pending[b.name], a(b).removeClass(this.settings.pendingClass), c && 0 === this.pendingRequest && this.formSubmitted && this.form() ? (a(this.currentForm).submit(), this.formSubmitted = !1) : !c && 0 === this.pendingRequest && this.formSubmitted && (a(this.currentForm).triggerHandler("invalid-form", [this]), this.formSubmitted = !1);
    }, previousValue: function previousValue(b, c) {
        return a.data(b, "previousValue") || a.data(b, "previousValue", { old: null, valid: !0, message: this.defaultMessage(b, { method: c }) });
    }, destroy: function destroy() {
        this.resetForm(), a(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur");
    } }, classRuleSettings: { required: { required: !0 }, email: { email: !0 }, url: { url: !0 }, date: { date: !0 }, dateISO: { dateISO: !0 }, number: { number: !0 }, digits: { digits: !0 }, creditcard: { creditcard: !0 } }, addClassRules: function addClassRules(b, c) {
        b.constructor === String ? this.classRuleSettings[b] = c : a.extend(this.classRuleSettings, b);
    }, classRules: function classRules(b) {
        var c = {},
            d = a(b).attr("class");return d && a.each(d.split(" "), function () {
            this in a.validator.classRuleSettings && a.extend(c, a.validator.classRuleSettings[this]);
        }), c;
    }, normalizeAttributeRule: function normalizeAttributeRule(a, b, c, d) {
        /min|max|step/.test(c) && (null === b || /number|range|text/.test(b)) && (d = Number(d), isNaN(d) && (d = void 0)), d || 0 === d ? a[c] = d : b === c && "range" !== b && (a[c] = !0);
    }, attributeRules: function attributeRules(b) {
        var c,
            d,
            e = {},
            f = a(b),
            g = b.getAttribute("type");for (c in a.validator.methods) {
            "required" === c ? (d = b.getAttribute(c), "" === d && (d = !0), d = !!d) : d = f.attr(c), this.normalizeAttributeRule(e, g, c, d);
        }return e.maxlength && /-1|2147483647|524288/.test(e.maxlength) && delete e.maxlength, e;
    }, dataRules: function dataRules(b) {
        var c,
            d,
            e = {},
            f = a(b),
            g = b.getAttribute("type");for (c in a.validator.methods) {
            d = f.data("rule" + c.charAt(0).toUpperCase() + c.substring(1).toLowerCase()), this.normalizeAttributeRule(e, g, c, d);
        }return e;
    }, staticRules: function staticRules(b) {
        var c = {},
            d = a.data(b.form, "validator");return d.settings.rules && (c = a.validator.normalizeRule(d.settings.rules[b.name]) || {}), c;
    }, normalizeRules: function normalizeRules(b, c) {
        return a.each(b, function (d, e) {
            if (e === !1) return void delete b[d];if (e.param || e.depends) {
                var f = !0;switch (_typeof(e.depends)) {case "string":
                    f = !!a(e.depends, c.form).length;break;case "function":
                    f = e.depends.call(c, c);}f ? b[d] = void 0 !== e.param ? e.param : !0 : (a.data(c.form, "validator").resetElements(a(c)), delete b[d]);
            }
        }), a.each(b, function (d, e) {
            b[d] = a.isFunction(e) && "normalizer" !== d ? e(c) : e;
        }), a.each(["minlength", "maxlength"], function () {
            b[this] && (b[this] = Number(b[this]));
        }), a.each(["rangelength", "range"], function () {
            var c;b[this] && (a.isArray(b[this]) ? b[this] = [Number(b[this][0]), Number(b[this][1])] : "string" == typeof b[this] && (c = b[this].replace(/[\[\]]/g, "").split(/[\s,]+/), b[this] = [Number(c[0]), Number(c[1])]));
        }), a.validator.autoCreateRanges && (null != b.min && null != b.max && (b.range = [b.min, b.max], delete b.min, delete b.max), null != b.minlength && null != b.maxlength && (b.rangelength = [b.minlength, b.maxlength], delete b.minlength, delete b.maxlength)), b;
    }, normalizeRule: function normalizeRule(b) {
        if ("string" == typeof b) {
            var c = {};a.each(b.split(/\s/), function () {
                c[this] = !0;
            }), b = c;
        }return b;
    }, addMethod: function addMethod(b, c, d) {
        a.validator.methods[b] = c, a.validator.messages[b] = void 0 !== d ? d : a.validator.messages[b], c.length < 3 && a.validator.addClassRules(b, a.validator.normalizeRule(b));
    }, methods: { required: function required(b, c, d) {
        if (!this.depend(d, c)) return "dependency-mismatch";if ("select" === c.nodeName.toLowerCase()) {
            var e = a(c).val();return e && e.length > 0;
        }return this.checkable(c) ? this.getLength(b, c) > 0 : b.length > 0;
    }, email: function email(a, b) {
        return this.optional(b) || /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a);
    }, url: function url(a, b) {
        return this.optional(b) || /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(a);
    }, date: function date(a, b) {
        return this.optional(b) || !/Invalid|NaN/.test(new Date(a).toString());
    }, dateISO: function dateISO(a, b) {
        return this.optional(b) || /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a);
    }, number: function number(a, b) {
        return this.optional(b) || /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a);
    }, digits: function digits(a, b) {
        return this.optional(b) || /^\d+$/.test(a);
    }, minlength: function minlength(b, c, d) {
        var e = a.isArray(b) ? b.length : this.getLength(b, c);return this.optional(c) || e >= d;
    }, maxlength: function maxlength(b, c, d) {
        var e = a.isArray(b) ? b.length : this.getLength(b, c);return this.optional(c) || d >= e;
    }, rangelength: function rangelength(b, c, d) {
        var e = a.isArray(b) ? b.length : this.getLength(b, c);return this.optional(c) || e >= d[0] && e <= d[1];
    }, min: function min(a, b, c) {
        return this.optional(b) || a >= c;
    }, max: function max(a, b, c) {
        return this.optional(b) || c >= a;
    }, range: function range(a, b, c) {
        return this.optional(b) || a >= c[0] && a <= c[1];
    }, step: function step(b, c, d) {
        var e = a(c).attr("type"),
            f = "Step attribute on input type " + e + " is not supported.",
            g = ["text", "number", "range"],
            h = new RegExp("\\b" + e + "\\b"),
            i = e && !h.test(g.join());if (i) throw new Error(f);return this.optional(c) || b % d === 0;
    }, equalTo: function equalTo(b, c, d) {
        var e = a(d);return this.settings.onfocusout && e.not(".validate-equalTo-blur").length && e.addClass("validate-equalTo-blur").on("blur.validate-equalTo", function () {
            a(c).valid();
        }), b === e.val();
    }, remote: function remote(b, c, d, e) {
        if (this.optional(c)) return "dependency-mismatch";e = "string" == typeof e && e || "remote";var f,
            g,
            h,
            i = this.previousValue(c, e);return this.settings.messages[c.name] || (this.settings.messages[c.name] = {}), i.originalMessage = i.originalMessage || this.settings.messages[c.name][e], this.settings.messages[c.name][e] = i.message, d = "string" == typeof d && { url: d } || d, h = a.param(a.extend({ data: b }, d.data)), i.old === h ? i.valid : (i.old = h, f = this, this.startRequest(c), g = {}, g[c.name] = b, a.ajax(a.extend(!0, { mode: "abort", port: "validate" + c.name, dataType: "json", data: g, context: f.currentForm, success: function success(a) {
            var d,
                g,
                h,
                j = a === !0 || "true" === a;f.settings.messages[c.name][e] = i.originalMessage, j ? (h = f.formSubmitted, f.resetInternals(), f.toHide = f.errorsFor(c), f.formSubmitted = h, f.successList.push(c), f.invalid[c.name] = !1, f.showErrors()) : (d = {}, g = a || f.defaultMessage(c, { method: e, parameters: b }), d[c.name] = i.message = g, f.invalid[c.name] = !0, f.showErrors(d)), i.valid = j, f.stopRequest(c, j);
        } }, d)), "pending");
    } } });var b,
        c = {};a.ajaxPrefilter ? a.ajaxPrefilter(function (a, b, d) {
        var e = a.port;"abort" === a.mode && (c[e] && c[e].abort(), c[e] = d);
    }) : (b = a.ajax, a.ajax = function (d) {
        var e = ("mode" in d ? d : a.ajaxSettings).mode,
            f = ("port" in d ? d : a.ajaxSettings).port;return "abort" === e ? (c[f] && c[f].abort(), c[f] = b.apply(this, arguments), c[f]) : b.apply(this, arguments);
    });
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*! jQuery Validation Plugin - v1.15.0 - 2/24/2016
 * http://jqueryvalidation.org/
 * Copyright (c) 2016 Jörn Zaefferer; Licensed MIT */
!function (a) {
    "function" == typeof define && define.amd ? define(["jquery", "../jquery.validate.min"], a) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = a(require("jquery")) : a(jQuery);
}(function (a) {
    a.extend(a.validator.messages, { required: "Это поле необходимо заполнить.", remote: "Пожалуйста, введите правильное значение.", email: "Пожалуйста, введите корректный адрес электронной почты.", url: "Пожалуйста, введите корректный URL.", date: "Пожалуйста, введите корректную дату.", dateISO: "Пожалуйста, введите корректную дату в формате ISO.", number: "Пожалуйста, введите число.", digits: "Пожалуйста, вводите только цифры.", creditcard: "Пожалуйста, введите правильный номер кредитной карты.", equalTo: "Пожалуйста, введите такое же значение ещё раз.", extension: "Пожалуйста, выберите файл с правильным расширением.", maxlength: a.validator.format("Пожалуйста, введите не больше {0} символов."), minlength: a.validator.format("Пожалуйста, введите не меньше {0} символов."), rangelength: a.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов."), range: a.validator.format("Пожалуйста, введите число от {0} до {1}."), max: a.validator.format("Пожалуйста, введите число, меньшее или равное {0}."), min: a.validator.format("Пожалуйста, введите число, большее или равное {0}.") });
});
"use strict";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * Owl Carousel v2.2.1
 * Copyright 2013-2017 David Deutsch
 * Licensed under  ()
 */
!function (a, b, c, d) {
    function e(b, c) {
        this.settings = null, this.options = a.extend({}, e.Defaults, c), this.$element = a(b), this._handlers = {}, this._plugins = {}, this._supress = {}, this._current = null, this._speed = null, this._coordinates = [], this._breakpoint = null, this._width = null, this._items = [], this._clones = [], this._mergers = [], this._widths = [], this._invalidated = {}, this._pipe = [], this._drag = { time: null, target: null, pointer: null, stage: { start: null, current: null }, direction: null }, this._states = { current: {}, tags: { initializing: ["busy"], animating: ["busy"], dragging: ["interacting"] } }, a.each(["onResize", "onThrottledResize"], a.proxy(function (b, c) {
            this._handlers[c] = a.proxy(this[c], this);
        }, this)), a.each(e.Plugins, a.proxy(function (a, b) {
            this._plugins[a.charAt(0).toLowerCase() + a.slice(1)] = new b(this);
        }, this)), a.each(e.Workers, a.proxy(function (b, c) {
            this._pipe.push({ filter: c.filter, run: a.proxy(c.run, this) });
        }, this)), this.setup(), this.initialize();
    }e.Defaults = { items: 3, loop: !1, center: !1, rewind: !1, mouseDrag: !0, touchDrag: !0, pullDrag: !0, freeDrag: !1, margin: 0, stagePadding: 0, merge: !1, mergeFit: !0, autoWidth: !1, startPosition: 0, rtl: !1, smartSpeed: 250, fluidSpeed: !1, dragEndSpeed: !1, responsive: {}, responsiveRefreshRate: 200, responsiveBaseElement: b, fallbackEasing: "swing", info: !1, nestedItemSelector: !1, itemElement: "div", stageElement: "div", refreshClass: "owl-refresh", loadedClass: "owl-loaded", loadingClass: "owl-loading", rtlClass: "owl-rtl", responsiveClass: "owl-responsive", dragClass: "owl-drag", itemClass: "owl-item", stageClass: "owl-stage", stageOuterClass: "owl-stage-outer", grabClass: "owl-grab" }, e.Width = { Default: "default", Inner: "inner", Outer: "outer" }, e.Type = { Event: "event", State: "state" }, e.Plugins = {}, e.Workers = [{ filter: ["width", "settings"], run: function run() {
        this._width = this.$element.width();
    } }, { filter: ["width", "items", "settings"], run: function run(a) {
        a.current = this._items && this._items[this.relative(this._current)];
    } }, { filter: ["items", "settings"], run: function run() {
        this.$stage.children(".cloned").remove();
    } }, { filter: ["width", "items", "settings"], run: function run(a) {
        var b = this.settings.margin || "",
            c = !this.settings.autoWidth,
            d = this.settings.rtl,
            e = { width: "auto", "margin-left": d ? b : "", "margin-right": d ? "" : b };!c && this.$stage.children().css(e), a.css = e;
    } }, { filter: ["width", "items", "settings"], run: function run(a) {
        var b = (this.width() / this.settings.items).toFixed(3) - this.settings.margin,
            c = null,
            d = this._items.length,
            e = !this.settings.autoWidth,
            f = [];for (a.items = { merge: !1, width: b }; d--;) {
            c = this._mergers[d], c = this.settings.mergeFit && Math.min(c, this.settings.items) || c, a.items.merge = c > 1 || a.items.merge, f[d] = e ? b * c : this._items[d].width();
        }this._widths = f;
    } }, { filter: ["items", "settings"], run: function run() {
        var b = [],
            c = this._items,
            d = this.settings,
            e = Math.max(2 * d.items, 4),
            f = 2 * Math.ceil(c.length / 2),
            g = d.loop && c.length ? d.rewind ? e : Math.max(e, f) : 0,
            h = "",
            i = "";for (g /= 2; g--;) {
            b.push(this.normalize(b.length / 2, !0)), h += c[b[b.length - 1]][0].outerHTML, b.push(this.normalize(c.length - 1 - (b.length - 1) / 2, !0)), i = c[b[b.length - 1]][0].outerHTML + i;
        }this._clones = b, a(h).addClass("cloned").appendTo(this.$stage), a(i).addClass("cloned").prependTo(this.$stage);
    } }, { filter: ["width", "items", "settings"], run: function run() {
        for (var a = this.settings.rtl ? 1 : -1, b = this._clones.length + this._items.length, c = -1, d = 0, e = 0, f = []; ++c < b;) {
            d = f[c - 1] || 0, e = this._widths[this.relative(c)] + this.settings.margin, f.push(d + e * a);
        }this._coordinates = f;
    } }, { filter: ["width", "items", "settings"], run: function run() {
        var a = this.settings.stagePadding,
            b = this._coordinates,
            c = { width: Math.ceil(Math.abs(b[b.length - 1])) + 2 * a, "padding-left": a || "", "padding-right": a || "" };this.$stage.css(c);
    } }, { filter: ["width", "items", "settings"], run: function run(a) {
        var b = this._coordinates.length,
            c = !this.settings.autoWidth,
            d = this.$stage.children();if (c && a.items.merge) for (; b--;) {
            a.css.width = this._widths[this.relative(b)], d.eq(b).css(a.css);
        } else c && (a.css.width = a.items.width, d.css(a.css));
    } }, { filter: ["items"], run: function run() {
        this._coordinates.length < 1 && this.$stage.removeAttr("style");
    } }, { filter: ["width", "items", "settings"], run: function run(a) {
        a.current = a.current ? this.$stage.children().index(a.current) : 0, a.current = Math.max(this.minimum(), Math.min(this.maximum(), a.current)), this.reset(a.current);
    } }, { filter: ["position"], run: function run() {
        this.animate(this.coordinates(this._current));
    } }, { filter: ["width", "position", "items", "settings"], run: function run() {
        var a,
            b,
            c,
            d,
            e = this.settings.rtl ? 1 : -1,
            f = 2 * this.settings.stagePadding,
            g = this.coordinates(this.current()) + f,
            h = g + this.width() * e,
            i = [];for (c = 0, d = this._coordinates.length; c < d; c++) {
            a = this._coordinates[c - 1] || 0, b = Math.abs(this._coordinates[c]) + f * e, (this.op(a, "<=", g) && this.op(a, ">", h) || this.op(b, "<", g) && this.op(b, ">", h)) && i.push(c);
        }this.$stage.children(".active").removeClass("active"), this.$stage.children(":eq(" + i.join("), :eq(") + ")").addClass("active"), this.settings.center && (this.$stage.children(".center").removeClass("center"), this.$stage.children().eq(this.current()).addClass("center"));
    } }], e.prototype.initialize = function () {
        if (this.enter("initializing"), this.trigger("initialize"), this.$element.toggleClass(this.settings.rtlClass, this.settings.rtl), this.settings.autoWidth && !this.is("pre-loading")) {
            var b, c, e;b = this.$element.find("img"), c = this.settings.nestedItemSelector ? "." + this.settings.nestedItemSelector : d, e = this.$element.children(c).width(), b.length && e <= 0 && this.preloadAutoWidthImages(b);
        }this.$element.addClass(this.options.loadingClass), this.$stage = a("<" + this.settings.stageElement + ' class="' + this.settings.stageClass + '"/>').wrap('<div class="' + this.settings.stageOuterClass + '"/>'), this.$element.append(this.$stage.parent()), this.replace(this.$element.children().not(this.$stage.parent())), this.$element.is(":visible") ? this.refresh() : this.invalidate("width"), this.$element.removeClass(this.options.loadingClass).addClass(this.options.loadedClass), this.registerEventHandlers(), this.leave("initializing"), this.trigger("initialized");
    }, e.prototype.setup = function () {
        var b = this.viewport(),
            c = this.options.responsive,
            d = -1,
            e = null;c ? (a.each(c, function (a) {
            a <= b && a > d && (d = Number(a));
        }), e = a.extend({}, this.options, c[d]), "function" == typeof e.stagePadding && (e.stagePadding = e.stagePadding()), delete e.responsive, e.responsiveClass && this.$element.attr("class", this.$element.attr("class").replace(new RegExp("(" + this.options.responsiveClass + "-)\\S+\\s", "g"), "$1" + d))) : e = a.extend({}, this.options), this.trigger("change", { property: { name: "settings", value: e } }), this._breakpoint = d, this.settings = e, this.invalidate("settings"), this.trigger("changed", { property: { name: "settings", value: this.settings } });
    }, e.prototype.optionsLogic = function () {
        this.settings.autoWidth && (this.settings.stagePadding = !1, this.settings.merge = !1);
    }, e.prototype.prepare = function (b) {
        var c = this.trigger("prepare", { content: b });return c.data || (c.data = a("<" + this.settings.itemElement + "/>").addClass(this.options.itemClass).append(b)), this.trigger("prepared", { content: c.data }), c.data;
    }, e.prototype.update = function () {
        for (var b = 0, c = this._pipe.length, d = a.proxy(function (a) {
            return this[a];
        }, this._invalidated), e = {}; b < c;) {
            (this._invalidated.all || a.grep(this._pipe[b].filter, d).length > 0) && this._pipe[b].run(e), b++;
        }this._invalidated = {}, !this.is("valid") && this.enter("valid");
    }, e.prototype.width = function (a) {
        switch (a = a || e.Width.Default) {case e.Width.Inner:case e.Width.Outer:
            return this._width;default:
            return this._width - 2 * this.settings.stagePadding + this.settings.margin;}
    }, e.prototype.refresh = function () {
        this.enter("refreshing"), this.trigger("refresh"), this.setup(), this.optionsLogic(), this.$element.addClass(this.options.refreshClass), this.update(), this.$element.removeClass(this.options.refreshClass), this.leave("refreshing"), this.trigger("refreshed");
    }, e.prototype.onThrottledResize = function () {
        b.clearTimeout(this.resizeTimer), this.resizeTimer = b.setTimeout(this._handlers.onResize, this.settings.responsiveRefreshRate);
    }, e.prototype.onResize = function () {
        return !!this._items.length && this._width !== this.$element.width() && !!this.$element.is(":visible") && (this.enter("resizing"), this.trigger("resize").isDefaultPrevented() ? (this.leave("resizing"), !1) : (this.invalidate("width"), this.refresh(), this.leave("resizing"), void this.trigger("resized")));
    }, e.prototype.registerEventHandlers = function () {
        a.support.transition && this.$stage.on(a.support.transition.end + ".owl.core", a.proxy(this.onTransitionEnd, this)), this.settings.responsive !== !1 && this.on(b, "resize", this._handlers.onThrottledResize), this.settings.mouseDrag && (this.$element.addClass(this.options.dragClass), this.$stage.on("mousedown.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("dragstart.owl.core selectstart.owl.core", function () {
            return !1;
        })), this.settings.touchDrag && (this.$stage.on("touchstart.owl.core", a.proxy(this.onDragStart, this)), this.$stage.on("touchcancel.owl.core", a.proxy(this.onDragEnd, this)));
    }, e.prototype.onDragStart = function (b) {
        var d = null;3 !== b.which && (a.support.transform ? (d = this.$stage.css("transform").replace(/.*\(|\)| /g, "").split(","), d = { x: d[16 === d.length ? 12 : 4], y: d[16 === d.length ? 13 : 5] }) : (d = this.$stage.position(), d = { x: this.settings.rtl ? d.left + this.$stage.width() - this.width() + this.settings.margin : d.left, y: d.top }), this.is("animating") && (a.support.transform ? this.animate(d.x) : this.$stage.stop(), this.invalidate("position")), this.$element.toggleClass(this.options.grabClass, "mousedown" === b.type), this.speed(0), this._drag.time = new Date().getTime(), this._drag.target = a(b.target), this._drag.stage.start = d, this._drag.stage.current = d, this._drag.pointer = this.pointer(b), a(c).on("mouseup.owl.core touchend.owl.core", a.proxy(this.onDragEnd, this)), a(c).one("mousemove.owl.core touchmove.owl.core", a.proxy(function (b) {
            var d = this.difference(this._drag.pointer, this.pointer(b));a(c).on("mousemove.owl.core touchmove.owl.core", a.proxy(this.onDragMove, this)), Math.abs(d.x) < Math.abs(d.y) && this.is("valid") || (b.preventDefault(), this.enter("dragging"), this.trigger("drag"));
        }, this)));
    }, e.prototype.onDragMove = function (a) {
        var b = null,
            c = null,
            d = null,
            e = this.difference(this._drag.pointer, this.pointer(a)),
            f = this.difference(this._drag.stage.start, e);this.is("dragging") && (a.preventDefault(), this.settings.loop ? (b = this.coordinates(this.minimum()), c = this.coordinates(this.maximum() + 1) - b, f.x = ((f.x - b) % c + c) % c + b) : (b = this.settings.rtl ? this.coordinates(this.maximum()) : this.coordinates(this.minimum()), c = this.settings.rtl ? this.coordinates(this.minimum()) : this.coordinates(this.maximum()), d = this.settings.pullDrag ? -1 * e.x / 5 : 0, f.x = Math.max(Math.min(f.x, b + d), c + d)), this._drag.stage.current = f, this.animate(f.x));
    }, e.prototype.onDragEnd = function (b) {
        var d = this.difference(this._drag.pointer, this.pointer(b)),
            e = this._drag.stage.current,
            f = d.x > 0 ^ this.settings.rtl ? "left" : "right";a(c).off(".owl.core"), this.$element.removeClass(this.options.grabClass), (0 !== d.x && this.is("dragging") || !this.is("valid")) && (this.speed(this.settings.dragEndSpeed || this.settings.smartSpeed), this.current(this.closest(e.x, 0 !== d.x ? f : this._drag.direction)), this.invalidate("position"), this.update(), this._drag.direction = f, (Math.abs(d.x) > 3 || new Date().getTime() - this._drag.time > 300) && this._drag.target.one("click.owl.core", function () {
            return !1;
        })), this.is("dragging") && (this.leave("dragging"), this.trigger("dragged"));
    }, e.prototype.closest = function (b, c) {
        var d = -1,
            e = 30,
            f = this.width(),
            g = this.coordinates();return this.settings.freeDrag || a.each(g, a.proxy(function (a, h) {
            return "left" === c && b > h - e && b < h + e ? d = a : "right" === c && b > h - f - e && b < h - f + e ? d = a + 1 : this.op(b, "<", h) && this.op(b, ">", g[a + 1] || h - f) && (d = "left" === c ? a + 1 : a), d === -1;
        }, this)), this.settings.loop || (this.op(b, ">", g[this.minimum()]) ? d = b = this.minimum() : this.op(b, "<", g[this.maximum()]) && (d = b = this.maximum())), d;
    }, e.prototype.animate = function (b) {
        var c = this.speed() > 0;this.is("animating") && this.onTransitionEnd(), c && (this.enter("animating"), this.trigger("translate")), a.support.transform3d && a.support.transition ? this.$stage.css({ transform: "translate3d(" + b + "px,0px,0px)", transition: this.speed() / 1e3 + "s" }) : c ? this.$stage.animate({ left: b + "px" }, this.speed(), this.settings.fallbackEasing, a.proxy(this.onTransitionEnd, this)) : this.$stage.css({ left: b + "px" });
    }, e.prototype.is = function (a) {
        return this._states.current[a] && this._states.current[a] > 0;
    }, e.prototype.current = function (a) {
        if (a === d) return this._current;if (0 === this._items.length) return d;if (a = this.normalize(a), this._current !== a) {
            var b = this.trigger("change", { property: { name: "position", value: a } });b.data !== d && (a = this.normalize(b.data)), this._current = a, this.invalidate("position"), this.trigger("changed", { property: { name: "position", value: this._current } });
        }return this._current;
    }, e.prototype.invalidate = function (b) {
        return "string" === a.type(b) && (this._invalidated[b] = !0, this.is("valid") && this.leave("valid")), a.map(this._invalidated, function (a, b) {
            return b;
        });
    }, e.prototype.reset = function (a) {
        a = this.normalize(a), a !== d && (this._speed = 0, this._current = a, this.suppress(["translate", "translated"]), this.animate(this.coordinates(a)), this.release(["translate", "translated"]));
    }, e.prototype.normalize = function (a, b) {
        var c = this._items.length,
            e = b ? 0 : this._clones.length;return !this.isNumeric(a) || c < 1 ? a = d : (a < 0 || a >= c + e) && (a = ((a - e / 2) % c + c) % c + e / 2), a;
    }, e.prototype.relative = function (a) {
        return a -= this._clones.length / 2, this.normalize(a, !0);
    }, e.prototype.maximum = function (a) {
        var b,
            c,
            d,
            e = this.settings,
            f = this._coordinates.length;if (e.loop) f = this._clones.length / 2 + this._items.length - 1;else if (e.autoWidth || e.merge) {
            for (b = this._items.length, c = this._items[--b].width(), d = this.$element.width(); b-- && (c += this._items[b].width() + this.settings.margin, !(c > d));) {}f = b + 1;
        } else f = e.center ? this._items.length - 1 : this._items.length - e.items;return a && (f -= this._clones.length / 2), Math.max(f, 0);
    }, e.prototype.minimum = function (a) {
        return a ? 0 : this._clones.length / 2;
    }, e.prototype.items = function (a) {
        return a === d ? this._items.slice() : (a = this.normalize(a, !0), this._items[a]);
    }, e.prototype.mergers = function (a) {
        return a === d ? this._mergers.slice() : (a = this.normalize(a, !0), this._mergers[a]);
    }, e.prototype.clones = function (b) {
        var c = this._clones.length / 2,
            e = c + this._items.length,
            f = function f(a) {
                return a % 2 === 0 ? e + a / 2 : c - (a + 1) / 2;
            };return b === d ? a.map(this._clones, function (a, b) {
            return f(b);
        }) : a.map(this._clones, function (a, c) {
            return a === b ? f(c) : null;
        });
    }, e.prototype.speed = function (a) {
        return a !== d && (this._speed = a), this._speed;
    }, e.prototype.coordinates = function (b) {
        var c,
            e = 1,
            f = b - 1;return b === d ? a.map(this._coordinates, a.proxy(function (a, b) {
            return this.coordinates(b);
        }, this)) : (this.settings.center ? (this.settings.rtl && (e = -1, f = b + 1), c = this._coordinates[b], c += (this.width() - c + (this._coordinates[f] || 0)) / 2 * e) : c = this._coordinates[f] || 0, c = Math.ceil(c));
    }, e.prototype.duration = function (a, b, c) {
        return 0 === c ? 0 : Math.min(Math.max(Math.abs(b - a), 1), 6) * Math.abs(c || this.settings.smartSpeed);
    }, e.prototype.to = function (a, b) {
        var c = this.current(),
            d = null,
            e = a - this.relative(c),
            f = (e > 0) - (e < 0),
            g = this._items.length,
            h = this.minimum(),
            i = this.maximum();this.settings.loop ? (!this.settings.rewind && Math.abs(e) > g / 2 && (e += f * -1 * g), a = c + e, d = ((a - h) % g + g) % g + h, d !== a && d - e <= i && d - e > 0 && (c = d - e, a = d, this.reset(c))) : this.settings.rewind ? (i += 1, a = (a % i + i) % i) : a = Math.max(h, Math.min(i, a)), this.speed(this.duration(c, a, b)), this.current(a), this.$element.is(":visible") && this.update();
    }, e.prototype.next = function (a) {
        a = a || !1, this.to(this.relative(this.current()) + 1, a);
    }, e.prototype.prev = function (a) {
        a = a || !1, this.to(this.relative(this.current()) - 1, a);
    }, e.prototype.onTransitionEnd = function (a) {
        if (a !== d && (a.stopPropagation(), (a.target || a.srcElement || a.originalTarget) !== this.$stage.get(0))) return !1;this.leave("animating"), this.trigger("translated");
    }, e.prototype.viewport = function () {
        var d;return this.options.responsiveBaseElement !== b ? d = a(this.options.responsiveBaseElement).width() : b.innerWidth ? d = b.innerWidth : c.documentElement && c.documentElement.clientWidth ? d = c.documentElement.clientWidth : console.warn("Can not detect viewport width."), d;
    }, e.prototype.replace = function (b) {
        this.$stage.empty(), this._items = [], b && (b = b instanceof jQuery ? b : a(b)), this.settings.nestedItemSelector && (b = b.find("." + this.settings.nestedItemSelector)), b.filter(function () {
            return 1 === this.nodeType;
        }).each(a.proxy(function (a, b) {
            b = this.prepare(b), this.$stage.append(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1);
        }, this)), this.reset(this.isNumeric(this.settings.startPosition) ? this.settings.startPosition : 0), this.invalidate("items");
    }, e.prototype.add = function (b, c) {
        var e = this.relative(this._current);c = c === d ? this._items.length : this.normalize(c, !0), b = b instanceof jQuery ? b : a(b), this.trigger("add", { content: b, position: c }), b = this.prepare(b), 0 === this._items.length || c === this._items.length ? (0 === this._items.length && this.$stage.append(b), 0 !== this._items.length && this._items[c - 1].after(b), this._items.push(b), this._mergers.push(1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)) : (this._items[c].before(b), this._items.splice(c, 0, b), this._mergers.splice(c, 0, 1 * b.find("[data-merge]").addBack("[data-merge]").attr("data-merge") || 1)), this._items[e] && this.reset(this._items[e].index()), this.invalidate("items"), this.trigger("added", { content: b, position: c });
    }, e.prototype.remove = function (a) {
        a = this.normalize(a, !0), a !== d && (this.trigger("remove", { content: this._items[a], position: a }), this._items[a].remove(), this._items.splice(a, 1), this._mergers.splice(a, 1), this.invalidate("items"), this.trigger("removed", { content: null, position: a }));
    }, e.prototype.preloadAutoWidthImages = function (b) {
        b.each(a.proxy(function (b, c) {
            this.enter("pre-loading"), c = a(c), a(new Image()).one("load", a.proxy(function (a) {
                c.attr("src", a.target.src), c.css("opacity", 1), this.leave("pre-loading"), !this.is("pre-loading") && !this.is("initializing") && this.refresh();
            }, this)).attr("src", c.attr("src") || c.attr("data-src") || c.attr("data-src-retina"));
        }, this));
    }, e.prototype.destroy = function () {
        this.$element.off(".owl.core"), this.$stage.off(".owl.core"), a(c).off(".owl.core"), this.settings.responsive !== !1 && (b.clearTimeout(this.resizeTimer), this.off(b, "resize", this._handlers.onThrottledResize));for (var d in this._plugins) {
            this._plugins[d].destroy();
        }this.$stage.children(".cloned").remove(), this.$stage.unwrap(), this.$stage.children().contents().unwrap(), this.$stage.children().unwrap(), this.$element.removeClass(this.options.refreshClass).removeClass(this.options.loadingClass).removeClass(this.options.loadedClass).removeClass(this.options.rtlClass).removeClass(this.options.dragClass).removeClass(this.options.grabClass).attr("class", this.$element.attr("class").replace(new RegExp(this.options.responsiveClass + "-\\S+\\s", "g"), "")).removeData("owl.carousel");
    }, e.prototype.op = function (a, b, c) {
        var d = this.settings.rtl;switch (b) {case "<":
            return d ? a > c : a < c;case ">":
            return d ? a < c : a > c;case ">=":
            return d ? a <= c : a >= c;case "<=":
            return d ? a >= c : a <= c;}
    }, e.prototype.on = function (a, b, c, d) {
        a.addEventListener ? a.addEventListener(b, c, d) : a.attachEvent && a.attachEvent("on" + b, c);
    }, e.prototype.off = function (a, b, c, d) {
        a.removeEventListener ? a.removeEventListener(b, c, d) : a.detachEvent && a.detachEvent("on" + b, c);
    }, e.prototype.trigger = function (b, c, d, f, g) {
        var h = { item: { count: this._items.length, index: this.current() } },
            i = a.camelCase(a.grep(["on", b, d], function (a) {
                return a;
            }).join("-").toLowerCase()),
            j = a.Event([b, "owl", d || "carousel"].join(".").toLowerCase(), a.extend({ relatedTarget: this }, h, c));return this._supress[b] || (a.each(this._plugins, function (a, b) {
            b.onTrigger && b.onTrigger(j);
        }), this.register({ type: e.Type.Event, name: b }), this.$element.trigger(j), this.settings && "function" == typeof this.settings[i] && this.settings[i].call(this, j)), j;
    }, e.prototype.enter = function (b) {
        a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) {
            this._states.current[b] === d && (this._states.current[b] = 0), this._states.current[b]++;
        }, this));
    }, e.prototype.leave = function (b) {
        a.each([b].concat(this._states.tags[b] || []), a.proxy(function (a, b) {
            this._states.current[b]--;
        }, this));
    }, e.prototype.register = function (b) {
        if (b.type === e.Type.Event) {
            if (a.event.special[b.name] || (a.event.special[b.name] = {}), !a.event.special[b.name].owl) {
                var c = a.event.special[b.name]._default;a.event.special[b.name]._default = function (a) {
                    return !c || !c.apply || a.namespace && a.namespace.indexOf("owl") !== -1 ? a.namespace && a.namespace.indexOf("owl") > -1 : c.apply(this, arguments);
                }, a.event.special[b.name].owl = !0;
            }
        } else b.type === e.Type.State && (this._states.tags[b.name] ? this._states.tags[b.name] = this._states.tags[b.name].concat(b.tags) : this._states.tags[b.name] = b.tags, this._states.tags[b.name] = a.grep(this._states.tags[b.name], a.proxy(function (c, d) {
            return a.inArray(c, this._states.tags[b.name]) === d;
        }, this)));
    }, e.prototype.suppress = function (b) {
        a.each(b, a.proxy(function (a, b) {
            this._supress[b] = !0;
        }, this));
    }, e.prototype.release = function (b) {
        a.each(b, a.proxy(function (a, b) {
            delete this._supress[b];
        }, this));
    }, e.prototype.pointer = function (a) {
        var c = { x: null, y: null };return a = a.originalEvent || a || b.event, a = a.touches && a.touches.length ? a.touches[0] : a.changedTouches && a.changedTouches.length ? a.changedTouches[0] : a, a.pageX ? (c.x = a.pageX, c.y = a.pageY) : (c.x = a.clientX, c.y = a.clientY), c;
    }, e.prototype.isNumeric = function (a) {
        return !isNaN(parseFloat(a));
    }, e.prototype.difference = function (a, b) {
        return { x: a.x - b.x, y: a.y - b.y };
    }, a.fn.owlCarousel = function (b) {
        var c = Array.prototype.slice.call(arguments, 1);return this.each(function () {
            var d = a(this),
                f = d.data("owl.carousel");f || (f = new e(this, "object" == (typeof b === "undefined" ? "undefined" : _typeof(b)) && b), d.data("owl.carousel", f), a.each(["next", "prev", "to", "destroy", "refresh", "replace", "add", "remove"], function (b, c) {
                f.register({ type: e.Type.Event, name: c }), f.$element.on(c + ".owl.carousel.core", a.proxy(function (a) {
                    a.namespace && a.relatedTarget !== this && (this.suppress([c]), f[c].apply(this, [].slice.call(arguments, 1)), this.release([c]));
                }, f));
            })), "string" == typeof b && "_" !== b.charAt(0) && f[b].apply(f, c);
        });
    }, a.fn.owlCarousel.Constructor = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function e(b) {
        this._core = b, this._interval = null, this._visible = null, this._handlers = { "initialized.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.autoRefresh && this.watch();
        }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers);
    };e.Defaults = { autoRefresh: !0, autoRefreshInterval: 500 }, e.prototype.watch = function () {
        this._interval || (this._visible = this._core.$element.is(":visible"), this._interval = b.setInterval(a.proxy(this.refresh, this), this._core.settings.autoRefreshInterval));
    }, e.prototype.refresh = function () {
        this._core.$element.is(":visible") !== this._visible && (this._visible = !this._visible, this._core.$element.toggleClass("owl-hidden", !this._visible), this._visible && this._core.invalidate("width") && this._core.refresh());
    }, e.prototype.destroy = function () {
        var a, c;b.clearInterval(this._interval);for (a in this._handlers) {
            this._core.$element.off(a, this._handlers[a]);
        }for (c in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[c] && (this[c] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.AutoRefresh = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function e(b) {
        this._core = b, this._loaded = [], this._handlers = { "initialized.owl.carousel change.owl.carousel resized.owl.carousel": a.proxy(function (b) {
            if (b.namespace && this._core.settings && this._core.settings.lazyLoad && (b.property && "position" == b.property.name || "initialized" == b.type)) for (var c = this._core.settings, e = c.center && Math.ceil(c.items / 2) || c.items, f = c.center && e * -1 || 0, g = (b.property && b.property.value !== d ? b.property.value : this._core.current()) + f, h = this._core.clones().length, i = a.proxy(function (a, b) {
                this.load(b);
            }, this); f++ < e;) {
                this.load(h / 2 + this._core.relative(g)), h && a.each(this._core.clones(this._core.relative(g)), i), g++;
            }
        }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers);
    };e.Defaults = { lazyLoad: !1 }, e.prototype.load = function (c) {
        var d = this._core.$stage.children().eq(c),
            e = d && d.find(".owl-lazy");!e || a.inArray(d.get(0), this._loaded) > -1 || (e.each(a.proxy(function (c, d) {
            var e,
                f = a(d),
                g = b.devicePixelRatio > 1 && f.attr("data-src-retina") || f.attr("data-src");this._core.trigger("load", { element: f, url: g }, "lazy"), f.is("img") ? f.one("load.owl.lazy", a.proxy(function () {
                f.css("opacity", 1), this._core.trigger("loaded", { element: f, url: g }, "lazy");
            }, this)).attr("src", g) : (e = new Image(), e.onload = a.proxy(function () {
                f.css({ "background-image": 'url("' + g + '")', opacity: "1" }), this._core.trigger("loaded", { element: f, url: g }, "lazy");
            }, this), e.src = g);
        }, this)), this._loaded.push(d.get(0)));
    }, e.prototype.destroy = function () {
        var a, b;for (a in this.handlers) {
            this._core.$element.off(a, this.handlers[a]);
        }for (b in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[b] && (this[b] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.Lazy = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function e(b) {
        this._core = b, this._handlers = { "initialized.owl.carousel refreshed.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.autoHeight && this.update();
        }, this), "changed.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.autoHeight && "position" == a.property.name && this.update();
        }, this), "loaded.owl.lazy": a.proxy(function (a) {
            a.namespace && this._core.settings.autoHeight && a.element.closest("." + this._core.settings.itemClass).index() === this._core.current() && this.update();
        }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers);
    };e.Defaults = { autoHeight: !1, autoHeightClass: "owl-height" }, e.prototype.update = function () {
        var b = this._core._current,
            c = b + this._core.settings.items,
            d = this._core.$stage.children().toArray().slice(b, c),
            e = [],
            f = 0;a.each(d, function (b, c) {
            e.push(a(c).height());
        }), f = Math.max.apply(null, e), this._core.$stage.parent().height(f).addClass(this._core.settings.autoHeightClass);
    }, e.prototype.destroy = function () {
        var a, b;for (a in this._handlers) {
            this._core.$element.off(a, this._handlers[a]);
        }for (b in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[b] && (this[b] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.AutoHeight = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function e(b) {
        this._core = b, this._videos = {}, this._playing = null, this._handlers = { "initialized.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.register({ type: "state", name: "playing", tags: ["interacting"] });
        }, this), "resize.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.video && this.isInFullScreen() && a.preventDefault();
        }, this), "refreshed.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.is("resizing") && this._core.$stage.find(".cloned .owl-video-frame").remove();
        }, this), "changed.owl.carousel": a.proxy(function (a) {
            a.namespace && "position" === a.property.name && this._playing && this.stop();
        }, this), "prepared.owl.carousel": a.proxy(function (b) {
            if (b.namespace) {
                var c = a(b.content).find(".owl-video");c.length && (c.css("display", "none"), this.fetch(c, a(b.content)));
            }
        }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this._core.$element.on(this._handlers), this._core.$element.on("click.owl.video", ".owl-video-play-icon", a.proxy(function (a) {
            this.play(a);
        }, this));
    };e.Defaults = { video: !1, videoHeight: !1, videoWidth: !1 }, e.prototype.fetch = function (a, b) {
        var c = function () {
                return a.attr("data-vimeo-id") ? "vimeo" : a.attr("data-vzaar-id") ? "vzaar" : "youtube";
            }(),
            d = a.attr("data-vimeo-id") || a.attr("data-youtube-id") || a.attr("data-vzaar-id"),
            e = a.attr("data-width") || this._core.settings.videoWidth,
            f = a.attr("data-height") || this._core.settings.videoHeight,
            g = a.attr("href");if (!g) throw new Error("Missing video URL.");if (d = g.match(/(http:|https:|)\/\/(player.|www.|app.)?(vimeo\.com|youtu(be\.com|\.be|be\.googleapis\.com)|vzaar\.com)\/(video\/|videos\/|embed\/|channels\/.+\/|groups\/.+\/|watch\?v=|v\/)?([A-Za-z0-9._%-]*)(\&\S+)?/), d[3].indexOf("youtu") > -1) c = "youtube";else if (d[3].indexOf("vimeo") > -1) c = "vimeo";else {
            if (!(d[3].indexOf("vzaar") > -1)) throw new Error("Video URL not supported.");c = "vzaar";
        }d = d[6], this._videos[g] = { type: c, id: d, width: e, height: f }, b.attr("data-video", g), this.thumbnail(a, this._videos[g]);
    }, e.prototype.thumbnail = function (b, c) {
        var d,
            e,
            f,
            g = c.width && c.height ? 'style="width:' + c.width + "px;height:" + c.height + 'px;"' : "",
            h = b.find("img"),
            i = "src",
            j = "",
            k = this._core.settings,
            l = function l(a) {
                e = '<div class="owl-video-play-icon"></div>', d = k.lazyLoad ? '<div class="owl-video-tn ' + j + '" ' + i + '="' + a + '"></div>' : '<div class="owl-video-tn" style="opacity:1;background-image:url(' + a + ')"></div>', b.after(d), b.after(e);
            };if (b.wrap('<div class="owl-video-wrapper"' + g + "></div>"), this._core.settings.lazyLoad && (i = "data-src", j = "owl-lazy"), h.length) return l(h.attr(i)), h.remove(), !1;"youtube" === c.type ? (f = "//img.youtube.com/vi/" + c.id + "/hqdefault.jpg", l(f)) : "vimeo" === c.type ? a.ajax({ type: "GET", url: "//vimeo.com/api/v2/video/" + c.id + ".json", jsonp: "callback", dataType: "jsonp", success: function success(a) {
            f = a[0].thumbnail_large, l(f);
        } }) : "vzaar" === c.type && a.ajax({ type: "GET", url: "//vzaar.com/api/videos/" + c.id + ".json", jsonp: "callback", dataType: "jsonp", success: function success(a) {
            f = a.framegrab_url, l(f);
        } });
    }, e.prototype.stop = function () {
        this._core.trigger("stop", null, "video"), this._playing.find(".owl-video-frame").remove(), this._playing.removeClass("owl-video-playing"), this._playing = null, this._core.leave("playing"), this._core.trigger("stopped", null, "video");
    }, e.prototype.play = function (b) {
        var c,
            d = a(b.target),
            e = d.closest("." + this._core.settings.itemClass),
            f = this._videos[e.attr("data-video")],
            g = f.width || "100%",
            h = f.height || this._core.$stage.height();this._playing || (this._core.enter("playing"), this._core.trigger("play", null, "video"), e = this._core.items(this._core.relative(e.index())), this._core.reset(e.index()), "youtube" === f.type ? c = '<iframe width="' + g + '" height="' + h + '" src="//www.youtube.com/embed/' + f.id + "?autoplay=1&rel=0&v=" + f.id + '" frameborder="0" allowfullscreen></iframe>' : "vimeo" === f.type ? c = '<iframe src="//player.vimeo.com/video/' + f.id + '?autoplay=1" width="' + g + '" height="' + h + '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>' : "vzaar" === f.type && (c = '<iframe frameborder="0"height="' + h + '"width="' + g + '" allowfullscreen mozallowfullscreen webkitAllowFullScreen src="//view.vzaar.com/' + f.id + '/player?autoplay=true"></iframe>'), a('<div class="owl-video-frame">' + c + "</div>").insertAfter(e.find(".owl-video")), this._playing = e.addClass("owl-video-playing"));
    }, e.prototype.isInFullScreen = function () {
        var b = c.fullscreenElement || c.mozFullScreenElement || c.webkitFullscreenElement;return b && a(b).parent().hasClass("owl-video-frame");
    }, e.prototype.destroy = function () {
        var a, b;this._core.$element.off("click.owl.video");for (a in this._handlers) {
            this._core.$element.off(a, this._handlers[a]);
        }for (b in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[b] && (this[b] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.Video = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function e(b) {
        this.core = b, this.core.options = a.extend({}, e.Defaults, this.core.options), this.swapping = !0, this.previous = d, this.next = d, this.handlers = { "change.owl.carousel": a.proxy(function (a) {
            a.namespace && "position" == a.property.name && (this.previous = this.core.current(), this.next = a.property.value);
        }, this), "drag.owl.carousel dragged.owl.carousel translated.owl.carousel": a.proxy(function (a) {
            a.namespace && (this.swapping = "translated" == a.type);
        }, this), "translate.owl.carousel": a.proxy(function (a) {
            a.namespace && this.swapping && (this.core.options.animateOut || this.core.options.animateIn) && this.swap();
        }, this) }, this.core.$element.on(this.handlers);
    };e.Defaults = { animateOut: !1, animateIn: !1 }, e.prototype.swap = function () {
        if (1 === this.core.settings.items && a.support.animation && a.support.transition) {
            this.core.speed(0);var b,
                c = a.proxy(this.clear, this),
                d = this.core.$stage.children().eq(this.previous),
                e = this.core.$stage.children().eq(this.next),
                f = this.core.settings.animateIn,
                g = this.core.settings.animateOut;this.core.current() !== this.previous && (g && (b = this.core.coordinates(this.previous) - this.core.coordinates(this.next), d.one(a.support.animation.end, c).css({ left: b + "px" }).addClass("animated owl-animated-out").addClass(g)), f && e.one(a.support.animation.end, c).addClass("animated owl-animated-in").addClass(f));
        }
    }, e.prototype.clear = function (b) {
        a(b.target).css({ left: "" }).removeClass("animated owl-animated-out owl-animated-in").removeClass(this.core.settings.animateIn).removeClass(this.core.settings.animateOut), this.core.onTransitionEnd();
    }, e.prototype.destroy = function () {
        var a, b;for (a in this.handlers) {
            this.core.$element.off(a, this.handlers[a]);
        }for (b in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[b] && (this[b] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.Animate = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    var e = function e(b) {
        this._core = b, this._timeout = null, this._paused = !1, this._handlers = { "changed.owl.carousel": a.proxy(function (a) {
            a.namespace && "settings" === a.property.name ? this._core.settings.autoplay ? this.play() : this.stop() : a.namespace && "position" === a.property.name && this._core.settings.autoplay && this._setAutoPlayInterval();
        }, this), "initialized.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.autoplay && this.play();
        }, this), "play.owl.autoplay": a.proxy(function (a, b, c) {
            a.namespace && this.play(b, c);
        }, this), "stop.owl.autoplay": a.proxy(function (a) {
            a.namespace && this.stop();
        }, this), "mouseover.owl.autoplay": a.proxy(function () {
            this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
        }, this), "mouseleave.owl.autoplay": a.proxy(function () {
            this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.play();
        }, this), "touchstart.owl.core": a.proxy(function () {
            this._core.settings.autoplayHoverPause && this._core.is("rotating") && this.pause();
        }, this), "touchend.owl.core": a.proxy(function () {
            this._core.settings.autoplayHoverPause && this.play();
        }, this) }, this._core.$element.on(this._handlers), this._core.options = a.extend({}, e.Defaults, this._core.options);
    };e.Defaults = { autoplay: !1, autoplayTimeout: 5e3, autoplayHoverPause: !1, autoplaySpeed: !1 }, e.prototype.play = function (a, b) {
        this._paused = !1, this._core.is("rotating") || (this._core.enter("rotating"), this._setAutoPlayInterval());
    }, e.prototype._getNextTimeout = function (d, e) {
        return this._timeout && b.clearTimeout(this._timeout), b.setTimeout(a.proxy(function () {
            this._paused || this._core.is("busy") || this._core.is("interacting") || c.hidden || this._core.next(e || this._core.settings.autoplaySpeed);
        }, this), d || this._core.settings.autoplayTimeout);
    }, e.prototype._setAutoPlayInterval = function () {
        this._timeout = this._getNextTimeout();
    }, e.prototype.stop = function () {
        this._core.is("rotating") && (b.clearTimeout(this._timeout), this._core.leave("rotating"));
    }, e.prototype.pause = function () {
        this._core.is("rotating") && (this._paused = !0);
    }, e.prototype.destroy = function () {
        var a, b;this.stop();for (a in this._handlers) {
            this._core.$element.off(a, this._handlers[a]);
        }for (b in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[b] && (this[b] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.autoplay = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    "use strict";
    var e = function e(b) {
        this._core = b, this._initialized = !1, this._pages = [], this._controls = {}, this._templates = [], this.$element = this._core.$element, this._overrides = { next: this._core.next, prev: this._core.prev, to: this._core.to }, this._handlers = { "prepared.owl.carousel": a.proxy(function (b) {
            b.namespace && this._core.settings.dotsData && this._templates.push('<div class="' + this._core.settings.dotClass + '">' + a(b.content).find("[data-dot]").addBack("[data-dot]").attr("data-dot") + "</div>");
        }, this), "added.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 0, this._templates.pop());
        }, this), "remove.owl.carousel": a.proxy(function (a) {
            a.namespace && this._core.settings.dotsData && this._templates.splice(a.position, 1);
        }, this), "changed.owl.carousel": a.proxy(function (a) {
            a.namespace && "position" == a.property.name && this.draw();
        }, this), "initialized.owl.carousel": a.proxy(function (a) {
            a.namespace && !this._initialized && (this._core.trigger("initialize", null, "navigation"), this.initialize(), this.update(), this.draw(), this._initialized = !0, this._core.trigger("initialized", null, "navigation"));
        }, this), "refreshed.owl.carousel": a.proxy(function (a) {
            a.namespace && this._initialized && (this._core.trigger("refresh", null, "navigation"), this.update(), this.draw(), this._core.trigger("refreshed", null, "navigation"));
        }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers);
    };e.Defaults = { nav: !1, navText: ["prev", "next"], navSpeed: !1, navElement: "div", navContainer: !1, navContainerClass: "owl-nav", navClass: ["owl-prev", "owl-next"], slideBy: 1, dotClass: "owl-dot", dotsClass: "owl-dots", dots: !0, dotsEach: !1, dotsData: !1, dotsSpeed: !1, dotsContainer: !1 }, e.prototype.initialize = function () {
        var b,
            c = this._core.settings;this._controls.$relative = (c.navContainer ? a(c.navContainer) : a("<div>").addClass(c.navContainerClass).appendTo(this.$element)).addClass("disabled"), this._controls.$previous = a("<" + c.navElement + ">").addClass(c.navClass[0]).html(c.navText[0]).prependTo(this._controls.$relative).on("click", a.proxy(function (a) {
            this.prev(c.navSpeed);
        }, this)), this._controls.$next = a("<" + c.navElement + ">").addClass(c.navClass[1]).html(c.navText[1]).appendTo(this._controls.$relative).on("click", a.proxy(function (a) {
            this.next(c.navSpeed);
        }, this)), c.dotsData || (this._templates = [a("<div>").addClass(c.dotClass).append(a("<span>")).prop("outerHTML")]), this._controls.$absolute = (c.dotsContainer ? a(c.dotsContainer) : a("<div>").addClass(c.dotsClass).appendTo(this.$element)).addClass("disabled"), this._controls.$absolute.on("click", "div", a.proxy(function (b) {
            var d = a(b.target).parent().is(this._controls.$absolute) ? a(b.target).index() : a(b.target).parent().index();b.preventDefault(), this.to(d, c.dotsSpeed);
        }, this));for (b in this._overrides) {
            this._core[b] = a.proxy(this[b], this);
        }
    }, e.prototype.destroy = function () {
        var a, b, c, d;for (a in this._handlers) {
            this.$element.off(a, this._handlers[a]);
        }for (b in this._controls) {
            this._controls[b].remove();
        }for (d in this.overides) {
            this._core[d] = this._overrides[d];
        }for (c in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[c] && (this[c] = null);
        }
    }, e.prototype.update = function () {
        var a,
            b,
            c,
            d = this._core.clones().length / 2,
            e = d + this._core.items().length,
            f = this._core.maximum(!0),
            g = this._core.settings,
            h = g.center || g.autoWidth || g.dotsData ? 1 : g.dotsEach || g.items;if ("page" !== g.slideBy && (g.slideBy = Math.min(g.slideBy, g.items)), g.dots || "page" == g.slideBy) for (this._pages = [], a = d, b = 0, c = 0; a < e; a++) {
            if (b >= h || 0 === b) {
                if (this._pages.push({ start: Math.min(f, a - d), end: a - d + h - 1 }), Math.min(f, a - d) === f) break;b = 0, ++c;
            }b += this._core.mergers(this._core.relative(a));
        }
    }, e.prototype.draw = function () {
        var b,
            c = this._core.settings,
            d = this._core.items().length <= c.items,
            e = this._core.relative(this._core.current()),
            f = c.loop || c.rewind;this._controls.$relative.toggleClass("disabled", !c.nav || d), c.nav && (this._controls.$previous.toggleClass("disabled", !f && e <= this._core.minimum(!0)), this._controls.$next.toggleClass("disabled", !f && e >= this._core.maximum(!0))), this._controls.$absolute.toggleClass("disabled", !c.dots || d), c.dots && (b = this._pages.length - this._controls.$absolute.children().length, c.dotsData && 0 !== b ? this._controls.$absolute.html(this._templates.join("")) : b > 0 ? this._controls.$absolute.append(new Array(b + 1).join(this._templates[0])) : b < 0 && this._controls.$absolute.children().slice(b).remove(), this._controls.$absolute.find(".active").removeClass("active"), this._controls.$absolute.children().eq(a.inArray(this.current(), this._pages)).addClass("active"));
    }, e.prototype.onTrigger = function (b) {
        var c = this._core.settings;b.page = { index: a.inArray(this.current(), this._pages), count: this._pages.length, size: c && (c.center || c.autoWidth || c.dotsData ? 1 : c.dotsEach || c.items) };
    }, e.prototype.current = function () {
        var b = this._core.relative(this._core.current());return a.grep(this._pages, a.proxy(function (a, c) {
            return a.start <= b && a.end >= b;
        }, this)).pop();
    }, e.prototype.getPosition = function (b) {
        var c,
            d,
            e = this._core.settings;return "page" == e.slideBy ? (c = a.inArray(this.current(), this._pages), d = this._pages.length, b ? ++c : --c, c = this._pages[(c % d + d) % d].start) : (c = this._core.relative(this._core.current()), d = this._core.items().length, b ? c += e.slideBy : c -= e.slideBy), c;
    }, e.prototype.next = function (b) {
        a.proxy(this._overrides.to, this._core)(this.getPosition(!0), b);
    }, e.prototype.prev = function (b) {
        a.proxy(this._overrides.to, this._core)(this.getPosition(!1), b);
    }, e.prototype.to = function (b, c, d) {
        var e;!d && this._pages.length ? (e = this._pages.length, a.proxy(this._overrides.to, this._core)(this._pages[(b % e + e) % e].start, c)) : a.proxy(this._overrides.to, this._core)(b, c);
    }, a.fn.owlCarousel.Constructor.Plugins.Navigation = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    "use strict";
    var e = function e(c) {
        this._core = c, this._hashes = {}, this.$element = this._core.$element, this._handlers = { "initialized.owl.carousel": a.proxy(function (c) {
            c.namespace && "URLHash" === this._core.settings.startPosition && a(b).trigger("hashchange.owl.navigation");
        }, this), "prepared.owl.carousel": a.proxy(function (b) {
            if (b.namespace) {
                var c = a(b.content).find("[data-hash]").addBack("[data-hash]").attr("data-hash");if (!c) return;this._hashes[c] = b.content;
            }
        }, this), "changed.owl.carousel": a.proxy(function (c) {
            if (c.namespace && "position" === c.property.name) {
                var d = this._core.items(this._core.relative(this._core.current())),
                    e = a.map(this._hashes, function (a, b) {
                        return a === d ? b : null;
                    }).join();if (!e || b.location.hash.slice(1) === e) return;b.location.hash = e;
            }
        }, this) }, this._core.options = a.extend({}, e.Defaults, this._core.options), this.$element.on(this._handlers), a(b).on("hashchange.owl.navigation", a.proxy(function (a) {
            var c = b.location.hash.substring(1),
                e = this._core.$stage.children(),
                f = this._hashes[c] && e.index(this._hashes[c]);f !== d && f !== this._core.current() && this._core.to(this._core.relative(f), !1, !0);
        }, this));
    };e.Defaults = { URLhashListener: !1 }, e.prototype.destroy = function () {
        var c, d;a(b).off("hashchange.owl.navigation");for (c in this._handlers) {
            this._core.$element.off(c, this._handlers[c]);
        }for (d in Object.getOwnPropertyNames(this)) {
            "function" != typeof this[d] && (this[d] = null);
        }
    }, a.fn.owlCarousel.Constructor.Plugins.Hash = e;
}(window.Zepto || window.jQuery, window, document), function (a, b, c, d) {
    function e(b, c) {
        var e = !1,
            f = b.charAt(0).toUpperCase() + b.slice(1);return a.each((b + " " + h.join(f + " ") + f).split(" "), function (a, b) {
            if (g[b] !== d) return e = !c || b, !1;
        }), e;
    }function f(a) {
        return e(a, !0);
    }var g = a("<support>").get(0).style,
        h = "Webkit Moz O ms".split(" "),
        i = { transition: { end: { WebkitTransition: "webkitTransitionEnd", MozTransition: "transitionend", OTransition: "oTransitionEnd", transition: "transitionend" } }, animation: { end: { WebkitAnimation: "webkitAnimationEnd", MozAnimation: "animationend", OAnimation: "oAnimationEnd", animation: "animationend" } } },
        j = { csstransforms: function csstransforms() {
            return !!e("transform");
        }, csstransforms3d: function csstransforms3d() {
            return !!e("perspective");
        }, csstransitions: function csstransitions() {
            return !!e("transition");
        }, cssanimations: function cssanimations() {
            return !!e("animation");
        } };j.csstransitions() && (a.support.transition = new String(f("transition")), a.support.transition.end = i.transition.end[a.support.transition]), j.cssanimations() && (a.support.animation = new String(f("animation")), a.support.animation.end = i.animation.end[a.support.animation]), j.csstransforms() && (a.support.transform = new String(f("transform")), a.support.transform3d = j.csstransforms3d());
}(window.Zepto || window.jQuery, window, document);
"use strict";

// /* == malihu jquery custom scrollbar plugin == Version: 3.1.5, License: MIT License (MIT) */ ! function(e) {
//     "function" == typeof define && define.amd ? define(["jquery"], e) : "undefined" != typeof module && module.exports ? module.exports = e : e(jQuery, window, document)
// }(function(e) {
//     ! function(t) {
//         var o = "function" == typeof define && define.amd,
//             a = "undefined" != typeof module && module.exports,
//             n = "https:" == document.location.protocol ? "https:" : "http:",
//             i = "cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js";
//         o || (a ? require("jquery-mousewheel")(e) : e.event.special.mousewheel || e("head").append(decodeURI("%3Cscript src=" + n + "//" + i + "%3E%3C/script%3E"))), t()
//     }(function() {
//         var t, o = "mCustomScrollbar",
//             a = "mCS",
//             n = ".mCustomScrollbar",
//             i = {
//                 setTop: 0,
//                 setLeft: 0,
//                 axis: "y",
//                 scrollbarPosition: "inside",
//                 scrollInertia: 950,
//                 autoDraggerLength: !0,
//                 alwaysShowScrollbar: 0,
//                 snapOffset: 0,
//                 mouseWheel: {
//                     enable: !0,
//                     scrollAmount: "auto",
//                     axis: "y",
//                     deltaFactor: "auto",
//                     disableOver: ["select", "option", "keygen", "datalist", "textarea"]
//                 },
//                 scrollButtons: {
//                     scrollType: "stepless",
//                     scrollAmount: "auto"
//                 },
//                 keyboard: {
//                     enable: !0,
//                     scrollType: "stepless",
//                     scrollAmount: "auto"
//                 },
//                 contentTouchScroll: 25,
//                 documentTouchScroll: !0,
//                 advanced: {
//                     autoScrollOnFocus: "input,textarea,select,button,datalist,keygen,a[tabindex],area,object,[contenteditable='true']",
//                     updateOnContentResize: !0,
//                     updateOnImageLoad: "auto",
//                     autoUpdateTimeout: 60
//                 },
//                 theme: "light",
//                 callbacks: {
//                     onTotalScrollOffset: 0,
//                     onTotalScrollBackOffset: 0,
//                     alwaysTriggerOffsets: !0
//                 }
//             },
//             r = 0,
//             l = {},
//             s = window.attachEvent && !window.addEventListener ? 1 : 0,
//             c = !1,
//             d = ["mCSB_dragger_onDrag", "mCSB_scrollTools_onDrag", "mCS_img_loaded", "mCS_disabled", "mCS_destroyed", "mCS_no_scrollbar", "mCS-autoHide", "mCS-dir-rtl", "mCS_no_scrollbar_y", "mCS_no_scrollbar_x", "mCS_y_hidden", "mCS_x_hidden", "mCSB_draggerContainer", "mCSB_buttonUp", "mCSB_buttonDown", "mCSB_buttonLeft", "mCSB_buttonRight"],
//             u = {
//                 init: function(t) {
//                     var t = e.extend(!0, {}, i, t),
//                         o = f.call(this);
//                     if (t.live) {
//                         var s = t.liveSelector || this.selector || n,
//                             c = e(s);
//                         if ("off" === t.live) return void m(s);
//                         l[s] = setTimeout(function() {
//                             c.mCustomScrollbar(t), "once" === t.live && c.length && m(s)
//                         }, 500)
//                     } else m(s);
//                     return t.setWidth = t.set_width ? t.set_width : t.setWidth, t.setHeight = t.set_height ? t.set_height : t.setHeight, t.axis = t.horizontalScroll ? "x" : p(t.axis), t.scrollInertia = t.scrollInertia > 0 && t.scrollInertia < 17 ? 17 : t.scrollInertia, "object" != typeof t.mouseWheel && 1 == t.mouseWheel && (t.mouseWheel = {
//                         enable: !0,
//                         scrollAmount: "auto",
//                         axis: "y",
//                         preventDefault: !1,
//                         deltaFactor: "auto",
//                         normalizeDelta: !1,
//                         invert: !1
//                     }), t.mouseWheel.scrollAmount = t.mouseWheelPixels ? t.mouseWheelPixels : t.mouseWheel.scrollAmount, t.mouseWheel.normalizeDelta = t.advanced.normalizeMouseWheelDelta ? t.advanced.normalizeMouseWheelDelta : t.mouseWheel.normalizeDelta, t.scrollButtons.scrollType = g(t.scrollButtons.scrollType), h(t), e(o).each(function() {
//                         var o = e(this);
//                         if (!o.data(a)) {
//                             o.data(a, {
//                                 idx: ++r,
//                                 opt: t,
//                                 scrollRatio: {
//                                     y: null,
//                                     x: null
//                                 },
//                                 overflowed: null,
//                                 contentReset: {
//                                     y: null,
//                                     x: null
//                                 },
//                                 bindEvents: !1,
//                                 tweenRunning: !1,
//                                 sequential: {},
//                                 langDir: o.css("direction"),
//                                 cbOffsets: null,
//                                 trigger: null,
//                                 poll: {
//                                     size: {
//                                         o: 0,
//                                         n: 0
//                                     },
//                                     img: {
//                                         o: 0,
//                                         n: 0
//                                     },
//                                     change: {
//                                         o: 0,
//                                         n: 0
//                                     }
//                                 }
//                             });
//                             var n = o.data(a),
//                                 i = n.opt,
//                                 l = o.data("mcs-axis"),
//                                 s = o.data("mcs-scrollbar-position"),
//                                 c = o.data("mcs-theme");
//                             l && (i.axis = l), s && (i.scrollbarPosition = s), c && (i.theme = c, h(i)), v.call(this), n && i.callbacks.onCreate && "function" == typeof i.callbacks.onCreate && i.callbacks.onCreate.call(this), e("#mCSB_" + n.idx + "_container img:not(." + d[2] + ")").addClass(d[2]), u.update.call(null, o)
//                         }
//                     })
//                 },
//                 update: function(t, o) {
//                     var n = t || f.call(this);
//                     return e(n).each(function() {
//                         var t = e(this);
//                         if (t.data(a)) {
//                             var n = t.data(a),
//                                 i = n.opt,
//                                 r = e("#mCSB_" + n.idx + "_container"),
//                                 l = e("#mCSB_" + n.idx),
//                                 s = [e("#mCSB_" + n.idx + "_dragger_vertical"), e("#mCSB_" + n.idx + "_dragger_horizontal")];
//                             if (!r.length) return;
//                             n.tweenRunning && Q(t), o && n && i.callbacks.onBeforeUpdate && "function" == typeof i.callbacks.onBeforeUpdate && i.callbacks.onBeforeUpdate.call(this), t.hasClass(d[3]) && t.removeClass(d[3]), t.hasClass(d[4]) && t.removeClass(d[4]), l.css("max-height", "none"), l.height() !== t.height() && l.css("max-height", t.height()), _.call(this), "y" === i.axis || i.advanced.autoExpandHorizontalScroll || r.css("width", x(r)), n.overflowed = y.call(this), M.call(this), i.autoDraggerLength && S.call(this), b.call(this), T.call(this);
//                             var c = [Math.abs(r[0].offsetTop), Math.abs(r[0].offsetLeft)];
//                             "x" !== i.axis && (n.overflowed[0] ? s[0].height() > s[0].parent().height() ? B.call(this) : (G(t, c[0].toString(), {
//                                 dir: "y",
//                                 dur: 0,
//                                 overwrite: "none"
//                             }), n.contentReset.y = null) : (B.call(this), "y" === i.axis ? k.call(this) : "yx" === i.axis && n.overflowed[1] && G(t, c[1].toString(), {
//                                 dir: "x",
//                                 dur: 0,
//                                 overwrite: "none"
//                             }))), "y" !== i.axis && (n.overflowed[1] ? s[1].width() > s[1].parent().width() ? B.call(this) : (G(t, c[1].toString(), {
//                                 dir: "x",
//                                 dur: 0,
//                                 overwrite: "none"
//                             }), n.contentReset.x = null) : (B.call(this), "x" === i.axis ? k.call(this) : "yx" === i.axis && n.overflowed[0] && G(t, c[0].toString(), {
//                                 dir: "y",
//                                 dur: 0,
//                                 overwrite: "none"
//                             }))), o && n && (2 === o && i.callbacks.onImageLoad && "function" == typeof i.callbacks.onImageLoad ? i.callbacks.onImageLoad.call(this) : 3 === o && i.callbacks.onSelectorChange && "function" == typeof i.callbacks.onSelectorChange ? i.callbacks.onSelectorChange.call(this) : i.callbacks.onUpdate && "function" == typeof i.callbacks.onUpdate && i.callbacks.onUpdate.call(this)), N.call(this)
//                         }
//                     })
//                 },
//                 scrollTo: function(t, o) {
//                     if ("undefined" != typeof t && null != t) {
//                         var n = f.call(this);
//                         return e(n).each(function() {
//                             var n = e(this);
//                             if (n.data(a)) {
//                                 var i = n.data(a),
//                                     r = i.opt,
//                                     l = {
//                                         trigger: "external",
//                                         scrollInertia: r.scrollInertia,
//                                         scrollEasing: "mcsEaseInOut",
//                                         moveDragger: !1,
//                                         timeout: 60,
//                                         callbacks: !0,
//                                         onStart: !0,
//                                         onUpdate: !0,
//                                         onComplete: !0
//                                     },
//                                     s = e.extend(!0, {}, l, o),
//                                     c = Y.call(this, t),
//                                     d = s.scrollInertia > 0 && s.scrollInertia < 17 ? 17 : s.scrollInertia;
//                                 c[0] = X.call(this, c[0], "y"), c[1] = X.call(this, c[1], "x"), s.moveDragger && (c[0] *= i.scrollRatio.y, c[1] *= i.scrollRatio.x), s.dur = ne() ? 0 : d, setTimeout(function() {
//                                     null !== c[0] && "undefined" != typeof c[0] && "x" !== r.axis && i.overflowed[0] && (s.dir = "y", s.overwrite = "all", G(n, c[0].toString(), s)), null !== c[1] && "undefined" != typeof c[1] && "y" !== r.axis && i.overflowed[1] && (s.dir = "x", s.overwrite = "none", G(n, c[1].toString(), s))
//                                 }, s.timeout)
//                             }
//                         })
//                     }
//                 },
//                 stop: function() {
//                     var t = f.call(this);
//                     return e(t).each(function() {
//                         var t = e(this);
//                         t.data(a) && Q(t)
//                     })
//                 },
//                 disable: function(t) {
//                     var o = f.call(this);
//                     return e(o).each(function() {
//                         var o = e(this);
//                         if (o.data(a)) {
//                             o.data(a);
//                             N.call(this, "remove"), k.call(this), t && B.call(this), M.call(this, !0), o.addClass(d[3])
//                         }
//                     })
//                 },
//                 destroy: function() {
//                     var t = f.call(this);
//                     return e(t).each(function() {
//                         var n = e(this);
//                         if (n.data(a)) {
//                             var i = n.data(a),
//                                 r = i.opt,
//                                 l = e("#mCSB_" + i.idx),
//                                 s = e("#mCSB_" + i.idx + "_container"),
//                                 c = e(".mCSB_" + i.idx + "_scrollbar");
//                             r.live && m(r.liveSelector || e(t).selector), N.call(this, "remove"), k.call(this), B.call(this), n.removeData(a), $(this, "mcs"), c.remove(), s.find("img." + d[2]).removeClass(d[2]), l.replaceWith(s.contents()), n.removeClass(o + " _" + a + "_" + i.idx + " " + d[6] + " " + d[7] + " " + d[5] + " " + d[3]).addClass(d[4])
//                         }
//                     })
//                 }
//             },
//             f = function() {
//                 return "object" != typeof e(this) || e(this).length < 1 ? n : this
//             },
//             h = function(t) {
//                 var o = ["rounded", "rounded-dark", "rounded-dots", "rounded-dots-dark"],
//                     a = ["rounded-dots", "rounded-dots-dark", "3d", "3d-dark", "3d-thick", "3d-thick-dark", "inset", "inset-dark", "inset-2", "inset-2-dark", "inset-3", "inset-3-dark"],
//                     n = ["minimal", "minimal-dark"],
//                     i = ["minimal", "minimal-dark"],
//                     r = ["minimal", "minimal-dark"];
//                 t.autoDraggerLength = e.inArray(t.theme, o) > -1 ? !1 : t.autoDraggerLength, t.autoExpandScrollbar = e.inArray(t.theme, a) > -1 ? !1 : t.autoExpandScrollbar, t.scrollButtons.enable = e.inArray(t.theme, n) > -1 ? !1 : t.scrollButtons.enable, t.autoHideScrollbar = e.inArray(t.theme, i) > -1 ? !0 : t.autoHideScrollbar, t.scrollbarPosition = e.inArray(t.theme, r) > -1 ? "outside" : t.scrollbarPosition
//             },
//             m = function(e) {
//                 l[e] && (clearTimeout(l[e]), $(l, e))
//             },
//             p = function(e) {
//                 return "yx" === e || "xy" === e || "auto" === e ? "yx" : "x" === e || "horizontal" === e ? "x" : "y"
//             },
//             g = function(e) {
//                 return "stepped" === e || "pixels" === e || "step" === e || "click" === e ? "stepped" : "stepless"
//             },
//             v = function() {
//                 var t = e(this),
//                     n = t.data(a),
//                     i = n.opt,
//                     r = i.autoExpandScrollbar ? " " + d[1] + "_expand" : "",
//                     l = ["<div id='mCSB_" + n.idx + "_scrollbar_vertical' class='mCSB_scrollTools mCSB_" + n.idx + "_scrollbar mCS-" + i.theme + " mCSB_scrollTools_vertical" + r + "'><div class='" + d[12] + "'><div id='mCSB_" + n.idx + "_dragger_vertical' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>", "<div id='mCSB_" + n.idx + "_scrollbar_horizontal' class='mCSB_scrollTools mCSB_" + n.idx + "_scrollbar mCS-" + i.theme + " mCSB_scrollTools_horizontal" + r + "'><div class='" + d[12] + "'><div id='mCSB_" + n.idx + "_dragger_horizontal' class='mCSB_dragger' style='position:absolute;'><div class='mCSB_dragger_bar' /></div><div class='mCSB_draggerRail' /></div></div>"],
//                     s = "yx" === i.axis ? "mCSB_vertical_horizontal" : "x" === i.axis ? "mCSB_horizontal" : "mCSB_vertical",
//                     c = "yx" === i.axis ? l[0] + l[1] : "x" === i.axis ? l[1] : l[0],
//                     u = "yx" === i.axis ? "<div id='mCSB_" + n.idx + "_container_wrapper' class='mCSB_container_wrapper' />" : "",
//                     f = i.autoHideScrollbar ? " " + d[6] : "",
//                     h = "x" !== i.axis && "rtl" === n.langDir ? " " + d[7] : "";
//                 i.setWidth && t.css("width", i.setWidth), i.setHeight && t.css("height", i.setHeight), i.setLeft = "y" !== i.axis && "rtl" === n.langDir ? "989999px" : i.setLeft, t.addClass(o + " _" + a + "_" + n.idx + f + h).wrapInner("<div id='mCSB_" + n.idx + "' class='mCustomScrollBox mCS-" + i.theme + " " + s + "'><div id='mCSB_" + n.idx + "_container' class='mCSB_container' style='position:relative; top:" + i.setTop + "; left:" + i.setLeft + ";' dir='" + n.langDir + "' /></div>");
//                 var m = e("#mCSB_" + n.idx),
//                     p = e("#mCSB_" + n.idx + "_container");
//                 "y" === i.axis || i.advanced.autoExpandHorizontalScroll || p.css("width", x(p)), "outside" === i.scrollbarPosition ? ("static" === t.css("position") && t.css("position", "relative"), t.css("overflow", "visible"), m.addClass("mCSB_outside").after(c)) : (m.addClass("mCSB_inside").append(c), p.wrap(u)), w.call(this);
//                 var g = [e("#mCSB_" + n.idx + "_dragger_vertical"), e("#mCSB_" + n.idx + "_dragger_horizontal")];
//                 g[0].css("min-height", g[0].height()), g[1].css("min-width", g[1].width())
//             },
//             x = function(t) {
//                 var o = [t[0].scrollWidth, Math.max.apply(Math, t.children().map(function() {
//                         return e(this).outerWidth(!0)
//                     }).get())],
//                     a = t.parent().width();
//                 return o[0] > a ? o[0] : o[1] > a ? o[1] : "100%"
//             },
//             _ = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = o.opt,
//                     i = e("#mCSB_" + o.idx + "_container");
//                 if (n.advanced.autoExpandHorizontalScroll && "y" !== n.axis) {
//                     i.css({
//                         width: "auto",
//                         "min-width": 0,
//                         "overflow-x": "scroll"
//                     });
//                     var r = Math.ceil(i[0].scrollWidth);
//                     3 === n.advanced.autoExpandHorizontalScroll || 2 !== n.advanced.autoExpandHorizontalScroll && r > i.parent().width() ? i.css({
//                         width: r,
//                         "min-width": "100%",
//                         "overflow-x": "inherit"
//                     }) : i.css({
//                         "overflow-x": "inherit",
//                         position: "absolute"
//                     }).wrap("<div class='mCSB_h_wrapper' style='position:relative; left:0; width:999999px;' />").css({
//                         width: Math.ceil(i[0].getBoundingClientRect().right + .4) - Math.floor(i[0].getBoundingClientRect().left),
//                         "min-width": "100%",
//                         position: "relative"
//                     }).unwrap()
//                 }
//             },
//             w = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = o.opt,
//                     i = e(".mCSB_" + o.idx + "_scrollbar:first"),
//                     r = oe(n.scrollButtons.tabindex) ? "tabindex='" + n.scrollButtons.tabindex + "'" : "",
//                     l = ["<a href='#' class='" + d[13] + "' " + r + " />", "<a href='#' class='" + d[14] + "' " + r + " />", "<a href='#' class='" + d[15] + "' " + r + " />", "<a href='#' class='" + d[16] + "' " + r + " />"],
//                     s = ["x" === n.axis ? l[2] : l[0], "x" === n.axis ? l[3] : l[1], l[2], l[3]];
//                 n.scrollButtons.enable && i.prepend(s[0]).append(s[1]).next(".mCSB_scrollTools").prepend(s[2]).append(s[3])
//             },
//             S = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = e("#mCSB_" + o.idx),
//                     i = e("#mCSB_" + o.idx + "_container"),
//                     r = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")],
//                     l = [n.height() / i.outerHeight(!1), n.width() / i.outerWidth(!1)],
//                     c = [parseInt(r[0].css("min-height")), Math.round(l[0] * r[0].parent().height()), parseInt(r[1].css("min-width")), Math.round(l[1] * r[1].parent().width())],
//                     d = s && c[1] < c[0] ? c[0] : c[1],
//                     u = s && c[3] < c[2] ? c[2] : c[3];
//                 r[0].css({
//                     height: d,
//                     "max-height": r[0].parent().height() - 10
//                 }).find(".mCSB_dragger_bar").css({
//                     "line-height": c[0] + "px"
//                 }), r[1].css({
//                     width: u,
//                     "max-width": r[1].parent().width() - 10
//                 })
//             },
//             b = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = e("#mCSB_" + o.idx),
//                     i = e("#mCSB_" + o.idx + "_container"),
//                     r = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")],
//                     l = [i.outerHeight(!1) - n.height(), i.outerWidth(!1) - n.width()],
//                     s = [l[0] / (r[0].parent().height() - r[0].height()), l[1] / (r[1].parent().width() - r[1].width())];
//                 o.scrollRatio = {
//                     y: s[0],
//                     x: s[1]
//                 }
//             },
//             C = function(e, t, o) {
//                 var a = o ? d[0] + "_expanded" : "",
//                     n = e.closest(".mCSB_scrollTools");
//                 "active" === t ? (e.toggleClass(d[0] + " " + a), n.toggleClass(d[1]), e[0]._draggable = e[0]._draggable ? 0 : 1) : e[0]._draggable || ("hide" === t ? (e.removeClass(d[0]), n.removeClass(d[1])) : (e.addClass(d[0]), n.addClass(d[1])))
//             },
//             y = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = e("#mCSB_" + o.idx),
//                     i = e("#mCSB_" + o.idx + "_container"),
//                     r = null == o.overflowed ? i.height() : i.outerHeight(!1),
//                     l = null == o.overflowed ? i.width() : i.outerWidth(!1),
//                     s = i[0].scrollHeight,
//                     c = i[0].scrollWidth;
//                 return s > r && (r = s), c > l && (l = c), [r > n.height(), l > n.width()]
//             },
//             B = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = o.opt,
//                     i = e("#mCSB_" + o.idx),
//                     r = e("#mCSB_" + o.idx + "_container"),
//                     l = [e("#mCSB_" + o.idx + "_dragger_vertical"), e("#mCSB_" + o.idx + "_dragger_horizontal")];
//                 if (Q(t), ("x" !== n.axis && !o.overflowed[0] || "y" === n.axis && o.overflowed[0]) && (l[0].add(r).css("top", 0), G(t, "_resetY")), "y" !== n.axis && !o.overflowed[1] || "x" === n.axis && o.overflowed[1]) {
//                     var s = dx = 0;
//                     "rtl" === o.langDir && (s = i.width() - r.outerWidth(!1), dx = Math.abs(s / o.scrollRatio.x)), r.css("left", s), l[1].css("left", dx), G(t, "_resetX")
//                 }
//             },
//             T = function() {
//                 function t() {
//                     r = setTimeout(function() {
//                         e.event.special.mousewheel ? (clearTimeout(r), W.call(o[0])) : t()
//                     }, 100)
//                 }
//                 var o = e(this),
//                     n = o.data(a),
//                     i = n.opt;
//                 if (!n.bindEvents) {
//                     if (I.call(this), i.contentTouchScroll && D.call(this), E.call(this), i.mouseWheel.enable) {
//                         var r;
//                         t()
//                     }
//                     P.call(this), U.call(this), i.advanced.autoScrollOnFocus && H.call(this), i.scrollButtons.enable && F.call(this), i.keyboard.enable && q.call(this), n.bindEvents = !0
//                 }
//             },
//             k = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = o.opt,
//                     i = a + "_" + o.idx,
//                     r = ".mCSB_" + o.idx + "_scrollbar",
//                     l = e("#mCSB_" + o.idx + ",#mCSB_" + o.idx + "_container,#mCSB_" + o.idx + "_container_wrapper," + r + " ." + d[12] + ",#mCSB_" + o.idx + "_dragger_vertical,#mCSB_" + o.idx + "_dragger_horizontal," + r + ">a"),
//                     s = e("#mCSB_" + o.idx + "_container");
//                 n.advanced.releaseDraggableSelectors && l.add(e(n.advanced.releaseDraggableSelectors)), n.advanced.extraDraggableSelectors && l.add(e(n.advanced.extraDraggableSelectors)), o.bindEvents && (e(document).add(e(!A() || top.document)).unbind("." + i), l.each(function() {
//                     e(this).unbind("." + i)
//                 }), clearTimeout(t[0]._focusTimeout), $(t[0], "_focusTimeout"), clearTimeout(o.sequential.step), $(o.sequential, "step"), clearTimeout(s[0].onCompleteTimeout), $(s[0], "onCompleteTimeout"), o.bindEvents = !1)
//             },
//             M = function(t) {
//                 var o = e(this),
//                     n = o.data(a),
//                     i = n.opt,
//                     r = e("#mCSB_" + n.idx + "_container_wrapper"),
//                     l = r.length ? r : e("#mCSB_" + n.idx + "_container"),
//                     s = [e("#mCSB_" + n.idx + "_scrollbar_vertical"), e("#mCSB_" + n.idx + "_scrollbar_horizontal")],
//                     c = [s[0].find(".mCSB_dragger"), s[1].find(".mCSB_dragger")];
//                 "x" !== i.axis && (n.overflowed[0] && !t ? (s[0].add(c[0]).add(s[0].children("a")).css("display", "block"), l.removeClass(d[8] + " " + d[10])) : (i.alwaysShowScrollbar ? (2 !== i.alwaysShowScrollbar && c[0].css("display", "none"), l.removeClass(d[10])) : (s[0].css("display", "none"), l.addClass(d[10])), l.addClass(d[8]))), "y" !== i.axis && (n.overflowed[1] && !t ? (s[1].add(c[1]).add(s[1].children("a")).css("display", "block"), l.removeClass(d[9] + " " + d[11])) : (i.alwaysShowScrollbar ? (2 !== i.alwaysShowScrollbar && c[1].css("display", "none"), l.removeClass(d[11])) : (s[1].css("display", "none"), l.addClass(d[11])), l.addClass(d[9]))), n.overflowed[0] || n.overflowed[1] ? o.removeClass(d[5]) : o.addClass(d[5])
//             },
//             O = function(t) {
//                 var o = t.type,
//                     a = t.target.ownerDocument !== document && null !== frameElement ? [e(frameElement).offset().top, e(frameElement).offset().left] : null,
//                     n = A() && t.target.ownerDocument !== top.document && null !== frameElement ? [e(t.view.frameElement).offset().top, e(t.view.frameElement).offset().left] : [0, 0];
//                 switch (o) {
//                     case "pointerdown":
//                     case "MSPointerDown":
//                     case "pointermove":
//                     case "MSPointerMove":
//                     case "pointerup":
//                     case "MSPointerUp":
//                         return a ? [t.originalEvent.pageY - a[0] + n[0], t.originalEvent.pageX - a[1] + n[1], !1] : [t.originalEvent.pageY, t.originalEvent.pageX, !1];
//                     case "touchstart":
//                     case "touchmove":
//                     case "touchend":
//                         var i = t.originalEvent.touches[0] || t.originalEvent.changedTouches[0],
//                             r = t.originalEvent.touches.length || t.originalEvent.changedTouches.length;
//                         return t.target.ownerDocument !== document ? [i.screenY, i.screenX, r > 1] : [i.pageY, i.pageX, r > 1];
//                     default:
//                         return a ? [t.pageY - a[0] + n[0], t.pageX - a[1] + n[1], !1] : [t.pageY, t.pageX, !1]
//                 }
//             },
//             I = function() {
//                 function t(e, t, a, n) {
//                     if (h[0].idleTimer = d.scrollInertia < 233 ? 250 : 0, o.attr("id") === f[1]) var i = "x",
//                         s = (o[0].offsetLeft - t + n) * l.scrollRatio.x;
//                     else var i = "y",
//                         s = (o[0].offsetTop - e + a) * l.scrollRatio.y;
//                     G(r, s.toString(), {
//                         dir: i,
//                         drag: !0
//                     })
//                 }
//                 var o, n, i, r = e(this),
//                     l = r.data(a),
//                     d = l.opt,
//                     u = a + "_" + l.idx,
//                     f = ["mCSB_" + l.idx + "_dragger_vertical", "mCSB_" + l.idx + "_dragger_horizontal"],
//                     h = e("#mCSB_" + l.idx + "_container"),
//                     m = e("#" + f[0] + ",#" + f[1]),
//                     p = d.advanced.releaseDraggableSelectors ? m.add(e(d.advanced.releaseDraggableSelectors)) : m,
//                     g = d.advanced.extraDraggableSelectors ? e(!A() || top.document).add(e(d.advanced.extraDraggableSelectors)) : e(!A() || top.document);
//                 m.bind("contextmenu." + u, function(e) {
//                     e.preventDefault()
//                 }).bind("mousedown." + u + " touchstart." + u + " pointerdown." + u + " MSPointerDown." + u, function(t) {
//                     if (t.stopImmediatePropagation(), t.preventDefault(), ee(t)) {
//                         c = !0, s && (document.onselectstart = function() {
//                             return !1
//                         }), L.call(h, !1), Q(r), o = e(this);
//                         var a = o.offset(),
//                             l = O(t)[0] - a.top,
//                             u = O(t)[1] - a.left,
//                             f = o.height() + a.top,
//                             m = o.width() + a.left;
//                         f > l && l > 0 && m > u && u > 0 && (n = l, i = u), C(o, "active", d.autoExpandScrollbar)
//                     }
//                 }).bind("touchmove." + u, function(e) {
//                     e.stopImmediatePropagation(), e.preventDefault();
//                     var a = o.offset(),
//                         r = O(e)[0] - a.top,
//                         l = O(e)[1] - a.left;
//                     t(n, i, r, l)
//                 }), e(document).add(g).bind("mousemove." + u + " pointermove." + u + " MSPointerMove." + u, function(e) {
//                     if (o) {
//                         var a = o.offset(),
//                             r = O(e)[0] - a.top,
//                             l = O(e)[1] - a.left;
//                         if (n === r && i === l) return;
//                         t(n, i, r, l)
//                     }
//                 }).add(p).bind("mouseup." + u + " touchend." + u + " pointerup." + u + " MSPointerUp." + u, function() {
//                     o && (C(o, "active", d.autoExpandScrollbar), o = null), c = !1, s && (document.onselectstart = null), L.call(h, !0)
//                 })
//             },
//             D = function() {
//                 function o(e) {
//                     if (!te(e) || c || O(e)[2]) return void(t = 0);
//                     t = 1, b = 0, C = 0, d = 1, y.removeClass("mCS_touch_action");
//                     var o = I.offset();
//                     u = O(e)[0] - o.top, f = O(e)[1] - o.left, z = [O(e)[0], O(e)[1]]
//                 }
//
//                 function n(e) {
//                     if (te(e) && !c && !O(e)[2] && (T.documentTouchScroll || e.preventDefault(), e.stopImmediatePropagation(), (!C || b) && d)) {
//                         g = K();
//                         var t = M.offset(),
//                             o = O(e)[0] - t.top,
//                             a = O(e)[1] - t.left,
//                             n = "mcsLinearOut";
//                         if (E.push(o), W.push(a), z[2] = Math.abs(O(e)[0] - z[0]), z[3] = Math.abs(O(e)[1] - z[1]), B.overflowed[0]) var i = D[0].parent().height() - D[0].height(),
//                             r = u - o > 0 && o - u > -(i * B.scrollRatio.y) && (2 * z[3] < z[2] || "yx" === T.axis);
//                         if (B.overflowed[1]) var l = D[1].parent().width() - D[1].width(),
//                             h = f - a > 0 && a - f > -(l * B.scrollRatio.x) && (2 * z[2] < z[3] || "yx" === T.axis);
//                         r || h ? (U || e.preventDefault(), b = 1) : (C = 1, y.addClass("mCS_touch_action")), U && e.preventDefault(), w = "yx" === T.axis ? [u - o, f - a] : "x" === T.axis ? [null, f - a] : [u - o, null], I[0].idleTimer = 250, B.overflowed[0] && s(w[0], R, n, "y", "all", !0), B.overflowed[1] && s(w[1], R, n, "x", L, !0)
//                     }
//                 }
//
//                 function i(e) {
//                     if (!te(e) || c || O(e)[2]) return void(t = 0);
//                     t = 1, e.stopImmediatePropagation(), Q(y), p = K();
//                     var o = M.offset();
//                     h = O(e)[0] - o.top, m = O(e)[1] - o.left, E = [], W = []
//                 }
//
//                 function r(e) {
//                     if (te(e) && !c && !O(e)[2]) {
//                         d = 0, e.stopImmediatePropagation(), b = 0, C = 0, v = K();
//                         var t = M.offset(),
//                             o = O(e)[0] - t.top,
//                             a = O(e)[1] - t.left;
//                         if (!(v - g > 30)) {
//                             _ = 1e3 / (v - p);
//                             var n = "mcsEaseOut",
//                                 i = 2.5 > _,
//                                 r = i ? [E[E.length - 2], W[W.length - 2]] : [0, 0];
//                             x = i ? [o - r[0], a - r[1]] : [o - h, a - m];
//                             var u = [Math.abs(x[0]), Math.abs(x[1])];
//                             _ = i ? [Math.abs(x[0] / 4), Math.abs(x[1] / 4)] : [_, _];
//                             var f = [Math.abs(I[0].offsetTop) - x[0] * l(u[0] / _[0], _[0]), Math.abs(I[0].offsetLeft) - x[1] * l(u[1] / _[1], _[1])];
//                             w = "yx" === T.axis ? [f[0], f[1]] : "x" === T.axis ? [null, f[1]] : [f[0], null], S = [4 * u[0] + T.scrollInertia, 4 * u[1] + T.scrollInertia];
//                             var y = parseInt(T.contentTouchScroll) || 0;
//                             w[0] = u[0] > y ? w[0] : 0, w[1] = u[1] > y ? w[1] : 0, B.overflowed[0] && s(w[0], S[0], n, "y", L, !1), B.overflowed[1] && s(w[1], S[1], n, "x", L, !1)
//                         }
//                     }
//                 }
//
//                 function l(e, t) {
//                     var o = [1.5 * t, 2 * t, t / 1.5, t / 2];
//                     return e > 90 ? t > 4 ? o[0] : o[3] : e > 60 ? t > 3 ? o[3] : o[2] : e > 30 ? t > 8 ? o[1] : t > 6 ? o[0] : t > 4 ? t : o[2] : t > 8 ? t : o[3]
//                 }
//
//                 function s(e, t, o, a, n, i) {
//                     e && G(y, e.toString(), {
//                         dur: t,
//                         scrollEasing: o,
//                         dir: a,
//                         overwrite: n,
//                         drag: i
//                     })
//                 }
//                 var d, u, f, h, m, p, g, v, x, _, w, S, b, C, y = e(this),
//                     B = y.data(a),
//                     T = B.opt,
//                     k = a + "_" + B.idx,
//                     M = e("#mCSB_" + B.idx),
//                     I = e("#mCSB_" + B.idx + "_container"),
//                     D = [e("#mCSB_" + B.idx + "_dragger_vertical"), e("#mCSB_" + B.idx + "_dragger_horizontal")],
//                     E = [],
//                     W = [],
//                     R = 0,
//                     L = "yx" === T.axis ? "none" : "all",
//                     z = [],
//                     P = I.find("iframe"),
//                     H = ["touchstart." + k + " pointerdown." + k + " MSPointerDown." + k, "touchmove." + k + " pointermove." + k + " MSPointerMove." + k, "touchend." + k + " pointerup." + k + " MSPointerUp." + k],
//                     U = void 0 !== document.body.style.touchAction && "" !== document.body.style.touchAction;
//                 I.bind(H[0], function(e) {
//                     o(e)
//                 }).bind(H[1], function(e) {
//                     n(e)
//                 }), M.bind(H[0], function(e) {
//                     i(e)
//                 }).bind(H[2], function(e) {
//                     r(e)
//                 }), P.length && P.each(function() {
//                     e(this).bind("load", function() {
//                         A(this) && e(this.contentDocument || this.contentWindow.document).bind(H[0], function(e) {
//                             o(e), i(e)
//                         }).bind(H[1], function(e) {
//                             n(e)
//                         }).bind(H[2], function(e) {
//                             r(e)
//                         })
//                     })
//                 })
//             },
//             E = function() {
//                 function o() {
//                     return window.getSelection ? window.getSelection().toString() : document.selection && "Control" != document.selection.type ? document.selection.createRange().text : 0
//                 }
//
//                 function n(e, t, o) {
//                     d.type = o && i ? "stepped" : "stepless", d.scrollAmount = 10, j(r, e, t, "mcsLinearOut", o ? 60 : null)
//                 }
//                 var i, r = e(this),
//                     l = r.data(a),
//                     s = l.opt,
//                     d = l.sequential,
//                     u = a + "_" + l.idx,
//                     f = e("#mCSB_" + l.idx + "_container"),
//                     h = f.parent();
//                 f.bind("mousedown." + u, function() {
//                     t || i || (i = 1, c = !0)
//                 }).add(document).bind("mousemove." + u, function(e) {
//                     if (!t && i && o()) {
//                         var a = f.offset(),
//                             r = O(e)[0] - a.top + f[0].offsetTop,
//                             c = O(e)[1] - a.left + f[0].offsetLeft;
//                         r > 0 && r < h.height() && c > 0 && c < h.width() ? d.step && n("off", null, "stepped") : ("x" !== s.axis && l.overflowed[0] && (0 > r ? n("on", 38) : r > h.height() && n("on", 40)), "y" !== s.axis && l.overflowed[1] && (0 > c ? n("on", 37) : c > h.width() && n("on", 39)))
//                     }
//                 }).bind("mouseup." + u + " dragend." + u, function() {
//                     t || (i && (i = 0, n("off", null)), c = !1)
//                 })
//             },
//             W = function() {
//                 function t(t, a) {
//                     if (Q(o), !z(o, t.target)) {
//                         var r = "auto" !== i.mouseWheel.deltaFactor ? parseInt(i.mouseWheel.deltaFactor) : s && t.deltaFactor < 100 ? 100 : t.deltaFactor || 100,
//                             d = i.scrollInertia;
//                         if ("x" === i.axis || "x" === i.mouseWheel.axis) var u = "x",
//                             f = [Math.round(r * n.scrollRatio.x), parseInt(i.mouseWheel.scrollAmount)],
//                             h = "auto" !== i.mouseWheel.scrollAmount ? f[1] : f[0] >= l.width() ? .9 * l.width() : f[0],
//                             m = Math.abs(e("#mCSB_" + n.idx + "_container")[0].offsetLeft),
//                             p = c[1][0].offsetLeft,
//                             g = c[1].parent().width() - c[1].width(),
//                             v = "y" === i.mouseWheel.axis ? t.deltaY || a : t.deltaX;
//                         else var u = "y",
//                             f = [Math.round(r * n.scrollRatio.y), parseInt(i.mouseWheel.scrollAmount)],
//                             h = "auto" !== i.mouseWheel.scrollAmount ? f[1] : f[0] >= l.height() ? .9 * l.height() : f[0],
//                             m = Math.abs(e("#mCSB_" + n.idx + "_container")[0].offsetTop),
//                             p = c[0][0].offsetTop,
//                             g = c[0].parent().height() - c[0].height(),
//                             v = t.deltaY || a;
//                         "y" === u && !n.overflowed[0] || "x" === u && !n.overflowed[1] || ((i.mouseWheel.invert || t.webkitDirectionInvertedFromDevice) && (v = -v), i.mouseWheel.normalizeDelta && (v = 0 > v ? -1 : 1), (v > 0 && 0 !== p || 0 > v && p !== g || i.mouseWheel.preventDefault) && (t.stopImmediatePropagation(), t.preventDefault()), t.deltaFactor < 5 && !i.mouseWheel.normalizeDelta && (h = t.deltaFactor, d = 17), G(o, (m - v * h).toString(), {
//                             dir: u,
//                             dur: d
//                         }))
//                     }
//                 }
//                 if (e(this).data(a)) {
//                     var o = e(this),
//                         n = o.data(a),
//                         i = n.opt,
//                         r = a + "_" + n.idx,
//                         l = e("#mCSB_" + n.idx),
//                         c = [e("#mCSB_" + n.idx + "_dragger_vertical"), e("#mCSB_" + n.idx + "_dragger_horizontal")],
//                         d = e("#mCSB_" + n.idx + "_container").find("iframe");
//                     d.length && d.each(function() {
//                         e(this).bind("load", function() {
//                             A(this) && e(this.contentDocument || this.contentWindow.document).bind("mousewheel." + r, function(e, o) {
//                                 t(e, o)
//                             })
//                         })
//                     }), l.bind("mousewheel." + r, function(e, o) {
//                         t(e, o)
//                     })
//                 }
//             },
//             R = new Object,
//             A = function(t) {
//                 var o = !1,
//                     a = !1,
//                     n = null;
//                 if (void 0 === t ? a = "#empty" : void 0 !== e(t).attr("id") && (a = e(t).attr("id")), a !== !1 && void 0 !== R[a]) return R[a];
//                 if (t) {
//                     try {
//                         var i = t.contentDocument || t.contentWindow.document;
//                         n = i.body.innerHTML
//                     } catch (r) {}
//                     o = null !== n
//                 } else {
//                     try {
//                         var i = top.document;
//                         n = i.body.innerHTML
//                     } catch (r) {}
//                     o = null !== n
//                 }
//                 return a !== !1 && (R[a] = o), o
//             },
//             L = function(e) {
//                 var t = this.find("iframe");
//                 if (t.length) {
//                     var o = e ? "auto" : "none";
//                     t.css("pointer-events", o)
//                 }
//             },
//             z = function(t, o) {
//                 var n = o.nodeName.toLowerCase(),
//                     i = t.data(a).opt.mouseWheel.disableOver,
//                     r = ["select", "textarea"];
//                 return e.inArray(n, i) > -1 && !(e.inArray(n, r) > -1 && !e(o).is(":focus"))
//             },
//             P = function() {
//                 var t, o = e(this),
//                     n = o.data(a),
//                     i = a + "_" + n.idx,
//                     r = e("#mCSB_" + n.idx + "_container"),
//                     l = r.parent(),
//                     s = e(".mCSB_" + n.idx + "_scrollbar ." + d[12]);
//                 s.bind("mousedown." + i + " touchstart." + i + " pointerdown." + i + " MSPointerDown." + i, function(o) {
//                     c = !0, e(o.target).hasClass("mCSB_dragger") || (t = 1)
//                 }).bind("touchend." + i + " pointerup." + i + " MSPointerUp." + i, function() {
//                     c = !1
//                 }).bind("click." + i, function(a) {
//                     if (t && (t = 0, e(a.target).hasClass(d[12]) || e(a.target).hasClass("mCSB_draggerRail"))) {
//                         Q(o);
//                         var i = e(this),
//                             s = i.find(".mCSB_dragger");
//                         if (i.parent(".mCSB_scrollTools_horizontal").length > 0) {
//                             if (!n.overflowed[1]) return;
//                             var c = "x",
//                                 u = a.pageX > s.offset().left ? -1 : 1,
//                                 f = Math.abs(r[0].offsetLeft) - u * (.9 * l.width())
//                         } else {
//                             if (!n.overflowed[0]) return;
//                             var c = "y",
//                                 u = a.pageY > s.offset().top ? -1 : 1,
//                                 f = Math.abs(r[0].offsetTop) - u * (.9 * l.height())
//                         }
//                         G(o, f.toString(), {
//                             dir: c,
//                             scrollEasing: "mcsEaseInOut"
//                         })
//                     }
//                 })
//             },
//             H = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = o.opt,
//                     i = a + "_" + o.idx,
//                     r = e("#mCSB_" + o.idx + "_container"),
//                     l = r.parent();
//                 r.bind("focusin." + i, function() {
//                     var o = e(document.activeElement),
//                         a = r.find(".mCustomScrollBox").length,
//                         i = 0;
//                     o.is(n.advanced.autoScrollOnFocus) && (Q(t), clearTimeout(t[0]._focusTimeout), t[0]._focusTimer = a ? (i + 17) * a : 0, t[0]._focusTimeout = setTimeout(function() {
//                         var e = [ae(o)[0], ae(o)[1]],
//                             a = [r[0].offsetTop, r[0].offsetLeft],
//                             s = [a[0] + e[0] >= 0 && a[0] + e[0] < l.height() - o.outerHeight(!1), a[1] + e[1] >= 0 && a[0] + e[1] < l.width() - o.outerWidth(!1)],
//                             c = "yx" !== n.axis || s[0] || s[1] ? "all" : "none";
//                         "x" === n.axis || s[0] || G(t, e[0].toString(), {
//                             dir: "y",
//                             scrollEasing: "mcsEaseInOut",
//                             overwrite: c,
//                             dur: i
//                         }), "y" === n.axis || s[1] || G(t, e[1].toString(), {
//                             dir: "x",
//                             scrollEasing: "mcsEaseInOut",
//                             overwrite: c,
//                             dur: i
//                         })
//                     }, t[0]._focusTimer))
//                 })
//             },
//             U = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = a + "_" + o.idx,
//                     i = e("#mCSB_" + o.idx + "_container").parent();
//                 i.bind("scroll." + n, function() {
//                     0 === i.scrollTop() && 0 === i.scrollLeft() || e(".mCSB_" + o.idx + "_scrollbar").css("visibility", "hidden")
//                 })
//             },
//             F = function() {
//                 var t = e(this),
//                     o = t.data(a),
//                     n = o.opt,
//                     i = o.sequential,
//                     r = a + "_" + o.idx,
//                     l = ".mCSB_" + o.idx + "_scrollbar",
//                     s = e(l + ">a");
//                 s.bind("contextmenu." + r, function(e) {
//                     e.preventDefault()
//                 }).bind("mousedown." + r + " touchstart." + r + " pointerdown." + r + " MSPointerDown." + r + " mouseup." + r + " touchend." + r + " pointerup." + r + " MSPointerUp." + r + " mouseout." + r + " pointerout." + r + " MSPointerOut." + r + " click." + r, function(a) {
//                     function r(e, o) {
//                         i.scrollAmount = n.scrollButtons.scrollAmount, j(t, e, o)
//                     }
//                     if (a.preventDefault(), ee(a)) {
//                         var l = e(this).attr("class");
//                         switch (i.type = n.scrollButtons.scrollType, a.type) {
//                             case "mousedown":
//                             case "touchstart":
//                             case "pointerdown":
//                             case "MSPointerDown":
//                                 if ("stepped" === i.type) return;
//                                 c = !0, o.tweenRunning = !1, r("on", l);
//                                 break;
//                             case "mouseup":
//                             case "touchend":
//                             case "pointerup":
//                             case "MSPointerUp":
//                             case "mouseout":
//                             case "pointerout":
//                             case "MSPointerOut":
//                                 if ("stepped" === i.type) return;
//                                 c = !1, i.dir && r("off", l);
//                                 break;
//                             case "click":
//                                 if ("stepped" !== i.type || o.tweenRunning) return;
//                                 r("on", l)
//                         }
//                     }
//                 })
//             },
//             q = function() {
//                 function t(t) {
//                     function a(e, t) {
//                         r.type = i.keyboard.scrollType, r.scrollAmount = i.keyboard.scrollAmount, "stepped" === r.type && n.tweenRunning || j(o, e, t)
//                     }
//                     switch (t.type) {
//                         case "blur":
//                             n.tweenRunning && r.dir && a("off", null);
//                             break;
//                         case "keydown":
//                         case "keyup":
//                             var l = t.keyCode ? t.keyCode : t.which,
//                                 s = "on";
//                             if ("x" !== i.axis && (38 === l || 40 === l) || "y" !== i.axis && (37 === l || 39 === l)) {
//                                 if ((38 === l || 40 === l) && !n.overflowed[0] || (37 === l || 39 === l) && !n.overflowed[1]) return;
//                                 "keyup" === t.type && (s = "off"), e(document.activeElement).is(u) || (t.preventDefault(), t.stopImmediatePropagation(), a(s, l))
//                             } else if (33 === l || 34 === l) {
//                                 if ((n.overflowed[0] || n.overflowed[1]) && (t.preventDefault(), t.stopImmediatePropagation()), "keyup" === t.type) {
//                                     Q(o);
//                                     var f = 34 === l ? -1 : 1;
//                                     if ("x" === i.axis || "yx" === i.axis && n.overflowed[1] && !n.overflowed[0]) var h = "x",
//                                         m = Math.abs(c[0].offsetLeft) - f * (.9 * d.width());
//                                     else var h = "y",
//                                         m = Math.abs(c[0].offsetTop) - f * (.9 * d.height());
//                                     G(o, m.toString(), {
//                                         dir: h,
//                                         scrollEasing: "mcsEaseInOut"
//                                     })
//                                 }
//                             } else if ((35 === l || 36 === l) && !e(document.activeElement).is(u) && ((n.overflowed[0] || n.overflowed[1]) && (t.preventDefault(), t.stopImmediatePropagation()), "keyup" === t.type)) {
//                                 if ("x" === i.axis || "yx" === i.axis && n.overflowed[1] && !n.overflowed[0]) var h = "x",
//                                     m = 35 === l ? Math.abs(d.width() - c.outerWidth(!1)) : 0;
//                                 else var h = "y",
//                                     m = 35 === l ? Math.abs(d.height() - c.outerHeight(!1)) : 0;
//                                 G(o, m.toString(), {
//                                     dir: h,
//                                     scrollEasing: "mcsEaseInOut"
//                                 })
//                             }
//                     }
//                 }
//                 var o = e(this),
//                     n = o.data(a),
//                     i = n.opt,
//                     r = n.sequential,
//                     l = a + "_" + n.idx,
//                     s = e("#mCSB_" + n.idx),
//                     c = e("#mCSB_" + n.idx + "_container"),
//                     d = c.parent(),
//                     u = "input,textarea,select,datalist,keygen,[contenteditable='true']",
//                     f = c.find("iframe"),
//                     h = ["blur." + l + " keydown." + l + " keyup." + l];
//                 f.length && f.each(function() {
//                     e(this).bind("load", function() {
//                         A(this) && e(this.contentDocument || this.contentWindow.document).bind(h[0], function(e) {
//                             t(e)
//                         })
//                     })
//                 }), s.attr("tabindex", "0").bind(h[0], function(e) {
//                     t(e)
//                 })
//             },
//             j = function(t, o, n, i, r) {
//                 function l(e) {
//                     u.snapAmount && (f.scrollAmount = u.snapAmount instanceof Array ? "x" === f.dir[0] ? u.snapAmount[1] : u.snapAmount[0] : u.snapAmount);
//                     var o = "stepped" !== f.type,
//                         a = r ? r : e ? o ? p / 1.5 : g : 1e3 / 60,
//                         n = e ? o ? 7.5 : 40 : 2.5,
//                         s = [Math.abs(h[0].offsetTop), Math.abs(h[0].offsetLeft)],
//                         d = [c.scrollRatio.y > 10 ? 10 : c.scrollRatio.y, c.scrollRatio.x > 10 ? 10 : c.scrollRatio.x],
//                         m = "x" === f.dir[0] ? s[1] + f.dir[1] * (d[1] * n) : s[0] + f.dir[1] * (d[0] * n),
//                         v = "x" === f.dir[0] ? s[1] + f.dir[1] * parseInt(f.scrollAmount) : s[0] + f.dir[1] * parseInt(f.scrollAmount),
//                         x = "auto" !== f.scrollAmount ? v : m,
//                         _ = i ? i : e ? o ? "mcsLinearOut" : "mcsEaseInOut" : "mcsLinear",
//                         w = !!e;
//                     return e && 17 > a && (x = "x" === f.dir[0] ? s[1] : s[0]), G(t, x.toString(), {
//                         dir: f.dir[0],
//                         scrollEasing: _,
//                         dur: a,
//                         onComplete: w
//                     }), e ? void(f.dir = !1) : (clearTimeout(f.step), void(f.step = setTimeout(function() {
//                         l()
//                     }, a)))
//                 }
//
//                 function s() {
//                     clearTimeout(f.step), $(f, "step"), Q(t)
//                 }
//                 var c = t.data(a),
//                     u = c.opt,
//                     f = c.sequential,
//                     h = e("#mCSB_" + c.idx + "_container"),
//                     m = "stepped" === f.type,
//                     p = u.scrollInertia < 26 ? 26 : u.scrollInertia,
//                     g = u.scrollInertia < 1 ? 17 : u.scrollInertia;
//                 switch (o) {
//                     case "on":
//                         if (f.dir = [n === d[16] || n === d[15] || 39 === n || 37 === n ? "x" : "y", n === d[13] || n === d[15] || 38 === n || 37 === n ? -1 : 1], Q(t), oe(n) && "stepped" === f.type) return;
//                         l(m);
//                         break;
//                     case "off":
//                         s(), (m || c.tweenRunning && f.dir) && l(!0)
//                 }
//             },
//             Y = function(t) {
//                 var o = e(this).data(a).opt,
//                     n = [];
//                 return "function" == typeof t && (t = t()), t instanceof Array ? n = t.length > 1 ? [t[0], t[1]] : "x" === o.axis ? [null, t[0]] : [t[0], null] : (n[0] = t.y ? t.y : t.x || "x" === o.axis ? null : t, n[1] = t.x ? t.x : t.y || "y" === o.axis ? null : t), "function" == typeof n[0] && (n[0] = n[0]()), "function" == typeof n[1] && (n[1] = n[1]()), n
//             },
//             X = function(t, o) {
//                 if (null != t && "undefined" != typeof t) {
//                     var n = e(this),
//                         i = n.data(a),
//                         r = i.opt,
//                         l = e("#mCSB_" + i.idx + "_container"),
//                         s = l.parent(),
//                         c = typeof t;
//                     o || (o = "x" === r.axis ? "x" : "y");
//                     var d = "x" === o ? l.outerWidth(!1) - s.width() : l.outerHeight(!1) - s.height(),
//                         f = "x" === o ? l[0].offsetLeft : l[0].offsetTop,
//                         h = "x" === o ? "left" : "top";
//                     switch (c) {
//                         case "function":
//                             return t();
//                         case "object":
//                             var m = t.jquery ? t : e(t);
//                             if (!m.length) return;
//                             return "x" === o ? ae(m)[1] : ae(m)[0];
//                         case "string":
//                         case "number":
//                             if (oe(t)) return Math.abs(t);
//                             if (-1 !== t.indexOf("%")) return Math.abs(d * parseInt(t) / 100);
//                             if (-1 !== t.indexOf("-=")) return Math.abs(f - parseInt(t.split("-=")[1]));
//                             if (-1 !== t.indexOf("+=")) {
//                                 var p = f + parseInt(t.split("+=")[1]);
//                                 return p >= 0 ? 0 : Math.abs(p)
//                             }
//                             if (-1 !== t.indexOf("px") && oe(t.split("px")[0])) return Math.abs(t.split("px")[0]);
//                             if ("top" === t || "left" === t) return 0;
//                             if ("bottom" === t) return Math.abs(s.height() - l.outerHeight(!1));
//                             if ("right" === t) return Math.abs(s.width() - l.outerWidth(!1));
//                             if ("first" === t || "last" === t) {
//                                 var m = l.find(":" + t);
//                                 return "x" === o ? ae(m)[1] : ae(m)[0]
//                             }
//                             return e(t).length ? "x" === o ? ae(e(t))[1] : ae(e(t))[0] : (l.css(h, t), void u.update.call(null, n[0]))
//                     }
//                 }
//             },
//             N = function(t) {
//                 function o() {
//                     return clearTimeout(f[0].autoUpdate), 0 === l.parents("html").length ? void(l = null) : void(f[0].autoUpdate = setTimeout(function() {
//                         return c.advanced.updateOnSelectorChange && (s.poll.change.n = i(), s.poll.change.n !== s.poll.change.o) ? (s.poll.change.o = s.poll.change.n, void r(3)) : c.advanced.updateOnContentResize && (s.poll.size.n = l[0].scrollHeight + l[0].scrollWidth + f[0].offsetHeight + l[0].offsetHeight + l[0].offsetWidth, s.poll.size.n !== s.poll.size.o) ? (s.poll.size.o = s.poll.size.n, void r(1)) : !c.advanced.updateOnImageLoad || "auto" === c.advanced.updateOnImageLoad && "y" === c.axis || (s.poll.img.n = f.find("img").length, s.poll.img.n === s.poll.img.o) ? void((c.advanced.updateOnSelectorChange || c.advanced.updateOnContentResize || c.advanced.updateOnImageLoad) && o()) : (s.poll.img.o = s.poll.img.n, void f.find("img").each(function() {
//                             n(this)
//                         }))
//                     }, c.advanced.autoUpdateTimeout))
//                 }
//
//                 function n(t) {
//                     function o(e, t) {
//                         return function() {
//                             return t.apply(e, arguments)
//                         }
//                     }
//
//                     function a() {
//                         this.onload = null, e(t).addClass(d[2]), r(2)
//                     }
//                     if (e(t).hasClass(d[2])) return void r();
//                     var n = new Image;
//                     n.onload = o(n, a), n.src = t.src
//                 }
//
//                 function i() {
//                     c.advanced.updateOnSelectorChange === !0 && (c.advanced.updateOnSelectorChange = "*");
//                     var e = 0,
//                         t = f.find(c.advanced.updateOnSelectorChange);
//                     return c.advanced.updateOnSelectorChange && t.length > 0 && t.each(function() {
//                         e += this.offsetHeight + this.offsetWidth
//                     }), e
//                 }
//
//                 function r(e) {
//                     clearTimeout(f[0].autoUpdate), u.update.call(null, l[0], e)
//                 }
//                 var l = e(this),
//                     s = l.data(a),
//                     c = s.opt,
//                     f = e("#mCSB_" + s.idx + "_container");
//                 return t ? (clearTimeout(f[0].autoUpdate), void $(f[0], "autoUpdate")) : void o()
//             },
//             V = function(e, t, o) {
//                 return Math.round(e / t) * t - o
//             },
//             Q = function(t) {
//                 var o = t.data(a),
//                     n = e("#mCSB_" + o.idx + "_container,#mCSB_" + o.idx + "_container_wrapper,#mCSB_" + o.idx + "_dragger_vertical,#mCSB_" + o.idx + "_dragger_horizontal");
//                 n.each(function() {
//                     Z.call(this)
//                 })
//             },
//             G = function(t, o, n) {
//                 function i(e) {
//                     return s && c.callbacks[e] && "function" == typeof c.callbacks[e]
//                 }
//
//                 function r() {
//                     return [c.callbacks.alwaysTriggerOffsets || w >= S[0] + y, c.callbacks.alwaysTriggerOffsets || -B >= w]
//                 }
//
//                 function l() {
//                     var e = [h[0].offsetTop, h[0].offsetLeft],
//                         o = [x[0].offsetTop, x[0].offsetLeft],
//                         a = [h.outerHeight(!1), h.outerWidth(!1)],
//                         i = [f.height(), f.width()];
//                     t[0].mcs = {
//                         content: h,
//                         top: e[0],
//                         left: e[1],
//                         draggerTop: o[0],
//                         draggerLeft: o[1],
//                         topPct: Math.round(100 * Math.abs(e[0]) / (Math.abs(a[0]) - i[0])),
//                         leftPct: Math.round(100 * Math.abs(e[1]) / (Math.abs(a[1]) - i[1])),
//                         direction: n.dir
//                     }
//                 }
//                 var s = t.data(a),
//                     c = s.opt,
//                     d = {
//                         trigger: "internal",
//                         dir: "y",
//                         scrollEasing: "mcsEaseOut",
//                         drag: !1,
//                         dur: c.scrollInertia,
//                         overwrite: "all",
//                         callbacks: !0,
//                         onStart: !0,
//                         onUpdate: !0,
//                         onComplete: !0
//                     },
//                     n = e.extend(d, n),
//                     u = [n.dur, n.drag ? 0 : n.dur],
//                     f = e("#mCSB_" + s.idx),
//                     h = e("#mCSB_" + s.idx + "_container"),
//                     m = h.parent(),
//                     p = c.callbacks.onTotalScrollOffset ? Y.call(t, c.callbacks.onTotalScrollOffset) : [0, 0],
//                     g = c.callbacks.onTotalScrollBackOffset ? Y.call(t, c.callbacks.onTotalScrollBackOffset) : [0, 0];
//                 if (s.trigger = n.trigger, 0 === m.scrollTop() && 0 === m.scrollLeft() || (e(".mCSB_" + s.idx + "_scrollbar").css("visibility", "visible"), m.scrollTop(0).scrollLeft(0)), "_resetY" !== o || s.contentReset.y || (i("onOverflowYNone") && c.callbacks.onOverflowYNone.call(t[0]), s.contentReset.y = 1), "_resetX" !== o || s.contentReset.x || (i("onOverflowXNone") && c.callbacks.onOverflowXNone.call(t[0]), s.contentReset.x = 1), "_resetY" !== o && "_resetX" !== o) {
//                     if (!s.contentReset.y && t[0].mcs || !s.overflowed[0] || (i("onOverflowY") && c.callbacks.onOverflowY.call(t[0]), s.contentReset.x = null), !s.contentReset.x && t[0].mcs || !s.overflowed[1] || (i("onOverflowX") && c.callbacks.onOverflowX.call(t[0]), s.contentReset.x = null), c.snapAmount) {
//                         var v = c.snapAmount instanceof Array ? "x" === n.dir ? c.snapAmount[1] : c.snapAmount[0] : c.snapAmount;
//                         o = V(o, v, c.snapOffset)
//                     }
//                     switch (n.dir) {
//                         case "x":
//                             var x = e("#mCSB_" + s.idx + "_dragger_horizontal"),
//                                 _ = "left",
//                                 w = h[0].offsetLeft,
//                                 S = [f.width() - h.outerWidth(!1), x.parent().width() - x.width()],
//                                 b = [o, 0 === o ? 0 : o / s.scrollRatio.x],
//                                 y = p[1],
//                                 B = g[1],
//                                 T = y > 0 ? y / s.scrollRatio.x : 0,
//                                 k = B > 0 ? B / s.scrollRatio.x : 0;
//                             break;
//                         case "y":
//                             var x = e("#mCSB_" + s.idx + "_dragger_vertical"),
//                                 _ = "top",
//                                 w = h[0].offsetTop,
//                                 S = [f.height() - h.outerHeight(!1), x.parent().height() - x.height()],
//                                 b = [o, 0 === o ? 0 : o / s.scrollRatio.y],
//                                 y = p[0],
//                                 B = g[0],
//                                 T = y > 0 ? y / s.scrollRatio.y : 0,
//                                 k = B > 0 ? B / s.scrollRatio.y : 0
//                     }
//                     b[1] < 0 || 0 === b[0] && 0 === b[1] ? b = [0, 0] : b[1] >= S[1] ? b = [S[0], S[1]] : b[0] = -b[0], t[0].mcs || (l(), i("onInit") && c.callbacks.onInit.call(t[0])), clearTimeout(h[0].onCompleteTimeout), J(x[0], _, Math.round(b[1]), u[1], n.scrollEasing), !s.tweenRunning && (0 === w && b[0] >= 0 || w === S[0] && b[0] <= S[0]) || J(h[0], _, Math.round(b[0]), u[0], n.scrollEasing, n.overwrite, {
//                         onStart: function() {
//                             n.callbacks && n.onStart && !s.tweenRunning && (i("onScrollStart") && (l(), c.callbacks.onScrollStart.call(t[0])), s.tweenRunning = !0, C(x), s.cbOffsets = r())
//                         },
//                         onUpdate: function() {
//                             n.callbacks && n.onUpdate && i("whileScrolling") && (l(), c.callbacks.whileScrolling.call(t[0]))
//                         },
//                         onComplete: function() {
//                             if (n.callbacks && n.onComplete) {
//                                 "yx" === c.axis && clearTimeout(h[0].onCompleteTimeout);
//                                 var e = h[0].idleTimer || 0;
//                                 h[0].onCompleteTimeout = setTimeout(function() {
//                                     i("onScroll") && (l(), c.callbacks.onScroll.call(t[0])), i("onTotalScroll") && b[1] >= S[1] - T && s.cbOffsets[0] && (l(), c.callbacks.onTotalScroll.call(t[0])), i("onTotalScrollBack") && b[1] <= k && s.cbOffsets[1] && (l(), c.callbacks.onTotalScrollBack.call(t[0])), s.tweenRunning = !1, h[0].idleTimer = 0, C(x, "hide")
//                                 }, e)
//                             }
//                         }
//                     })
//                 }
//             },
//             J = function(e, t, o, a, n, i, r) {
//                 function l() {
//                     S.stop || (x || m.call(), x = K() - v, s(), x >= S.time && (S.time = x > S.time ? x + f - (x - S.time) : x + f - 1, S.time < x + 1 && (S.time = x + 1)), S.time < a ? S.id = h(l) : g.call())
//                 }
//
//                 function s() {
//                     a > 0 ? (S.currVal = u(S.time, _, b, a, n), w[t] = Math.round(S.currVal) + "px") : w[t] = o + "px", p.call()
//                 }
//
//                 function c() {
//                     f = 1e3 / 60, S.time = x + f, h = window.requestAnimationFrame ? window.requestAnimationFrame : function(e) {
//                         return s(), setTimeout(e, .01)
//                     }, S.id = h(l)
//                 }
//
//                 function d() {
//                     null != S.id && (window.requestAnimationFrame ? window.cancelAnimationFrame(S.id) : clearTimeout(S.id), S.id = null)
//                 }
//
//                 function u(e, t, o, a, n) {
//                     switch (n) {
//                         case "linear":
//                         case "mcsLinear":
//                             return o * e / a + t;
//                         case "mcsLinearOut":
//                             return e /= a, e--, o * Math.sqrt(1 - e * e) + t;
//                         case "easeInOutSmooth":
//                             return e /= a / 2, 1 > e ? o / 2 * e * e + t : (e--, -o / 2 * (e * (e - 2) - 1) + t);
//                         case "easeInOutStrong":
//                             return e /= a / 2, 1 > e ? o / 2 * Math.pow(2, 10 * (e - 1)) + t : (e--, o / 2 * (-Math.pow(2, -10 * e) + 2) + t);
//                         case "easeInOut":
//                         case "mcsEaseInOut":
//                             return e /= a / 2, 1 > e ? o / 2 * e * e * e + t : (e -= 2, o / 2 * (e * e * e + 2) + t);
//                         case "easeOutSmooth":
//                             return e /= a, e--, -o * (e * e * e * e - 1) + t;
//                         case "easeOutStrong":
//                             return o * (-Math.pow(2, -10 * e / a) + 1) + t;
//                         case "easeOut":
//                         case "mcsEaseOut":
//                         default:
//                             var i = (e /= a) * e,
//                                 r = i * e;
//                             return t + o * (.499999999999997 * r * i + -2.5 * i * i + 5.5 * r + -6.5 * i + 4 * e)
//                     }
//                 }
//                 e._mTween || (e._mTween = {
//                     top: {},
//                     left: {}
//                 });
//                 var f, h, r = r || {},
//                     m = r.onStart || function() {},
//                     p = r.onUpdate || function() {},
//                     g = r.onComplete || function() {},
//                     v = K(),
//                     x = 0,
//                     _ = e.offsetTop,
//                     w = e.style,
//                     S = e._mTween[t];
//                 "left" === t && (_ = e.offsetLeft);
//                 var b = o - _;
//                 S.stop = 0, "none" !== i && d(), c()
//             },
//             K = function() {
//                 return window.performance && window.performance.now ? window.performance.now() : window.performance && window.performance.webkitNow ? window.performance.webkitNow() : Date.now ? Date.now() : (new Date).getTime()
//             },
//             Z = function() {
//                 var e = this;
//                 e._mTween || (e._mTween = {
//                     top: {},
//                     left: {}
//                 });
//                 for (var t = ["top", "left"], o = 0; o < t.length; o++) {
//                     var a = t[o];
//                     e._mTween[a].id && (window.requestAnimationFrame ? window.cancelAnimationFrame(e._mTween[a].id) : clearTimeout(e._mTween[a].id), e._mTween[a].id = null, e._mTween[a].stop = 1)
//                 }
//             },
//             $ = function(e, t) {
//                 try {
//                     delete e[t]
//                 } catch (o) {
//                     e[t] = null
//                 }
//             },
//             ee = function(e) {
//                 return !(e.which && 1 !== e.which)
//             },
//             te = function(e) {
//                 var t = e.originalEvent.pointerType;
//                 return !(t && "touch" !== t && 2 !== t)
//             },
//             oe = function(e) {
//                 return !isNaN(parseFloat(e)) && isFinite(e)
//             },
//             ae = function(e) {
//                 var t = e.parents(".mCSB_container");
//                 return [e.offset().top - t.offset().top, e.offset().left - t.offset().left]
//             },
//             ne = function() {
//                 function e() {
//                     var e = ["webkit", "moz", "ms", "o"];
//                     if ("hidden" in document) return "hidden";
//                     for (var t = 0; t < e.length; t++)
//                         if (e[t] + "Hidden" in document) return e[t] + "Hidden";
//                     return null
//                 }
//                 var t = e();
//                 return t ? document[t] : !1
//             };
//         e.fn[o] = function(t) {
//             return u[t] ? u[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist") : u.init.apply(this, arguments)
//         }, e[o] = function(t) {
//             return u[t] ? u[t].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof t && t ? void e.error("Method " + t + " does not exist") : u.init.apply(this, arguments)
//         }, e[o].defaults = i, window[o] = !0, e(window).bind("load", function() {
//             e(n)[o](), e.extend(e.expr[":"], {
//                 mcsInView: e.expr[":"].mcsInView || function(t) {
//                     var o, a, n = e(t),
//                         i = n.parents(".mCSB_container");
//                     if (i.length) return o = i.parent(), a = [i[0].offsetTop, i[0].offsetLeft], a[0] + ae(n)[0] >= 0 && a[0] + ae(n)[0] < o.height() - n.outerHeight(!1) && a[1] + ae(n)[1] >= 0 && a[1] + ae(n)[1] < o.width() - n.outerWidth(!1)
//                 },
//                 mcsInSight: e.expr[":"].mcsInSight || function(t, o, a) {
//                     var n, i, r, l, s = e(t),
//                         c = s.parents(".mCSB_container"),
//                         d = "exact" === a[3] ? [
//                             [1, 0],
//                             [1, 0]
//                         ] : [
//                             [.9, .1],
//                             [.6, .4]
//                         ];
//                     if (c.length) return n = [s.outerHeight(!1), s.outerWidth(!1)], r = [c[0].offsetTop + ae(s)[0], c[0].offsetLeft + ae(s)[1]], i = [c.parent()[0].offsetHeight, c.parent()[0].offsetWidth], l = [n[0] < i[0] ? d[0] : d[1], n[1] < i[1] ? d[0] : d[1]], r[0] - i[0] * l[0][0] < 0 && r[0] + n[0] - i[0] * l[0][1] >= 0 && r[1] - i[1] * l[1][0] < 0 && r[1] + n[1] - i[1] * l[1][1] >= 0
//                 },
//                 mcsOverflow: e.expr[":"].mcsOverflow || function(t) {
//                     var o = e(t).data(a);
//                     if (o) return o.overflowed[0] || o.overflowed[1]
//                 }
//             })
//         })
//     })
// });




