$(function () {
    validateForm();
    sendCallbackYet();
});

/*валидация формы*/
function validateForm() {
  $("form[name='callback_form']").validate({
        rules: {
            phone: "required"
        },
        messages: {
            phone: "Оставьте телефон для связи"
        },
        submitHandler: function submitHandler(form) {
            sendFormCallback();
        }
    });

}

function sendFormCallback() {
    var data = $('.c-callback-form').serialize();
    $.ajax({
        url: '/ajax/print_book/send.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $('.c-callback-form').hide();
            $('.c-callback-success').show();
            sendCallbackYet();
        },
        error: function (data) {
            /*это оставить только для success*/
            $('.c-callback-form').hide();
            $('.c-callback-success').show();
            sendCallbackYet();
        }
    })
}
function sendCallbackYet() {
    $('body').on('click', '.c-send-yet', function () {
        $('.c-callback-form').show();
        $('.c-callback-success').hide();
        $('.c-callback-form input[name="phone"]').val('');
    })
}