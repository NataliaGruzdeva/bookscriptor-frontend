$(function () {
    validateServiceForm();
    sendServiceFormAgain();
});
/*валидация формы*/
function validateServiceForm() {
    $("form[name='callback_form']").each(function(){
        var form = $(this),
            $formSuccess = form.siblings('.c-callback-success');
        form.validate({
            rules: {
                phone: "required"
            },
            messages: {
                phone: "Оставьте телефон для связи"
            },
            submitHandler: function submitHandler(form) {
                sendServiceFormCallback($(form), $formSuccess);
            }
        });
    })
}

function sendServiceFormCallback($form, $formSuccess) {
    var data = $form.serialize();
    $.ajax({
        url: '/ajax/print_book/send.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $form.hide();
            $formSuccess.show();
        },
        error: function (data) {
            /*это оставить только для success*/
            $form.hide();
            $formSuccess.show();
        }
    })
}
function sendServiceFormAgain() {
    $('body').on('click', '.c-send-yet', function () {
        var $formSuccess = $(this).closest('.c-callback-success'),
            $form = $formSuccess.siblings('.c-callback-form');
        
        $form.show();
        $formSuccess.hide();
        $form.find('input[name="phone"]').val('');
    })
}