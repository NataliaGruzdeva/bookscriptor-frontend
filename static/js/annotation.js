var download_file,
    data,
    mainHeight;

$(function () {
    changeSteps();
    validateForm();
    validateFormBrif();
    textareaAction();
    textareaAutosize();

});
/*валидация формы*/
function textareaAutosize() {
    $('textarea').each(function(){
        autosize(this);
    }).on('autosize:resized', function(){
        mainHeight = $('[data-type="form"]').height() + 241;
        calcHeight(mainHeight);
    }).on('change', function(){
        mainHeight = $('[data-type="form"]').height() + 241;
        calcHeight(mainHeight);
    }).on('cut', function(){
        mainHeight = $('[data-type="form"]').height() + 241;
        calcHeight(mainHeight);
    }).on('paste', function(){
        mainHeight = $('[data-type="form"]').height() + 241;
        calcHeight(mainHeight);
    });
}
function validateFormBrif() {
    $('form[name="annotation"]').each(function(){
        var form = $(this),
            $formSuccess = form.siblings('.c-callback-success');
        form.validate({
            rules: {

                bio: "required",
                shortbook: "required",
                links: "required",
                wishes: "required"
            },
            messages: {
                file: "Загрузите фото",
                bio: "Заполните поле",
                shortbook: "Заполните поле",
                links: "Заполните поле",
                wishes: "Заполните поле"
            },
            submitHandler: function submitHandler(form) {
                sendForm($(form), $formSuccess);
            }
        });
    })
}

function calcHeight() {
    var coverHeight = $('[data-type="cover"]').height();
    $('[data-type="cover"]').css({'min-height': mainHeight});
    var middleHeight = coverHeight - (261 + 460);
    $('[data-cover="middle"]').css({'min-height': middleHeight});
}

/*валидация формы*/
function validateForm() {
    var form = $("form[name='callback_form']");
    validatorPlaneta = $("form[name='callback_form']").validate({
        rules: {
            phone: "required"
        },
        messages: {
            phone: "Оставьте телефон для связи"
        },
        submitHandler: function submitHandler(form) {
            sendFormCallback();
        }
    });

}

function sendFormCallback() {
    var data = $('.c-callback-form').serialize();
    $.ajax({
        url: '/ajax/print_book/send.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $('.c-callback-form').hide();
            $('.c-callback-success').show();

            sendCallbackYet();
        },
        error: function (data) {
            /*это оставить только для success*/
            $('.c-callback-form').hide();
            $('.c-callback-success').show();

            sendCallbackYet();
        }
    })
}
function sendCallbackYet() {
    $('body').on('click', '.c-send-yet', function () {
        $('[data-type="form"]').show();
        $('[data-type="form"] textarea').val('');
        $('textarea').each(function(){
            autosize.destroy(this);
        });
        $('[data-type="success"]').hide();
        textareaAutosize();
    })
}
function sendForm() {
    var data = new FormData();
    data.append('book', download_file);
    data.append('bio', $('textarea[name="bio"]').val());
    data.append('shortbook', $('textarea[name="shortbook"]').val());
    data.append('links', $('textarea[name="links"]').val());
    data.append('wishes', $('textarea[name="wishes"]').val());

    // getFormData(data);
    // getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {
            $('[data-type="form"]').hide();
            $('[data-type="success"]').show();

        },
        error: function (data) {
            $('[data-type="form"]').hide();
            $('[data-type="success"]').show();
            $('[data-type="success"]').css({'min-height': '899px'});
            $('[data-type="cover"]').css({'min-height': '913px'});
            $('.c-link-anchor[href="#form"]').click();
            sendCallbackYet();

        }
    })
}
 function textareaAction() {
     $('body').on('focusin', '[data-type="textarea"]', function (e) {
         e.preventDefault();
         $('[data-type="textarea"]').removeClass('big-height');
         $(this).addClass('big-height');

     })
     $('body').on('focusout', '[data-type="textarea"]', function (e) {
         e.preventDefault();
         $('[data-type="alternate"]').hide();
         $('[data-type="textarea"]').removeClass('big-height');
     })
 }
function changeSteps() {
    if($('.dropzone-photo').length) {
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone(".dropzone-photo", {
            maxFiles: 1,
            url: "/ajax/print_book/dropzone_upload.php",
            paramName: "image",
            acceptedFiles: '.jpg, .jpeg, .png',
            dictInvalidFileType: 'Неподдерживаемый формат файла',
            addRemoveLinks: true,
            dictRemoveFile: 'Удалить',
            init: function() {
                var $this = this,
                    isChanged = false;

                $this.on("addedfile", function(file, done) {
                    $('.dropzone-steps').removeClass('preload');
                    $('.c-preloader').hide();

                    if (isChanged) {
                        var newFile = file;
                        isChanged = false;
                        $this.removeAllFiles(true);
                        $this.emit("addedfile", newFile);
                        //$this.emit("thumbnail", newFile, newFile.url);
                        $this.files.push(newFile);
                    }

                });

                $this.on("removedfile", function(file, done) {
                    $('.dropzone-steps').removeClass('preload');
                    $('.c-preloader').hide();
                });

                $this.on("totaluploadprogress", function(file, progress, bytesSent) {
                    $('.dropzone-steps').addClass('preload');
                    $('.c-preloader').show();
                });

                $('body').on('click', '.c-change-dropzone', function () {
                    isChanged = true;
                    $this.removeAllFiles(true);

                    $('#upload').trigger('click');
                });
            },
            accept: function(file, done) {
                if(!$('.c-append-form').length) {
                    $('.dz-details').append('<div class="append-form c-append-form"><a href="javascript:void(0);" class="change-dropzone c-change-dropzone">Изменить файл</a></div>');
                }
                download_file = file;
            }

        });
    }
}

