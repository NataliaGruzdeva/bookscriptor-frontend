/*-------------------------------------------------------------------------------
Google map
-------------------------------------------------------------------------------*/


$(document).ready(function(){
    (function(){
    var map;

    // Широта и долгоота
    var latitude = 59.940239;
    var longitude = 30.300851;

    map = new GMaps({
      el: '.js-gmap',
      lat: latitude,
      lng:longitude,
      scrollwheel:false,
      zoom: 17,
      zoomControl : true,
      panControl : false,
      streetViewControl : false,
      mapTypeControl: false,
      overviewMapControl: false,
      clickable: false
    });

    map.addMarker({
      lat: latitude,
      lng: longitude,
      animation: google.maps.Animation.DROP,
      verticalAlign: 'bottom',
      horizontalAlign: 'center',
      backgroundColor: '#d3cfcf',
      infoWindow:{
        content: '<div class="map-info"></div>'
      }
    });
    $(window).resize(function(){
      google.maps.event.trigger(map, "resize");
    });
  }());

  });