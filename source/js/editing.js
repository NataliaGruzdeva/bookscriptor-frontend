$(function () {
    proofReaderTabs();
    changeSteps();
    editReaderTabs();
    validateForm();
    sendCallbackYet();
});
function proofReaderTabs() {
    $('body').on('click', '.c-tab', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');

        $('.c-tab-block').hide().removeClass('active');
        $(showBlock).fadeIn('100').addClass('active');
    });
}
function editReaderTabs() {
    $('body').on('click', '.c-tab-download', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');

        $('.c-tab-content').hide().removeClass('active');
        $(showBlock).fadeIn('100').addClass('active');
    });
}
function changeSteps() {
    if($('.dropzone-steps').length) {
        Dropzone.autoDiscover = false;

        var myDropzone = new Dropzone(".dropzone-steps", {
            maxFiles: 1,
            url: "/ajax/print_book/dropzone_upload.php",
            paramName: "image",
            acceptedFiles: '.doc, .docx',
            init: function() {
                var $this = this;

                $('.c-cancel-dropzone').on('click', function(){
                    $('.dropzone-steps').removeClass('dropzone-steps-upload');
                    $('.c-steps').removeClass('active');
                    $('.c-step-1').addClass('active');
                    $this.removeAllFiles(true);
                });

                $('.c-change-dropzone').on('click', function(){
                    $this.removeAllFiles();
                });
            },
            accept: function(file, done) {
                $('.dropzone-steps').addClass('dropzone-steps-upload');
                $('.c-steps').removeClass('active');
                $('.c-step-2').addClass('active');
                download_file = file;
                //console.log(download_file);
            },
            error: function(file) {
                $('.dropzone-steps').addClass('dropzone-steps-upload');
                $('.c-steps').removeClass('active');
                $('.c-step-1').addClass('active');
                $('.c-step-2').removeClass('active');
                $('[data-dz-errormessage]').html('Неподдерживаемый формат файла');
                download_file = file;
                //console.log(download_file);
            }

        });
    }
}
/*валидация формы*/
function validateForm() {
    var form = $("form[name='callback_form']");
    validatorPlaneta = $("form[name='callback_form']").validate({
        rules: {
            phone: "required"
        },
        messages: {
            phone: "Оставьте телефон для связи"
        },
        submitHandler: function submitHandler(form) {
            sendFormCallback();
        }
    });

}

function sendFormCallback() {
    var data = $('.c-callback-form').serialize();
    $.ajax({
        url: '/ajax/print_book/send.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $('.c-callback-form').hide();
            $('.c-callback-success').show();
            sendCallbackYet();
        },
        error: function (data) {
            /*это оставить только для success*/
            $('.c-callback-form').hide();
            $('.c-callback-success').show();
            sendCallbackYet();
        }
    })
}
function sendCallbackYet() {
    $('body').on('click', '.c-send-yet', function () {
        $('.c-callback-form').show();
        $('.c-callback-success').hide();
        $('.c-callback-form input[name="phone"]').val('');
    })
}