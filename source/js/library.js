$(function () {
    deleteBookFromLibrary();    

    function deleteBookFromLibrary() {
        $('body').on('click', '.c-library-book-delete', function() {
            var $book = $(this).closest('.c-library-book');
            $book.remove();
            if($('.c-library-book').length < 1) {
                $('.c-library-empty').removeClass('hidden');
            }
        })
    }
});