$(function () {
    textareaAutosize();
    changePay();
    validateForm()
    inputNumber();
});

function textareaAutosize() {
    $('textarea').each(function(){
        autosize(this);
    }).on('autosize:resized', function(){

    });
}

function changePay() {
    $('body').on('click', '[data-click="pay"]', function (e) {
        e.preventDefault();
        $('[data-click="pay"]').removeClass('active').attr('data-state', 'off');
        $(this).addClass('active').attr('data-state', 'on');
    })
}

function validateForm() {
    $('form[ name="delivery"]').each(function(){
        var form = $(this);
        form.validate({
            rules: {
                name: "required",
                phone: "required",
                email: "required",
                comment: "required"
            },
            messages: {
                name: "Заполните поле",
                phone: "Заполните поле",
                email: "Заполните поле",
                comment: "Заполните поле"
            },
            submitHandler: function submitHandler(form) {
                sendForm($(form));
            }
        });
    })
}

function sendForm() {
    var data = $('form[ name="delivery"]').serialize();
    data += '&pay=' + $('[data-state="on"]').data('pay-type');
    console.log(data);
    // getFormData(data);
    // getDataFormWithoutPageCount(data);

    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        error: function (data) {
            console.log('ajax', data);
            $('[data-type="orderForm"]').hide();
            $('[data-type="success"]').show();
        }
    })
}

function inputNumber() {
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">—</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
}