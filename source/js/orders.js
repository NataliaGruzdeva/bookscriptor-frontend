$(function () {
    $(".c-datapicker").datepicker({
        dateFormat: 'd MM yy',
        beforeShow: function (input, inst) {
            $('#ui-datepicker-div').removeClass('custom-datapicker');
            $('#ui-datepicker-div').addClass('custom-datapicker');
        },
        onSelect: function (date) {
            $(this).val(setRussianDecline(date));
        }
    });
});