$(function () {
    loadAvatar();
    deleteAvatar();
    
    $('body').on('click', '.c-profile-tab', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');
        
        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');
        
        $('.c-profile-block').fadeOut().removeClass('active');
        $(showBlock).fadeIn().addClass('active');
    });
    
    
    $('body').on('click', '.c-add-other-socials', function (e) {
        let $this = $(this),
            $showBlock = $this.siblings('.c-other-socials');
        
        $this.fadeOut();
        $showBlock.fadeIn().addClass('active');
        
    })    
    
    function deleteAvatar() {
        $('body').on('click', '.c-delete-avatar', function (e) {
            var isDelete = confirm('Вы действительно хотите удалить текущую фотографию?');
            e.preventDefault();
            if(isDelete) {
                $('.c-delete-avatar').removeClass('show');
                $('.c-load-avatar').text('Загрузить фото');
            }
        })
    }
    
    function loadAvatar() {
        if($('.dropzone-avatar').length) {
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone(".dropzone-avatar", {
                maxFiles: 1,
                url: "/ajax/print_book/dropzone_upload.php",
                paramName: "image",
                init: function() {   
                    var $this = this;
                    
                    $this.on("success", function(file, done) {
                        //debugger;
                    });  
                    
                    $this.on("totaluploadprogress", function(file, progress) {
                        $('.c-load-avatar, .c-popup__txt').addClass('preload');
                        $('.c-preloader, .c-preloader-txt').show();
                    });   
                    
                    $this.on("complete", function(file, data) {
                        $('.c-load-avatar, .c-popup__txt').removeClass('preload');
                        $('.c-preloader, .c-preloader-txt').hide();
                        $('.c-load-avatar').text('Изменить фото');
                        $('.c-delete-avatar').addClass('show');
                        $.fancybox.close();
                        
                        download_file = file;
                        sendAvatar();
                    });

                },
                acceptedFiles: '.jpg, .jpeg, .png'
//                accept: function(file, done) {
//                    //download_file = file;
//                    //console.log(download_file);
//                    //$.fancybox.close();
//                }

            });
        }
    }
    
    function sendAvatar() {
        data = new FormData();
        data.append('avatar', download_file);
        console.log(data);

        $.ajax({
            url: '',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {

            }
        })
    }
});