function onCancelPage() {
    var isNotSave = false;
    $('.c-input-custom').each(function () {
        var value = $(this).val(),
            savedValue = $(this).data('value');
        if($(this).hasClass('c-input-phone')) {
            value = value.replace(/[(_) -]/g, '');
        }
        if (value != savedValue) {
            console.log(value);
            console.log(savedValue);
            isNotSave = true;
        }
        
    })
    if(isNotSave) {
        return "Покинуть эту страницу?";
    }
}