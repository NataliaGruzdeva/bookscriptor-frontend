$(function () {
    textareaAutosize();
    validateForm();
    inputNumber();
    tabBooks();
    readAll();
    sendFormBook();
    sendFormElBook();
    sendFormFreeBook();

});

function tabBooks() {
    $('body').on('click', '.c-profile-tab', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');

        $('.c-profile-block').removeClass('active');
        $(showBlock).addClass('active');
    });

}
function textareaAutosize() {
    $('textarea').each(function(){
        autosize(this);
    }).on('autosize:resized', function(){

    });
}

function readAll() {
    $('body').on('click', '[data-type="all"]', function (e) {
        e.preventDefault();
        $(this).siblings('[data-type="review"]').addClass('height-auto');
        $(this).hide();
    })
}

function validateForm() {
    $('form[ name="sendReview"]').each(function(){
        var form = $(this);
        form.validate({
            rules: {
                review: "required",
            },
            messages: {
                review: "Заполните поле",
            },
            submitHandler: function submitHandler(form) {
                sendFormReview($(form));
            }
        });
    })
}

function sendFormReview() {
    var data = $('form[ name="sendReview"]').serialize();
    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        error: function (data) {
            $('[data-type="sendReview"]').hide();
            $('[data-type="success"]').show();
        }
    })
}

function sendFormBook() {
    $('body').on('click', '[data-action="cart"]', function (e) {
        var data = $('form[ name="book"]').serialize();
        data += "&typeBook=print";
        console.log(data);
        $.ajax({
            url: '/ajax/print_book/send_anket.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {
                $('[data-action="cart"]').html('В корзине →');
            }
        })
    });
}
function sendFormElBook() {
    $('body').on('click', '[data-action="cart-el"]', function (e) {
        var data = $('form[ name="book"]').serialize();
        data += "&typeBook=electr";
        console.log(data);
        $.ajax({
            url: '/ajax/print_book/send_anket.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {
                $('[data-action="cart-el"]').html('В корзине →');
            }
        })
    });
}
function sendFormFreeBook() {
    $('body').on('click', '[data-action="biblio"]', function (e) {
        var data = $('form[ name="book"]').serialize();
        data += "&typeBook=el";
        console.log(data);
        $.ajax({
            url: '/ajax/print_book/send_anket.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {
                $('[data-action="biblio"]').html('В библиотеке →');
            }
        })
    });
}



function inputNumber() {
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">—</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            var newVal = oldValue + 1;
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
}