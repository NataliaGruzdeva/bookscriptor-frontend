$(function () {
    $('[data-type="accordion"]').accordion({
        active: false,
        collapsible: true,
        heightStyle: 'content'
    });
});
