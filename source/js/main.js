var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }

    return false;
};

/**
 * @deprecated
 * @returns {boolean}
 */
var isProductionHost = function isProductionHost() {
    return /^https:\/\/bookscriptor\.ru/.test(window.location.href);
};

/**
 * @param context
 * @param reload = -1|0|1
 * @param doFunction
 */
var wrapperCheckUserAge = function(context, reload, doFunction)
{
    var setAgeModal;
    var mustShowSetAgeModal = false;

    if ($("div.modal").is("#set-age-modal")) {
        setAgeModal = $('#set-age-modal');
        mustShowSetAgeModal = setAgeModal.data('must-show');
    }

    var inLibrary = $(context).is(".btn-library:not(.in-library)");

    if (mustShowSetAgeModal && !inLibrary) {
        var setAgeForm = setAgeModal.find('.set-age-form');
        var confirmSettingAgeButton = setAgeModal.find('.set-age-button');
        var waitingButton = confirmSettingAgeButton.waitingButton();

        var versionBlock = $(context).closest('.version');
        var bookContent = $(context).closest('.book-content');
        var bookResult = $('.book-result');

        var offerId = null;
        var bookId = null;
        if (versionBlock.is('.version')) {
            offerId = versionBlock.data('version-id');
            bookId = versionBlock.closest(".product").data("book-id");
        } else if (bookContent.is('.book-content')) {
            offerId = bookContent.data('version-id');
            bookId = bookContent.data('book-id');
        } else if (bookResult.is('.book-result')) {
            offerId = bookResult.data('version-id');
            bookId = bookResult.data('book-id');
        }

        setAgeModal.modal('show');

        confirmSettingAgeButton.on('click', function (e) {
            if ($(this).attr('disabled') !== undefined) {
                e.preventDefault();
                return;
            }
            waitingButton.startWaiting();

            var userAge = setAgeForm.find('[name=userAge]:checked').val();
            userAge = parseInt(userAge);

            var canRead = false;

            if (userAge > 0) {
                $.ajax('/ajax/setAge.php', {
                    type: 'post',
                    data: {
                        age: userAge,
                        bookId: bookId,
                        offerId: offerId,
                        reset: true
                    },
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        if (bookContent !== undefined) {
                            bookContent.data('can-read', data.canRead);
                        }
                        if (versionBlock !== undefined) {
                            versionBlock.data('can-be-added-to-library', data.canInLibrary);
                            versionBlock.closest('.product').data('can-read', data.canRead);
                        }
                        setAgeModal.data('must-show', '');
                        setAgeModal.modal('hide');

                        if ((data.canRead && !reload)
                            || (data.canRead && reload < 0)
                        ) {
                            doFunction(context);
                        }

                        canRead = data.canRead;

                        waitingButton.stopWaiting();
                    }
                });
            }

            if (reload > 0 || (!canRead && reload < 0)) {
                window.location.reload();
            }

            return false;
        });

        setAgeForm.on('change', '[name=userAge]', function () {
            confirmSettingAgeButton.removeClass('disabled');
            confirmSettingAgeButton.removeAttr('disabled');
        });
    } else {
        doFunction(context);
    }
};

function ageTooltipHandler(e)
{
    showTooltipAgeRestricted(this);
    return false;
}

function hideTooltipAgeRestricted(e)
{
    if (!$(e.target).hasClass("tooltip-inner")
        && ($(e.target).attr("id") != "btn-book-preview")
    ) {
        $("*").tooltip("destroy");
        $(document).off("click", hideTooltipAgeRestricted);
    }
}

function showTooltipAgeRestricted(context)
{
    var tooltipId = $(context).attr("aria-describedby");

    if (tooltipId === undefined) {
        addTooltipAgeRestricted($(context));
        $(context).tooltip("show");
        $(document).bind("click", hideTooltipAgeRestricted);
    } else {
        $(context).tooltip("destroy");
        $(document).bind("click", hideTooltipAgeRestricted);
    }
}

function addTooltipAgeRestricted(btn)
{
    btn.attr('data-toggle', 'tooltip');
    btn.attr('data-original-title', 'Эта книга имеет возрастное ограничение. <a href="#" class="tooltip-age-again link-solid-white">Укажите</a> свой возраст');
    btn.tooltip({
        html: true,
        trigger: "manual",
        container: "body"
    });
}

function toCartButton(button) {
    return {
        makeEnabledCartButton: function() {
            button.removeClass();
            button.addClass('btn btn-red btn-fs-12 btn-majuscule btn-fw-semibold btn-cart').text('В корзину');
        },
        makeDisabledCartButton: function(cartUrl) {
            button.removeClass();
            button.addClass('btn btn-red btn-fs-12 btn-majuscule btn-fw-semibold btn-cart in-cart btn-green').text('В корзине');
            button.attr('href', cartUrl)
        },
        makeToLibraryButton: function() {
            button.removeClass();
            button.addClass('btn btn-red btn-fs-12 btn-majuscule btn-fw-semibold btn-library').text('В библиотеку');
        },
        makeInLibraryButton: function(readUrl, canRead) {
            button.removeClass();
            button.addClass('btn btn-green btn-fs-12 btn-majuscule btn-fw-semibold btn-library in-library').text('Читать');
            if (canRead) {
                button.attr('href', readUrl)
            } else {
                addTooltipAgeRestricted(button);
            }
        },
        makeAgeRestricted: function() {
            addTooltipAgeRestricted(button);
        }
    }
}

$.fn.waitingButton = function() {
    var $this = $(this);

    var $text = $('.waiting-button-text:first', $this);
    $this.data('original-text', $text.text());

    var waitingText = $this.data('waiting-text');
    var originalText = $this.data('original-text');

    var setWaitingText = function(text) {
        waitingText = text;
    };

    var startWaiting = function () {
        $this.attr('disabled', 'disabled');
        $('.fa-spinner:first', $this).removeClass('hidden');

        if (waitingText){
            //$this.data('original-text', $text.text());
            $text.text(waitingText);
        }
    };

    var stopWaiting = function () {
        $this.removeAttr('disabled');
        $('.fa-spinner:first', $this).addClass('hidden');

        if (originalText){
            $text.text(originalText);
            //$this.data('original-text', $text.text());
        }
    };

    return {
        setWaitingText: setWaitingText,
        startWaiting: startWaiting,
        stopWaiting: stopWaiting
    }
};

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$.fn.fileUploadAnimation = function() {
    var loadingBlock = $(this);

    var lines = loadingBlock.find(".lines .line");
    var circleColor = loadingBlock.find("circle.circle-color");
    var svgBlock = loadingBlock.find("svg");
    var progressPercentageBlock = loadingBlock.find('.progress-percentage');

    var svgStrokeLength = 380;

    var start = function() {
        circleColor.css('stroke-dashoffset', svgStrokeLength);
    };

    var progress = function(percent, delay) {
        var percentNew = svgStrokeLength - (svgStrokeLength * percent / 100);
        circleColor.animate({
            'stroke-dashoffset': percentNew
        }, delay, function () {
            var lineNumber = 0;

            // TODO: refactor.
            if (percent <= 30) lineNumber = 0;
            else if (percent <= 45) lineNumber = 1;
            else if (percent <= 60) lineNumber = 2;
            else if (percent <= 75) lineNumber = 3;
            else if (percent <= 90) lineNumber = 4;
            else lineNumber = 5;

            lines.eq(lineNumber).addClass('progress');
            progressPercentageBlock.text(percent);

            if (percentNew == 0) {
                setTimeout(function () {
                    svgBlock.addClass('animated zoomOut');
                }, delay);
            }
        });
    };

    return {
        start: start,
        progress: progress
    }
};

$.fn.validateField = function() {
    var input = $(this);

    function setFieldError(errorsBlock, errorText) {
        input.addClass('error');
        errorsBlock.text('').append('<div class="error">' + errorText + '</div>').show();
    }

    function clearFieldErrors(errorsBlock) {
        input.removeClass('error');
        errorsBlock.text('');
    }

    function validateNotEmpty(errorsBlock, errorText) {
        if (input.val() == '') {
            setFieldError(errorsBlock, errorText);
            return false;
        } else {
            clearFieldErrors(errorsBlock);
            return true;
        }
    }

    function validateIsChecked(errorsBlock, errorText) {
        if (input.filter(':checked').size() == 0) {
            setFieldError(errorsBlock, errorText);
            return false;
        } else {
            clearFieldErrors(errorsBlock);
            return true;
        }
    }

    function validateInteger(errorsBlock, errorText, min, max) {
        var isValid = $.isNumeric(input.val());
        if ($.isNumeric(min)) {
            isValid = isValid && input.val() >= min;
        }
        if ($.isNumeric(max)) {
            isValid = isValid && input.val() <= max;
        }
        if (!isValid) {
            setFieldError(errorsBlock, errorText);
            return false;
        } else {
            clearFieldErrors(errorsBlock);
            return true;
        }
    }

    function validatePhone(errorsBlock, errorText) {
        if (!input.inputmask('isComplete')) {
            setFieldError(errorsBlock, errorText);
            return false;
        } else {
            clearFieldErrors(errorsBlock);
            return true;
        }
    }

    return {
        setFieldError: setFieldError,
        clearFieldErrors: clearFieldErrors,
        validateNotEmpty: validateNotEmpty,
        validateIsChecked: validateIsChecked,
        validateInteger: validateInteger,
        validatePhone: validatePhone
    }
};

// TODO: extend $.fn instead.
window.FieldAjaxValidation = function(hideErrorsBlock) {
    var fields = [];
    var inputBlocks = [];
    var errorsBlocks = [];

    // TODO: refactor.
    if (hideErrorsBlock === undefined) {
        hideErrorsBlock = true;
    }

    var addField = function(fieldName, inputBlock, errorsBlock) {
        if (fields.indexOf(fieldName) === -1) {
            fields.push(fieldName);
            inputBlocks[fieldName] = inputBlock;
            errorsBlocks[fieldName] = errorsBlock;
        }
    };

    var success = function(data) {
        var k;
        for (k in inputBlocks) {
            if (inputBlocks.hasOwnProperty(k)) {
                inputBlocks[k].removeClass('error');
            }
        }
        for (k in errorsBlocks) {
            if (errorsBlocks.hasOwnProperty(k)) {
                errorsBlocks[k].text('');
                if (hideErrorsBlock) {
                    errorsBlocks[k].hide();
                }
            }
        }
    };

    var error = function(jqXHR) {
        fields.forEach(function(field) {
            if ($.isPlainObject(jqXHR['responseJSON']['errors'])) {
                var errors = jqXHR['responseJSON']['errors'][field];
                if ($.isArray(errors)) {
                    inputBlocks[field].tooltip('destroy');
                    if (inputBlocks[field].is(errorsBlocks[field])) {
                        inputBlocks[field].attr('data-title', errors.join('<br>'));
                        inputBlocks[field].tooltip({title: errors.join('<br>')});
                    } else {
                        errorsBlocks[field].text('');
                        errors.forEach(function(item) {
                            errorsBlocks[field].append('<div class="error">' + item + '</div>');
                        });
                        if (hideErrorsBlock) {
                            errorsBlocks[field].show();
                        }
                    }
                    inputBlocks[field].addClass('error');
                } else {
                    inputBlocks[field].removeClass('error');
                    errorsBlocks[field].text('');
                    if (hideErrorsBlock) {
                        errorsBlocks[field].hide();
                    }
                }
            } else {
                errorsBlocks[field].append('<div class="error">Произошла неизвестная ошибка. Повторите попытку позже.</div>');
            }
        });
    };

    return {
        success: success,
        error: error,
        addField: addField
    }
};

/** Ajax loading */

$.fn.Loader = function(loadableArea) {
    var loader = $(this);
    // var loaderContent = loader.find('.content');
    // var overflow = $('body').css('overflow');
    // var position = $('body').css('position');
    // var placeLoader = function() {
    //     var newTop = document.body.clientHeight / 2 + $(window).scrollTop();
    //     var loaderBottom = loader.offset().top + loader.height();
    //     var loaderTop = loader.offset().top;
    //     if ((loaderTop < newTop - 60) && (loaderBottom > newTop + 60)) {
    //         loaderContent.offset({
    //             top: newTop
    //         });
    //     }
    // };
    var start = function() {
        // $('body').css('overflow', 'hidden');
        // $('body').css('position', 'relative');
        loader.show();
        var scroll = $('body').scrollTop();
        $('.body-wrap').addClass('no-scrolling');
        // loader.width(loadableArea.width());
        // loader.height(loadableArea.height());
        // var leftOffset = loadableArea.offset().left;
        // var topOffset = loadableArea.offset().top;
        // loader.offset({
        //     left: leftOffset,
        //     top: topOffset
        // });
        // placeLoader();
        // $(window).on('resize', function() {
        //     placeLoader();
        // });
        // $(window).on('scroll', function() {
        //     placeLoader();
        // });
    };
    var stop = function() {
        loader.hide();
        $('.body-wrap').removeClass('no-scrolling');
        // $('body').css('overflow', overflow);
        // $('body').css('position', position);
    };
    return {
        start: start,
        stop: stop
    }
};

$.fn.initPhoneMask = function() {
    this.formatter({
        'pattern': '+7 ({{999}}) {{999}}-{{99}}-{{99}}',
        'persistent': false
    });
    if (this.val() === undefined || !this.val().length) {
        this.val('+7');
    } else {
        this.resetPattern();
    }
    this.each(function() {
        var $this = $(this);
        $this.on('keyup', function(e) {
            if (e.target.value == '') {
                $this.formatter().resetPattern('+{{99999999999999}}');
            }
            if (e.target.value == '+7') {
                $this.formatter().resetPattern('+7 ({{999}}) {{999}}-{{99}}-{{99}}');
            }
        });
    });
};

window.getSelect2CommonConfig = function() {
    return {
        language: {
            noResults: function() {
                return 'По вашему запросу ничего не найдено';
            }
        }
    };
};

/**
 * The main Bookscriptor JS library.
 * @type {{getAnchorFromUrl: Window.bks.getAnchorFromUrl, getAnchorFromCurrentUrl: Window.bks.getAnchorFromCurrentUrl, fileTools: Window.bks.fileTools, ajaxJsonResponse: Window.bks.ajaxJsonResponse, isProductionHost: Window.bks.isProductionHost, isTestBot: Window.bks.isTestBot, reachYandexMetrikaGoal: Window.bks.reachYandexMetrikaGoal, isAdminPanel: Window.bks.isAdminPanel}}
 */
window.bks = {
    getAnchorFromUrl: function(url) {
        var idx = url.indexOf("#");
        return idx != -1 ? url.substring(idx + 1) : "";
    },
    getAnchorFromCurrentUrl: function() {
        return bks.getAnchorFromUrl(window.location.href);
    },
    fileTools: function(file) {
        var fileReader = new FileReader();
        var header = "";
        var callbackFunction;

        fileReader.onloadend = function(e) {
            var arr = (new Uint8Array(e.target.result)).subarray(0, 4);

            for (var i = 0; i < arr.length; i++) {
                var byteStr = arr[i].toString(16);
                header += arr[i] < 10 ? '0' + byteStr : byteStr;
            }

            callbackFunction.call();
        };

        fileReader.readAsArrayBuffer(file);

        return {
            /**
             * @callback onDetectMimeTypeCallback
             * @param {Object} result
             * @param {Function} result.isDoc - Is the file in the DOC format.
             * @param {Function} result.isZip - Is the file in the ZIP format. Such file may be in the DOCX or ODT format.
             * @param {Function} result.isRtf - Is the file in the RTF format.
             * @param {Function} result.isPdf - Is the file in the PDF format.
             * @param {Function} result.isPng - Is the file in the PNG format.
             * @param {Function} result.isJpg - Is the file in the JPEG/JFIF/Exif format.
             * @param {Function} result.isTxt - Is the file in the TXT format.
             * @param {File} file - The processed file.
             */

            /**
             * Calls given callback when a file type will be determined.
             * @param {onDetectMimeTypeCallback} callback - The callback that will be called with the result.
             */
            onDetectMimeType: function(callback) {
                callbackFunction = callback.bind(null, {
                    isDoc: function() { return header == 'd0cf11e0'; },
                    isZip: function() { return header == '504b0304'; }, //docx, odt
                    isRtf: function() { return header == '7b5c7274'; },
                    isPdf: function() { return header == '25504446'; },
                    isPng: function() { return header == '89504e47'; },
                    isJpg: function() { return $.inArray(header, ['ffd8ffdb', 'ffd8ffe0', 'ffd8ffe1']); },
                    isTxt: function() { return file.type == 'text/plain' }
                }, file);
            }
        }
    },
    ajaxJsonResponse: function(response) {
        return {
            getField: function(code, defaultValue) {
                if ($.isPlainObject(response['jqXHR']['responseJSON'])) {
                    var object = response['jqXHR']['responseJSON'];
                    if (object[code] !== undefined) {
                        return object[code];
                    }
                }
                return defaultValue !== undefined ? defaultValue : null;
            }
        }
    },
    isProductionHost: function () {
        return /^https:\/\/bookscriptor\.ru/.test(window.location.href);
    },
    isTestBot: function() {
        return (getUrlParameter("test") == 1) || $("#is-test-user").is("span");
    },
    isAdminPanel: function() {
        return $("#bx-panel").is("div");
    },
    reachYandexMetrikaGoal: function(goalCode) {
        if (!bks.isTestBot() && bks.isProductionHost() && yaCounter29315860 !== undefined) {
            yaCounter29315860.reachGoal(goalCode);
        }
    },
    addBookOfferToCart: function (bookOfferId, callback) {
        $.ajax('/ajax/addBookOfferToBasket.php', {
            type: 'post',
            data: {bookOfferId: bookOfferId},
            dataType: 'json',
            async: false,
            success: callback
        });
    }
};

$(document).ready(function(){
    $('#close-no-pay').click(function(e){
        e.preventDefault();

        $.post('/ajax/hideUnpaid.php')
            .done(function(data) {
                $('.dropdown.dropdown-info').removeClass('open');
            });
    });


    /** Common variables */

    var pageTitle = $(document).find("title").text();

    /** Main navigation */

    var mainNavBasket = $('.add-panel .sum .reader');
    var mainNavBasketItemsTotal = mainNavBasket.find('.basket .items-total');
    var mainNavBasketPrice = mainNavBasket.find('.money .price');
    var mainUserMenu = $('.main-header .profile .dropdown-menu');
    var mainUserMenuFavorites = mainUserMenu.find('.favorite');
    var mainUserMenuFavoritesCount = mainUserMenuFavorites.find('.favorites-count');

    var siteSearchForm = $('.main-header .site-search-form');
    var siteSearchInput = siteSearchForm.find('.input-search');
    var siteSearchIcon = siteSearchForm.find('.icon-search');

    $('body').on('click', '.dropdown-menu .search-type-element', function(e) {
        e.preventDefault();
        $('.current-search-type:first').text($(this).text());
        $('.current-search-type:first').data('search-type', $(this).data('search-type'));
    });

    function setMainNavBasket(itemsTotal, totalPrice) {
        mainNavBasketItemsTotal.html(itemsTotal);
        mainNavBasketPrice.html(totalPrice);
    }

    function setMainUserMenuFavoritesCount(count) {
        mainUserMenuFavoritesCount.html(count);
    }

    window.toggleFavoriteAjax = function(bookId, forceReload) {
        forceReload = forceReload || false;

        if (forceReload) {
            booksLoader.start();
        }

        $.ajax('/ajax/toggleFavoriteBook.php', {
            type: 'post',
            data: {
                bookId: bookId
            },
            dataType: 'json',
            success: function(data) {
                setMainUserMenuFavoritesCount(data['favorites-count']);
                if (forceReload) {
                    loadBooksByAjax();
                }
            }
        });
    };

    /** Breadcrumbs */

    $('nav.breadcrumb ul li a.current').on('click', function(e) {
        e.preventDefault();
    });

    $('.selection').mCustomScrollbar({
        theme:'dark',
        scrollInertia:600
    });
    $(window).stellar({
        responsive: true,
        horizontalScrolling: false,
        hideDistantzElements: false,
        horizontalOffset: 0,
        verticalOffset: 0,
    });
    $('.title').dotdotdot();
    $('.events .description').dotdotdot();

    $('select').select2($.merge(window.getSelect2CommonConfig(), {
        minimumResultsForSearch: Infinity
    }));

    /** Popovers */
    $('body').on('click', '[data-toggle="popover"]', function(e){
        e.preventDefault();
    });

    $('body').popover({
        selector: '[data-toggle="popover"]'
    });

    //$(".e-book-button-info").popover();

    /** Tooltips */

    $("[data-toggle='tooltip']").tooltip();

    $("body").on("click", ".tooltip-age-again", function (e) {
        e.preventDefault();
        if ($("div.modal").is("#set-age-modal")) {
            setAgeModal = $('#set-age-modal');
            var birthday = setAgeModal.data('birthday');

            if (birthday.length) {
                window.location.href = "/personal/profile/edit/#date-container";
            } else {
                setAgeModal.data("must-show", 1);
                wrapperCheckUserAge(this, 1, function (context) {
                    $('#set-age-modal').data("must-show", "");
                });
            }
        }
    });

    /*  Collapse */

    $('.category-figure').on('show.bs.collapse', function () {
        $('.category-title',this).addClass('category-open');
    });
    $('.category-figure').on('hide.bs.collapse', function () {
        $('.category-title',this).removeClass('category-open');
    });

    function juryVote(awardsId, nomineeId, score)
    {
        var totalScore = false;

        $.ajax('/ajax/gorkyAwards.php', {
            type: 'post',
            data: {
                action: 'juryVote',
                awardsId: awardsId,
                nomineeId: nomineeId,
                score: score,
                update: true
            },
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.result === true) {
                    totalScore = data.totalScore;
                }
            }
        });

        return totalScore;
    }

    function raitingStar () {
        $('.rating-stars.changeable li').on('mouseover', function(){
            var onStar = parseInt($(this).data('value'), 10);

            $(this).parent().children('li.star').each(function(e){
                if (e < onStar) {
                    $(this).addClass('hover');
                }
                else {
                    $(this).removeClass('hover');
                }
            });

        }).on('mouseout', function(){
            $(this).parent().children('li.star').each(function(e){
                $(this).removeClass('hover');
            });
        });

        $('.rating-stars.changeable li').on('click', function(){
            var onStar = parseInt($(this).data('value'), 10);
            var stars = $(this).parent().children('li.star');
            var nomineeItem = $(this).closest(".book-premium-box");

            if (onStar > 0) {
                var awardsId = nomineeItem.data("awards");
                var nomineeId = nomineeItem.data("nominee");
                var totalScore = juryVote(awardsId, nomineeId, onStar);
                nomineeItem.find(".total-sum").html(totalScore);
            }

            for (i = 0; i < stars.length; i++) {
                $(stars[i]).removeClass('selected');
            }

            for (i = 0; i < onStar; i++) {
                $(stars[i]).addClass('selected');
            }
        });
    }

    var registrationModal = $('#enter-registration');

    function registrationEnterEvents(){
        var btnEnter = $('.btn-enter');
        var btnRegistration = $('.btn-registration');

        btnEnter.on('click', function(e){
            e.preventDefault();
            $('a[href="#tab-enter"]').tab('show')
        });

        btnRegistration.on('click', function(e){
            e.preventDefault();
            $('a[href="#tab-registration"]').tab('show')
        });
    }

    registrationEnterEvents();

    if($('*').is('.rating-stars.changeable')) { raitingStar(); }

    /** Main page **/
    var mainPage = $('.main-page');

    if (mainPage.size()) {
        mainPage.find("#more-facts-btn").click(function (event) {
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: '/ajax/randomFact.php?curr_id=' + mainPage.find("#more-facts-btn").attr('rel'),
                cache: false,
                dataType: 'json',
                data: {
                    ajax: true,
                    action: 'update'
                },
                success: function(data) {
                    var timeAnimation = 600;
                    var factsContainer =  mainPage.find('#note');
                    factsContainer.fadeOut(timeAnimation, function () {
                        factsContainer.html(data.html);
                        factsContainer.fadeIn(timeAnimation);
                        mainPage.find("#more-facts-btn").attr('rel', data.id);
                    });

                }
            });
        });
        // end function
        mainPage.find('.event-show').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            initialSlide: mainPage.find('.event').attr('rel'),
            asNavFor: '.event-date'
        });
        mainPage.find('.event-date').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.event-show',
            loop:true,
            initialSlide: mainPage.find('.event').attr('rel'),
            dots: false,
            arrows: false,
            centerMode: true,
            focusOnSelect: true
        });
    }

    /** Main page **/
    var panelModule = $('.panel');

    if ($(panelModule).length > 0) {
        panelModule.find('#panel-book-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 3,
            arrows: true,
            dots: true,
            infinite: false,
            draggable: false,
            lazyLoad: 'progressive',
        });
    }

    panelModule.find('.close').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.panel').slideUp('fast');
    });

    panelModule.find('.cover.cover-favorite .favorite').mouseout(function () {
        $(this).tooltip('hide');
    });

    panelModule.find('.cover.cover-favorite .favorite-on').tooltip({
        placement: 'bottom',
    }).attr('data-original-title', 'Удалить из избранного');
    $('.cover.cover-favorite .favorite').click(function (e) {
        e.preventDefault();
        $self = $(this);
        $self.tooltip('hide');
        if ($self.hasClass('favorite-on')) {
            $self.removeClass('favorite-on');
            $self.parents('.book-content').addClass('book-delete');
            $self.tooltip({
                placement: 'bottom'
            }).attr('data-original-title', 'Добавить в избранное').tooltip('hide');
        } else {
            $self.addClass('favorite-on');
            $self.parents('.book-content').removeClass('book-delete');
            $self.tooltip({
                placement: 'bottom'
            }).attr('data-original-title', 'Удалить из избранного').tooltip('hide');
        }
    });



    /** Catalog page */

    /** Searching */
    var queryType = $('.current-search-type:first').data("search-type");
    var articleQueryTypes = ['article_name', 'article_text'];
    var query = "";

    var executeSearch = function($context) {
        query = $context.val();
        if (query && query.length >= 3) {
            queryType = $('.current-search-type:first').data("search-type");
            if (articleQueryTypes.indexOf(queryType) >= 0) {
                alert("ARTICLE");
            } else {
                if ($('#catalog-section').length > 0) {
                    loadBooksByAjax();
                } else {
                    window.location.href = '/catalog/?query=' + query + '&queryType=' + queryType;
                }
            }
        }
    };

    siteSearchInput.on('keypress', function(e) {
        if (e.which == 13) {
            executeSearch($(this));
        }
    });

    siteSearchIcon.on('click', function(e) {
        e.preventDefault();
        executeSearch($(this).siblings('.input-search'));
    });

    if ($("div").is(".calculate")) {
        $(".calculate").on('click', '.proceed-btn-wrap span.btn', ageTooltipHandler);
    }

    var userLibraryPage = $("div.library");
    if (userLibraryPage.length) {
        var libraryPageLoadableArea = userLibraryPage.find('#books-loadable-area');
        libraryPageLoadableArea.on('click', '.btn-library.in-library', function (e) {
            e.preventDefault();
            wrapperCheckUserAge(this, 1, function(context) {
                var bookContent = $(context).closest('.book-content');
                var canRead = bookContent.data('can-read');
                if (canRead) {
                    window.open($(context).attr("href"), '_blank');
                } else {
                    $(context).attr("href", "");
                    showTooltipAgeRestricted(context);
                }
            });

            return false;
        });
    }

    var catalogPage = $('.catalog-page, .authors-list-page');
    if (catalogPage.size() > 0) {
        var catalogPageLoadableArea = catalogPage.find('#books-loadable-area, #users-loadable-area');
        var catalogPageLoader = catalogPage.find('#books-area-loader, #users-area-loader');

        var booksLoader = catalogPageLoader.Loader(catalogPageLoadableArea);
        var pagesTotal = catalogPage.data('pages-total');
        var pageNum = catalogPage.data('page-number');
        var displayBy = catalogPage.data('display-by');
        var sortBy = catalogPage.data('sort-by');
        var sortDir = catalogPage.data('sort-direction');
        var query = catalogPage.data('query');

        var bookVersionsInCart = catalogPage.data('book-versions-in-cart');
        var bookVersionsInLibrary = catalogPage.data('book-versions-in-library');

        // cast version identifiers to integers
        bookVersionsInCart.forEach(function(item, i, arr) {
            arr[i] = Number(arr[i]);
        });
        bookVersionsInLibrary.forEach(function(item, i, arr) {
            arr[i] = Number(arr[i]);
        });

        var loadBooksByAjax = function(data, successCallback) {
            var queryParams = jQuery.query
                .set("displayBy", displayBy)
                .set("sortBy", sortBy)
                .set("sortDir", sortDir)
                .set("pageNumber", pageNum)
                .set("query", query)
                .set("queryType", queryType);

            window.history.pushState(queryParams, pageTitle, queryParams);

            booksLoader.start();

            $.ajax('', {
                type: 'post',
                data: $.extend({
                    ajax: true,
                    displayBy: displayBy,
                    sortBy: sortBy,
                    sortDir: sortDir,
                    pageNumber: pageNum,
                    query: query,
                    queryType: queryType
                }, data),
                dataType: 'json',
                success: function(data) {
                    if (data['booksContent'] !== undefined) {
                        pagesTotal = data['pagesTotal'];
                        catalogPageLoadableArea.html(data['booksContent']);
                        catalogPageLoadableArea.find('[data-toggle="popover"]').popover();

                        if (successCallback !== undefined) {
                            successCallback(data);
                        }

                        $('.title').dotdotdot();
                        console.log('Books are loaded.');
                    } else {
                        console.log('An error on books loading is occurred.');
                    }
                },
                error: function(xhr) {
                    console.log('Unable to load books by the reason: ' + xhr.statusText);
                },
                complete: function() {
                    booksLoader.stop();
                }
            });
        };

        /** Popovers dynamics */

        catalogPage.find('.link-popover').on('click', function (e) {
            e.preventDefault();
        });

        $(document).click(function(event) {
            var popover = $(event.target).closest('.link-popover');
            $('.link-popover').not(popover.get()).popover('hide');
        });

        /** Display by */

        catalogPage.find('.display-by').on('click', '.dropdown-menu .display-by-link', function(e) {
            e.preventDefault();
            displayBy = $(this).data('display-by-number');
            var displayByText = $(this).data('display-by-text');
            $(this).closest('.display-by').find('.display-by-number').text(displayByText);
            loadBooksByAjax();
        });

        /** Sorting */

        catalogPage.find('.sort').on('click', '.dropdown-menu .sort-link', function(e) {
            e.preventDefault();
            sortBy = $(this).data('sort-by');
            sortDir = $(this).data('sort-direction');
            pageNum = 1;
            var text = $(this).data('link-text');
            $(this).closest('.sort').find('.link-dropdown').html(text);
            loadBooksByAjax();
        });

        /** Pagination */

        catalogPageLoadableArea.on('click', '.catalog-nav .catalog-nav-item-link, .users-nav .users-nav-item-link', function(e) {
            e.preventDefault();
            pageNum = $(this).data('page-num');
            loadBooksByAjax();
        });

        catalogPageLoadableArea.on('click', '.catalog-nav .catalog-nav-arrow-left, .users-nav .users-nav-arrow-left', function(e) {
            e.preventDefault();
            if (pageNum - 1 >= 1) {
                pageNum -= 1;
                loadBooksByAjax();
            }
        });

        catalogPageLoadableArea.on('click', '.catalog-nav .catalog-nav-arrow-right, .users-nav .users-nav-arrow-right', function(e) {
            e.preventDefault();
            console.log(pageNum + ' ' + pagesTotal);
            if (pageNum + 1 <= pagesTotal) {
                pageNum += 1;
                loadBooksByAjax();
            }
        });

        /** Favorites */

        catalogPageLoadableArea.on('click', '.book-content .actions .favorite', function (e) {
            e.preventDefault();
            var forceReload = false;

            if ($(this).hasClass('favorites-reload')) {
                forceReload = true;
            }

            if ($(this).hasClass('no-action')) {
                return;
            }

            var bookId = $(this).closest('.book-content').data('book-id');

            $(this).toggleClass('favorite-on');

            if ($(this).hasClass('favorite-remove')) {
                $(this).parents('.book-content').remove();
            }

            window.toggleFavoriteAjax(bookId, forceReload);
        });

        catalogPageLoadableArea.on('click', '.book-content .actions .btn-cart:not(.in-cart), .book-content .actions .btn-library:not(.in-library)', function (e) {
            e.preventDefault();
            wrapperCheckUserAge(this, 1, function(context) {
                var bookContent = $(context).closest('.book-content');
                var versionId = bookContent.data('version-id');
                var readUrl = bookContent.data('read-url');
                var canRead = bookContent.data('can-read');
                var cartUrl = bookContent.data('cart-url');
                var inLibrary = $(context).is(".btn-library:not(.in-library)");

                if (canRead || inLibrary) {
                    if ($(context).hasClass('btn-cart')) {
                        bookVersionsInCart.push(versionId);
                        toCartButton($(context)).makeDisabledCartButton(cartUrl);
                    } else if ($(context).hasClass('btn-library')) {
                        bookVersionsInLibrary.push(versionId);
                        toCartButton($(context)).makeInLibraryButton(readUrl, canRead);
                    }
                    bks.addBookOfferToCart(versionId, function () {
                        setMainNavBasket(data['itemsTotal'], data['totalPrice']);
                    });
                } else {
                    toCartButton($(context)).makeAgeRestricted();
                }
            });

            return false;
        });

        catalogPageLoadableArea.on('click', '.book-content .actions span.btn', ageTooltipHandler);

        catalogPageLoadableArea.on('click', '.btn-library.in-library', function (e) {
            e.preventDefault();
            wrapperCheckUserAge(this, 1, function(context) {
                var bookContent = $(context).closest('.book-content');
                var canRead = bookContent.data('can-read');
                if (canRead) {
                    window.open($(context).attr("href"), '_blank');
                } else {
                    $(context).attr("href", "");
                    showTooltipAgeRestricted(context);
                }
            });

            return false;
        });

        /** Books versions */

        catalogPageLoadableArea.on('click', '.link-popover', function (e) {
            $(this).popover('show');
            e.preventDefault();

            var changeVersion = function(linkBlock) {
                var bookBlock = linkBlock.closest('.book-content');
                var versionBlock = bookBlock.find('.version-item');
                var versionClass = linkBlock.data('version-class');
                var versionText = linkBlock.text();
                var selectedVersionId = linkBlock.data('version-id');
                var canBeAddedToLibrary = linkBlock.data('can-be-added-to-library');
                var readUrl = bookBlock.data('read-url');
                var canRead = bookBlock.data('can-read');
                var cartUrl = bookBlock.data('cart-url');

                bookBlock.data('version-id', selectedVersionId);
                bookBlock.find('.price').html(linkBlock.data('price'));
                versionBlock.removeClass('electronic print audio').addClass(versionClass);
                versionBlock.find('.version-link .version-link-text').text(versionText);

                var btnCart = toCartButton(bookBlock.find('.btn-cart, .btn-library'));
                var btnCalc = bookBlock.find('.btn-calculate');
                var allBtns = bookBlock.find('.btn');//Необходимо для неавторизованных пользователей

                if (bookVersionsInCart.indexOf(selectedVersionId) >= 0) {
                    btnCart.makeDisabledCartButton(cartUrl);
                } else if (bookVersionsInLibrary.indexOf(selectedVersionId) >= 0) {
                    btnCart.makeInLibraryButton(readUrl, canRead);
                } else if (canBeAddedToLibrary) {
                    btnCart.makeToLibraryButton();
                } else {
                    btnCart.makeEnabledCartButton();
                }

                switch (versionClass) {
                    case 'print':
                        allBtns.addClass('hidden');
                        btnCalc.removeClass('hidden');
                        break;
                    default:
                        allBtns.removeClass('hidden');
                        btnCalc.addClass('hidden');
                        break;
                }
            };

            var popover = $(this).siblings('.popover');
            $('body').on('click', '.version-item-link', function(e){
                e.preventDefault();
                changeVersion($(this));
            });
        });
    }

    /** Setting age modal */
    function getBasketUrl()
    {
        return $("li.basket > .items-total").attr("href");
    }

    var productPage = $('.row.product');
    if (productPage.size()) {
        var bookId = productPage.data('book-id');

        var complainModal = productPage.find('#complain-modal');
        var complainForm = productPage.find('.complain-form');

        var complainSubjectInput = complainForm.find('[name=complainSubject]');
        var complainTextInput = complainForm.find('[name=complainText]');

        var complainSubjectErrorsBlock = complainSubjectInput.next('.errors');
        var complainTextErrorsBlock = complainTextInput.next('.errors');

        var sendComplainButton = complainModal.find('.send-complain-button');
        var complainThxText = complainModal.find('.complain-thx-text');

        productPage.find('.cover-side .complain').on('click', function() {
            complainModal.modal('show');
        });

        sendComplainButton.on('click', function(e) {
            e.preventDefault();

            var formData = complainForm.serializeObject();

            var subjectValid = complainSubjectInput.validateField().validateNotEmpty(complainSubjectErrorsBlock, 'Введите тему жалобы');
            var complainValid = complainTextInput.validateField().validateNotEmpty(complainTextErrorsBlock, 'Введите текст жалобы');

            if (!subjectValid || !complainValid) {
                return;
            }

            $.ajax('/ajax/complain.php', {
                type: 'post',
                data: $.extend(formData, {
                    objectId: bookId,
                    objectType: 'book'
                }),
                dataType: 'json',
                complete: function() {
                    complainForm.hide();
                    complainThxText.show();
                }
            });
        });

        /** Favorites */

        productPage.find('.book-info .description .favorite').on('click', function (e) {
            e.preventDefault();

            if ($(this).hasClass('no-action')) {
                return;
            }

            $(this).toggleClass('favorite-on');
            window.toggleFavoriteAjax(bookId);
        });

        /** Book versions */

        var bookVersions = productPage.find('.product-version .version');

        bookVersions.on('click', '.btn-cart:not(.in-cart), .btn-library:not(.in-library)', function(e) {
            e.preventDefault();
            wrapperCheckUserAge(this, 0, function(context) {
                var versionBlock = $(context).closest('.version');
                var versionId = versionBlock.data('version-id');
                var canBeAddedToLibrary = versionBlock.data('can-be-added-to-library');
                var readUrl = versionBlock.closest('.product').data('read-url');
                var canRead = versionBlock.closest('.product').data('can-read');
                var cartUrl = versionBlock.closest('.product').data('cart-url');
                var inLibrary = $(context).is(".btn-library:not(.in-library)");

                if (canRead || inLibrary) {
                    if (canBeAddedToLibrary) {
                        toCartButton($(context)).makeInLibraryButton(readUrl, canRead);
                    } else {
                        toCartButton($(context)).makeDisabledCartButton(cartUrl);
                    }
                    bks.addBookOfferToCart(versionId, function () {
                        setMainNavBasket(data['itemsTotal'], data['totalPrice']);
                    });
                } else {
                    showTooltipAgeRestricted(context);
                }
            });

            return false;
        });

        bookVersions.on('click', '.btn-block span.btn', ageTooltipHandler);

        bookVersions.on('click', '.btn-library.in-library', function(e) {
            e.preventDefault();
            wrapperCheckUserAge(this, 0, function(context) {
                var versionBlock = $(context).closest('.version');
                var canRead = versionBlock.closest('.product').data('can-read');

                if (canRead) {
                    window.open($(context).attr("href"), '_blank');
                } else {
                    $(context).attr("href", "");
                    showTooltipAgeRestricted(context);
                }
            });

            return false;
        });

        /** Reviews */

        var serverErrorModal = productPage.find('#server-error-modal');
        var tabsContainer = productPage.find('.book-content .nav-tabs');

        var initTab = function(type) {
            var tab, loader, thxModal, countBlock;

            switch (type) {
                case 1: tab = productPage.find('#tab-review'); break;
                case 2: tab = productPage.find('#tab-critique'); break;
            }

            switch (type) {
                case 1: loader = productPage.find('#main-loader').Loader(productPage.find('#review-form')); break;
                case 2: loader = productPage.find('#main-loader').Loader(productPage.find('#critique-form')); break;
            }

            switch (type) {
                case 1: thxModal = productPage.find('#review-thx-modal'); break;
                case 2: thxModal = productPage.find('#critique-thx-modal'); break;
            }

            switch (type) {
                case 1: countBlock = tabsContainer.find('#review-tab-item .count'); break;
                case 2: countBlock = tabsContainer.find('#critique-tab-item .count'); break;
            }

            var reviewForm = tab.find('.form');
            var textArea = reviewForm.find('.text-area');
            var ratingStarsBlock = reviewForm.find('.rating-stars');
            var ratingStarsContainer = ratingStarsBlock.find('.stars');
            var stars = ratingStarsContainer.find('.star');
            var sendReviewButton = reviewForm.find('.send-review-button');

            sendReviewButton.on('click', function() {
                var rating = stars.filter('.selected').size();
                var review = textArea.val();

                if (review == '') {
                    textArea.tooltip('show');
                    ratingStarsContainer.tooltip('hide');
                    return;
                } else {
                    textArea.tooltip('hide');
                }

                if (rating == 0) {
                    ratingStarsContainer.tooltip('show');
                    return;
                } else {
                    ratingStarsContainer.tooltip('hide');
                }

                loader.start();
                $.ajax('', {
                    type: 'post',
                    data: {
                        ajax: true,
                        action: 'addReview',
                        bookId: bookId,
                        review: review,
                        rating: rating,
                        type: type == 1 ? 'review' : 'critique'
                    },
                    dataType: 'json',
                    success: function(data) {
                        if (data['review'] !== undefined) {
                            var topOffset = tab.offset().top;
                            tab.prepend(data['review']);
                            window.scrollTo(0, topOffset);
                            countBlock.text(data['total']);
                        } else {
                            thxModal.modal('show');
                        }
                    },
                    error: function(jqXHR) {
                        serverErrorModal.modal('show');
                    },
                    complete: function() {
                        loader.stop();
                        textArea.val('');
                        stars.removeClass('selected');
                    }
                });
            });
        };

        initTab(1);
        initTab(2);
    }

    var basketPage = $('.row.basket');
    if (basketPage.size()) {
        var basketPageLoadableArea = basketPage.find('#main-loadable-area');
        var basketPageLoader = basketPage.find('#main-area-loader');
        var basketLoader = basketPageLoader.Loader(basketPageLoadableArea);

        function reInitAffixBlock() {
            $('.basket .total').affix({
                offset: {
                    top: 250,
                    bottom: function () {
                        return (this.bottom = $('.main-footer').outerHeight(true) + 72)
                    }
                }
            });
        }

        /** Deleting of book versions */

        basketPageLoadableArea.on('click', '.product-versions .version .delete-version-link', function(e) {
            e.preventDefault();
            basketLoader.start();
            $.ajax('', {
                type: 'post',
                data: {
                    ajax: true,
                    action: 'deleteVersion',
                    itemId: $(this).closest('.version').data('basket-item-id')
                },
                dataType: 'json',
                success: function(data) {
                    basketPageLoadableArea.html(data['mainLoadableContent']);
                    setMainNavBasket(data['itemsTotal'], data['totalPrice']);
                },
                complete: function() {
                    basketLoader.stop();
                    reInitAffixBlock();
                }
            });
        });

        /** Deleting of books */

        basketPageLoadableArea.on('click', '.product .delete-book-link', function(e) {
            e.preventDefault();
            basketLoader.start();
            $.ajax('', {
                type: 'post',
                data: {
                    ajax: true,
                    action: 'deleteBook',
                    bookId: $(this).closest('.product').data('book-id')
                },
                dataType: 'json',
                success: function(data) {
                    basketPageLoadableArea.html(data['mainLoadableContent']);
                    setMainNavBasket(data['itemsTotal'], data['totalPrice']);
                },
                complete: function() {
                    basketLoader.stop();
                    reInitAffixBlock();
                }
            });
        });

        /** Applying coupons */

        function validateCoupon(coupon, couponInput, errorsBlock) {
            if (coupon == '') {
                couponInput.addClass('error');
                errorsBlock.text('').append('<div class="error">Введите промокод.</div>').show();
                return false;
            }
            couponInput.removeClass('error');
            errorsBlock.text('').hide();
            return true;
        }

        basketPageLoadableArea.on('click', '#discount-coupon-form .discount-coupon-count', function(e) {
            e.preventDefault();

            var couponForm = $(this).closest('#discount-coupon-form');
            var couponInput = couponForm.find('.discount-coupon-input');
            var errorsBlock = couponForm.find('.errors');
            var coupon = couponInput.val();
            var couponAjaxValidation = new window.FieldAjaxValidation();

            couponAjaxValidation.addField('coupon', couponInput, errorsBlock);

            if (validateCoupon(coupon, couponInput, errorsBlock)) {
                basketLoader.start();
                $.ajax('', {
                    type: 'post',
                    data: {
                        ajax: true,
                        action: 'applyCoupon',
                        coupon: coupon
                    },
                    dataType: 'json',
                    success: function(data) {
                        couponAjaxValidation.success(data);
                        basketPageLoadableArea.html(data['mainLoadableContent']);
                        setMainNavBasket(data['itemsTotal'], data['totalPrice']);
                    },
                    error: function(jqXHR) {
                        couponAjaxValidation.error(jqXHR);
                    },
                    complete: function() {
                        basketLoader.stop();
                        reInitAffixBlock();
                    }
                });
            }
        });
    }

    $('#enter-registration').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var authForm = $(this).find('.main-auth-form');
        authForm.attr('data-return-url', link.data('return-url'));
        authForm.attr('data-next-modal', link.data('next-modal'));
    });

    $('.main-auth-form, #main-phone-password').on('submit', function(e){
        e.preventDefault();
        var authModal = $(this).closest('#enter-registration');
        $form = $(this);
        var loginInput = $('input[name="USER_LOGIN"]', $form);
        var passwordInput = $('input[name="USER_PASSWORD"]', $form);

        if (loginInput.val() == '' || passwordInput.val() == '') {
            $('.error-message', $form).text('Не заполнены обязательные поля');
            $('.validated-input', $form).addClass('error');

            return;
        }

        $('.waiting-button', $form).waitingButton().startWaiting();
        $.post($form.attr('action'), $form.serialize())
            .done(function(data) {
                var nextModal = $form.data('next-modal');
                if (nextModal !== undefined) {
                    authModal.modal('toggle');
                    $(nextModal).modal('toggle');
                    return;
                }

                // TODO: -
                var ServiceProcessing = $('#services-form');
                if (ServiceProcessing[0] && ServiceProcessing.data('completed')) {
                    $('#loadable-area').find('.submit-button').trigger('click');
                } else {
                    var returnUrl = $form.data('return-url');
                    if (returnUrl !== undefined) {
                        location.href = returnUrl;
                    } else {
                        location.reload();
                    }
                }
            })
            .error(function(data) {
                $('.waiting-button', $form).waitingButton().stopWaiting();
                data = JSON.parse(data.responseText);
                if (data.errors) {
                    $('.error-message', $form).text(data.errors.MESSAGE);
                    $('.validated-input', $form).addClass('error');
                }
            });
    });

    $('#main-forgot-form').on('submit', function(e){
        e.preventDefault();
        $form = $(this);
        $('.waiting-button', $form).waitingButton().startWaiting();
        $.post($form.attr('action'), $form.serialize())
            .done(function(data) {
                $('.waiting-button', $form).waitingButton().stopWaiting();
                data = JSON.parse(data);
                $('#pas-recovery-step-1').modal('toggle');
                if (data.type == 'email') {
                    $('#change-email').text(data.email);
                    $('#pas-recovery-step-2').modal();
                } else {
                    $('#mobile-confirm').modal();
                }
            })
            .error(function(data) {
                $('.waiting-button', $form).waitingButton().stopWaiting();
                data = JSON.parse(data.responseText);
                if (data.errors) {
                    $('.error-message', $form).text(data.errors.MESSAGE);
                    $('.validated-input', $form).addClass('error');
                }
            });
    });

    $('#pas-recovery-step-1').on('shown.bs.modal', function () {
        $('#enter-registration').modal('hide');
    });

    $('#change-password-form').on('submit', function(e){
        e.preventDefault();
        $form = $(this);
        $.post($form.attr('action'), $form.serialize())
            .done(function(data) {
                data = JSON.parse(data);
                $('.modal-body', '#pas-recovery-step-3').html(data.message);
            })
            .error(function(data) {
                data = JSON.parse(data.responseText);
                if (data.errors) {
                    $('.error-message', $form).text(data.errors.MESSAGE);
                    $('.validated-input', $form).addClass('error');
                }
            });
    });

    $('#main-phone-confirm').on('submit', function(e){
        e.preventDefault();
        $form = $(this);
        $.post($form.attr('action'), $form.serialize())
            .done(function(data) {
                data = JSON.parse(data);
                $('#mobile-confirm').modal('toggle');
                $('#thx-phone').text(data.login);
                $('#registration-thx-phone').modal();
            })
            .error(function(data) {
                data = JSON.parse(data.responseText);
                if (data.errors) {
                    $('.error-message', $form).text(data.errors.MESSAGE);
                    $('.validated-input', $form).addClass('error');
                }
            });
    });

    $('.main-reg-form').on('submit', function(e){
        e.preventDefault();
        $form = $(this);
        $('.waiting-button', $form).waitingButton().startWaiting();
        $.post($form.attr('action'), $form.serialize())
            .done(function(data) {
                $('.waiting-button', $form).waitingButton().stopWaiting();
                data = JSON.parse(data);
                $('.error-message', $form).text("");
                $('.validated-input', $form).removeClass('error');
                if (data.type == 'email') {
                    registrationModal.modal('toggle');
                    $('#thx-email').text(data.login);
                    $('#registration-thx').modal();
                } else {
                    $('#auth-additional-info').text('Войдите в систему, используя полученные имя пользователя и пароль');
                    $('.main-auth-form input[type=text], .main-auth-form input[type=password]').val("").removeClass('error');
                    $('.main-auth-form .error-message').text('');
                    $('#login-tab').click();
                }

            })
            .error(function(data) {
                $('.waiting-button', $form).waitingButton().stopWaiting();
                data = JSON.parse(data.responseText);
                if (data.errors) {
                    $('.error-message', $form).text(data.errors.MESSAGE);
                    $('#' + data.errors.TYPE, $form).addClass('error');
                }
            });
    });

    $('.exit-button').click(function(e){
        e.preventDefault();
        $('#logout-form').submit();
    });

    $('#registration-thx').on('hide.bs.modal', function (e) {
        var ServiceProcessing = $('#services-form');
        if (ServiceProcessing[0] && ServiceProcessing.data('completed')) {
            $('#loadable-area').find('.submit-button').trigger('click');
        } else {
            location.reload();
        }
    });

    $('#pas-recovery-step-2').on('hide.bs.modal', function (e) {
        location.reload();
    });

    $('#checkbox-publish input[type=checkbox]').change(function(){
        $(this).parent().siblings().children().filter(':checked').not(this).removeAttr('checked');
    });

    if (window.location.hash.length) {
        var anchor = window.location.hash;
        if (anchor == "#signup") {
            var signupAvailable = $('*').is("[data-tab-id='#reg-tab']");
            if (signupAvailable) {
                $("#enter-registration").modal("show");
                $("#enter-registration #reg-tab").tab("show");
                window.history.pushState('', document.title, window.location.pathname);
            }
        } else if (anchor == "#login") {
            var signinAvailable = $('*').is("[data-tab-id='#login-tab']");
            if (signinAvailable) {
                $("#enter-registration").modal("show");
                $("#enter-registration #login-tab").tab("show");
                window.history.pushState('', document.title, window.location.pathname);
            }
        }
    }

    var articlePage = $('.article');
    if (articlePage.length > 0) {
        if (bks.getAnchorFromCurrentUrl() === 'personal-information') {
            var agreementAuthorModal = $('#agreeement-author');
            agreementAuthorModal.on('shown.bs.modal', function () {
                var transitionCount = 0;
                $("body").on($.support.transition.end, '#agreeement-author', function() {
                    // hack; if transitionCount = 3, then animation of opening the modal is completed
                    if (transitionCount++ == 3) {
                        agreementAuthorModal.animate({
                            scrollTop: $('#personal-information').offset().top
                        }, 'slow');
                        window.location.hash = '';
                    }
                });
            });
            agreementAuthorModal.modal('show');
        }
    }
});



var btnGorkyShow = $('.btnGorkyShowList');

btnGorkyShow.on('click', function(e) {
    e.preventDefault();
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
        $(this).html('Свернуть список');
    } else {
        $(this).html('Показать всех номинантов');
    }
});