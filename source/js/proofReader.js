$(function () {
    proofReaderTabs();
    changeSteps();
    initPopupProofreader();
    
    function proofReaderTabs() {
        $('body').on('click', '.c-tab', function (e) {
            let $this = $(this),
                showBlock = $this.data('link');

            e.preventDefault();
            $this.siblings().removeClass('active');
            $this.addClass('active');

            $('.c-tab-block').hide().removeClass('active');
            $(showBlock).fadeIn('100').addClass('active');
        });
    }
    
    function initPopupProofreader() {
        $('.c-pay-proofreader').fancybox({
            autoSize: 'false',
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none'
        });
    }
    
    function changeSteps() {
        if($('.dropzone-steps').length) {
            Dropzone.autoDiscover = false;
            var myDropzone = new Dropzone(".dropzone-steps", {
                maxFiles: 1,
                url: "/ajax/print_book/dropzone_upload.php",
                paramName: "image",
                acceptedFiles: '.doc, .docx',
                init: function() {
                    var $this = this,
                        isChanged = false;
                    
                    $this.on("addedfile", function(file, done) {
                        $('.dropzone-steps').removeClass('preload');
                        $('.c-preloader').hide();
                        
                        if (isChanged) {
                            var newFile = file;
                            isChanged = false;
                            $this.removeAllFiles(true);
                            $this.emit("addedfile", newFile);
                            //$this.emit("thumbnail", newFile, newFile.url);
                            $this.files.push(newFile);
                        }
                    }); 
                    
                    $this.on("removedfile", function(file, done) {
                        $('.dropzone-steps').removeClass('preload');
                        $('.c-preloader').hide();
                    }); 
                    
                    $this.on("totaluploadprogress", function(file, progress, bytesSent) {
                        $('.dropzone-steps').addClass('preload');
                        $('.c-preloader').show();
                    });  
                    
                    $('.c-cancel-dropzone').on('click', function(){
                        $('.dropzone-steps').removeClass('dropzone-steps-upload');
                        $('.c-steps').removeClass('active');
                        $('.c-step-1').addClass('active');
                        $this.removeAllFiles(true);
                    });
                    
                    $('.c-change-dropzone').on('click', function(){
                        isChanged = true;
                        $('#upload').trigger('click');
                    });
                },
                accept: function(file, done) {
                    $('.dropzone-steps').addClass('dropzone-steps-upload');
                    $('.c-steps').removeClass('active');
                    $('.c-step-2').addClass('active');
                    download_file = file;
                    //console.log(download_file);
                },
                error: function(file) {
                    $('.dropzone-steps').addClass('dropzone-steps-upload');
                    $('.c-steps').removeClass('active');
                    $('.c-step-1').addClass('active');
                    $('.c-step-2').removeClass('active');
                    $('[data-dz-errormessage]').html('Неподдерживаемый формат файла');
                    download_file = file;
                    //console.log(download_file);
                }
            });
        }
    }
});