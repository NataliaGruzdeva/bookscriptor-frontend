$(document).ready(function(){
    $('#withdraw-amount').keyup(function(){
        var value = $(this).val().replace(/[^0-9.,]/g, "");

        if (value == "") {
            value = 0;
        } else if (value != 0) {
            value = value.replace(/^0+/, '');
        }

        if (value > $(this).data('max')) {
            value = $(this).data('max');
        }

        $(this).val(value);
    });


    $('#withdraw-form').submit(function(e){
        var $amountInput = $('#withdraw-amount');
        $amountInput.keyup();

        var amount = $('#withdraw-amount').val().replace(',', '.');
        amount = parseFloat(amount).toFixed(2);

        var error = "";

        if (amount == 0) {
            error = 'Необходимо указать сумму для снятия';
        } else if (amount < $('#withdraw-amount').data('min')){
            error = 'Минимальная сумма для снятия составляет ' + $('#withdraw-amount').data('min') + ' <span class="rouble-sign">Р</span>';
        }

        if (error) {
            e.preventDefault();
            $('#withdraw-error').html(error);
        } else {
            $('#withdraw-error').html("");
            var withdrawWaitingButton = $('#submit-withdraw').waitingButton();
            withdrawWaitingButton.startWaiting();
        }
    });
});