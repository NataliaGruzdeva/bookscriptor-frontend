var download_file,
    data;

$(function () {
    textareaAutosize();
    validateForm();
    inputNumber();
    tabBooks();
    readAll();
    changeSteps();
    addDescr();
    deleteField();
    leavePage();
    sortByTags();
    sortSelect();
    $('body').on('click', '[data-link="#author"]', function () {
      sliderImage();
  })
    validateFormCallback();
    changeImage();
    describeImage();
    closeCallbackPopup();
    $('input[name="phoneCallback"]').mask("+7 (999) 999-9999");

});

/*работа формы, заполнение поле с описание, динамика*/
function describeImage() {
    $('[data-content="btnToForm"]').show();
    $('body').on('click', '[data-jump]', function (e) {
        e.preventDefault();
        $(this).parents('[data-content]').addClass('hidden');
        var tempData = $(this).data('jump');
        $('[data-content]').each(function () {
            if ($(this).data('content') == tempData) {
                $(this).removeClass('hidden');
                $('.c-link-anchor[href="#form"]').click();
            }
        });

    });
}

/*выбор иллюстрации*/
function changeImage() {
    $('body').on('click', '[data-change]', function (e) {
        e.preventDefault();
        $('[data-change]').removeClass('change').attr('data-change', 'off');
        $(this).addClass('change').attr('data-change', 'on');
    })
}
/*селект сортировка иллюстраций*/
function sortSelect() {
    $('body').on('change', '[data-sort="select"]', function (e) {
        e.preventDefault();
        data = "sortSelect=" + $(this).val();
        $.ajax({
            url: '/ajax/print_book/send_anket.php',
            type: 'POST',
            dataType: 'JSON',
            data: data,
            processData: false,
            contentType: false,
            success: function (data) {

            },
            error: function (data) {

            }
        })
    })
}
/*сортировка иллюстраций по тегам*/
function sortByTags() {
    $('body').on('click', '[data-sort="tag"]', function (e) {
        e.preventDefault();
        var sortName = $(this).data('sort-type');
        $('[data-sort="tag"]').removeClass('active');
        $(this).addClass('active');
        $('[data-sort-name]').hide();
        $('[data-sort-name]').each(function () {
            if($(this).data('sort-name') == sortName) {
                $(this).show();
            }
        })
        if($(this).data('sort-type') == 'all') {
            $('[data-sort-name]').show();
        }


    })
}
/*карусель иллюстраций на вкладке Авторы*/
function sliderImage() {
    $('[data-block="slider"]').slick({
        slidesToShow: 2,
        arrows: true,
        slidesToScroll: 1
    });
}
/*табы*/
function tabBooks() {
    $('body').on('click', '.c-profile-tab', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');

        $('.c-profile-block').removeClass('active');
        $(showBlock).addClass('active');
    });

}

//ресайз текстового поля в зависимости от строк введенного текста
function textareaAutosize() {
    $('textarea').each(function(){
        autosize(this);
    }).on('autosize:resized', function(){

    });
}

function closeCallbackPopup() {
    $('body').on('click', '[data-action="close"]', function (e) {
        e.preventDefault();
        $.fancybox.close();
    })
}

//ссылка "показать всё"
function readAll() {
    $('body').on('click', '[data-type="all"]', function (e) {
        e.preventDefault();
        $(this).parent('[data-type="review"]').addClass('height-auto');
        $(this).hide();
    })
}

/*валидация формы обратного звонка в попапе*/
function validateFormCallback() {
    $('form[name="popupCallback"]').validate({
        rules: {
            phoneCallback: "required",
            nameCallback: "required",
        },
        messages: {
            nameCallback: "Напишите свое имя",
            phoneCallback: "Оставьте телефон для связи",
        },
        submitHandler: function submitHandler(form) {
            sendFormCallback();
        }
    });

}

/*валидация формы*/
function validateForm() {
    $("form[name='illustration']").validate({
        rules: {
            phone: "required",
            name: "required",
            email: {
                required: true,
                // Отдельное правило для проверки email
                email: true
            },
            comment: "required",
            comment2: "required",
            comment3: "required",
        },
        messages: {
            name: "Напишите свое имя",
            email: "Оставьте свой email",
            phone: "Оставьте телефон для связи",
        },
        submitHandler: function submitHandler(form) {
            sendForm()
        }
    });

}


//добавление нового поля с описанием иллюстрации
function addDescr() {
    $('body').on('click', '[data-action="add"]', function (e) {
        e.preventDefault();
        //переменная для генерации уникальных id для textarea
        var temp = Math.floor(Math.random()*100);
        $(this).before('<div class="block-form__bl-wrapper">\n' +
            '                  <div class="block-input block-input--cross">\n' +
            '                    <textarea id="comment' + temp + '" name="comment' + temp + '" data-type="textarea" cols="30" rows="5" placeholder="" class="input-custom c-input-custom" style="overflow: hidden; word-wrap: break-word; height: 61px;"></textarea>\n' +
            '                    <label for="comment' + temp + '" class="label-custom c-label-custom">Описание иллюстрации</label>\n' +
            '<a href="" data-action="delete" class="block-form__delete"> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" class="hover">' +
            '<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-red-cross"></use>' +
            '</svg>' +
            '<div class="block-form__delete--hover">Удалить поле</div>' +
            '</a>' +
            '                  </div>' +
            '</div>');
    });
}

//confirm "покинуть страницу"
function leavePage() {
    window.onbeforeunload  = function()
    {
        return confirm('Вы уверены, что хотите покинуть страницу? Данные формы будут потеряны');
    }
}

//удалить поле с описанием иллюстрации
function deleteField() {
    $('body').on('click', '[data-action="delete"]', function (e) {
        e.preventDefault();
        $(this).parents('.block-form__bl-wrapper').remove();
    });
}


//отправка формы
function sendForm() {
    var data = new FormData();
    data.append('comment', $('textarea[name="comment"]').val());
    data.append('color', $('input[name="color"]:checked').data('check'));
    data.append('count', $('input[name="count"]').val());
    data.append('comment2', $('textarea[name="comment2"]').val());
    data.append('comment3', $('textarea[name="comment3"]').val());
    data.append('bookName', $('select[name="bookName"]').val());
    data.append('book', download_file);

    data.append('name', $('input[name="name"]').val());
    data.append('email', $('input[name="email"]').val());
    data.append('phone', $('input[name="phone"]').val());
    data.append('image', $('[data-change="off"]').find('img').prop('src'));

    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        error: function (data) {

        }
    })
}

//отправка формы
function sendFormCallback() {
    var data = $('form[name="popupCallback"]').serialize();
    $.ajax({
        url: '/ajax/print_book/send_anket.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

        },
        error: function (data) {
            $('[data-type="form"]').hide();
            $('[data-type="success"]').show();
            // sendCallbackYet();
        }
    })
}

//загрузка файла через dropzone
function changeSteps() {
    if($('.dropzone-steps').length) {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(".dropzone-steps", {
            maxFiles: 1,
            url: "/ajax/print_book/dropzone_upload.php",
            paramName: "image",
            acceptedFiles: '.doc, .docx',
            init: function() {
                var $this = this,
                    isChanged = false;

                $this.on("addedfile", function(file, done) {
                    $('.dropzone-steps').removeClass('preload');
                    $('.c-preloader').hide();

                    if (isChanged) {
                        var newFile = file;
                        isChanged = false;
                        $this.removeAllFiles(true);
                        $this.emit("addedfile", newFile);
                        //$this.emit("thumbnail", newFile, newFile.url);
                        $this.files.push(newFile);
                    }

                });

                $this.on("removedfile", function(file, done) {
                    $('.dropzone-steps').removeClass('preload');
                    $('.c-preloader').hide();
                });

                $this.on("totaluploadprogress", function(file, progress, bytesSent) {
                    $('.dropzone-steps').addClass('preload');
                    $('.c-preloader').show();
                });

                $('body').on('click', '.c-change-dropzone', function () {
                    isChanged = true;
                    $this.removeAllFiles(true);
                    $('#upload').trigger('click');
                });
            },
            accept: function(file, done) {
                $('.c-steps').removeClass('active');
                $('.c-step-2').addClass('active');
                if(!$('.c-append-form').length) {
                    $('.dz-details').append('<div class="append-form c-append-form"><a href="javascript:void(0);" class="change-dropzone c-change-dropzone">Изменить файл</a></div>');
                }
                download_file = file;
            }

        });
    }
}



//блок с количеством (+/-)
function inputNumber() {
    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">—</div></div>').insertAfter('.quantity input');
    jQuery('.quantity').each(function() {
        var spinner = jQuery(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
}