$(function () {
    customInputNumber();
    validateForm();
    sendCallbackYet();
    blogTabs();
    initMasonry();
    filterArticles();
});

function initMasonry() {
    $('[data-type="masonry"]').masonry({
        columnWidth: 1,
        itemSelector: '.blog__item',
        stamp: '.blog__stamp'
    });
}
function blogTabs() {
    $('body').on('click', '.c-tab', function (e) {
        let $this = $(this),
            showBlock = $this.data('link');

        e.preventDefault();
        $this.siblings().removeClass('active');
        $this.addClass('active');

        $('.c-tab-block').hide().removeClass('active');
        $(showBlock).fadeIn('100').addClass('active');
    });
}

function filterArticles() {
    $('body').on('click', '[data-theme]', function (e) {
        var dataValue = $(this).data('theme');
        $('[data-theme-item]').hide();
        $('[data-theme-item*="' + dataValue + '"]').show();
        initMasonry();
        if(dataValue == "all") {
            $('[data-theme-item]').show();
            initMasonry();
        }
    });
}
function customInputNumber() {
    $('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
    $('.quantity').each(function() {
        var spinner = $(this),
            input = spinner.find('input[type="number"]'),
            btnUp = spinner.find('.quantity-up'),
            btnDown = spinner.find('.quantity-down'),
            min = input.attr('min'),
            max = input.attr('max');

        btnUp.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue >= max) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue + 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

        btnDown.click(function() {
            var oldValue = parseFloat(input.val());
            if (oldValue <= min) {
                var newVal = oldValue;
            } else {
                var newVal = oldValue - 1;
            }
            spinner.find("input").val(newVal);
            spinner.find("input").trigger("change");
        });

    });
}
/*валидация формы*/
function validateForm() {
   $("form[name='subscribe_form']").validate({
        rules: {
            phone: "required"
        },
        messages: {
            phone: "Оставьте телефон для связи"
        },
        submitHandler: function submitHandler(form) {
            sendFormCallback();
        }
    });
 $("form[name='subscribe_form_two']").validate({
        rules: {
            email: "required"
        },
        messages: {
            email: "Укажите свой e-mail"
        },
        submitHandler: function submitHandler(form) {
            sendFormCallback();
        }
    });

}

function sendFormCallback() {
    var data = $('.c-callback-form').serialize();
    $.ajax({
        url: '/ajax/print_book/send.php',
        type: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (data) {
            $('.c-callback-form').hide();
            $('.c-callback-success').show();
            sendCallbackYet();

        },
        error: function (data) {
            /*это оставить только для success*/
            $('.c-callback-form').hide();
            $('.c-callback-success').show();
            sendCallbackYet();
        }
    })
}
function sendCallbackYet() {
    $('body').on('click', '.c-send-yet', function () {
        $('.c-callback-form').show();
        $('.c-callback-success').hide();
        $('.c-callback-form input[name="phone"]').val('');
    })
}